/**
 * @author adavid
 *
 * Apr 10, 2019
 */

public class EnumTest {

  
  public static void main(String s[]) {
    boolean boo= CheckListItems.PRE_CLERKSHIP_CURRICULUM.isCompleted;
    System.out.println(boo);
    CheckListItems  item= CheckListItems.PRE_CLERKSHIP_CURRICULUM.setCompleted(true);
    boo = item.isCompleted;
    System.out.println(boo);
      item= CheckListItems.PRE_CLERKSHIP_CURRICULUM.setCompleted(false);
    boo = item.isCompleted;
    System.out.println(boo);
  }
  
  
  
  
  
  public enum CheckListItems {  
    PRE_CLERKSHIP_CURRICULUM("PRE_CLERKSHIP_CURRICULUM","Pre-Clerkship Curriculum", false);
    private  String itemCode;
    private  String itemDisplayText;
    private boolean isCompleted;
    
    private CheckListItems(String itemCode, String itemDisplayText, boolean isCompleted) {
      this.itemCode = itemCode;
      this.itemDisplayText = itemDisplayText;
      this.isCompleted = isCompleted;
    }
    public CheckListItems setCompleted(boolean isCompleted) {
      this.isCompleted = isCompleted;
      return this;
    }
    
    }
}

