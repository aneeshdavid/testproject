package com.crack.Stack;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;

public class StackOfPlates<T> {
	private List<Stack<T>> stacks;
	int size;
	public StackOfPlates(int size) {
		this.size = size;
		this.stacks = new ArrayList<>();
	}
	
	public void push(T value) {
		Stack<T> stack = getLastStack();
		if(stack!=null && !(stack.size() >= this.size)) {
			stack.push(value);
		}else {
			Stack<T> newStack = new Stack<>();
			stack.push(value);
			this.stacks.add(newStack);
		}
	}
	
	public T pop() {
		Stack<T> stack = null;
		try {
		 stack = getLastStack();
		if(stack==null)
			throw new EmptyStackException();
		}catch(EmptyStackException ex) {
			System.out.println("Stack is empty");
		}
		return stack.pop();
	}
	
	public T popAt(int index) {
		T value = null;
		try {
			if(this.stacks.size()< index+1)
				throw new IndexOutOfBoundsException();
			
			Stack<T> stack = this.stacks.get(index);
			value = stack.pop();
			if(stack.isEmpty())
				this.stacks.remove(index);
			if(this.stacks.size()> index+1) {
				T item = popAt( index+1);
				stack.push(item);
			}
		}catch(IndexOutOfBoundsException ex) {
			System.out.println("Index not found");
		}
		return value;
	}
	
	private Stack<T> getLastStack() {
		if(this.stacks.isEmpty()) {
			return null;
		}
		return this.stacks.get(this.stacks.size()-1);
	}
	public static void main(String[] args) {
		

	}

}
