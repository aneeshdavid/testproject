package com.crack.Stack;

import java.util.EmptyStackException;

public class MultiStack {
	int maxStack =3;
	int stackSize;
	int stack[];
	int values[];
	
	public MultiStack(int stackSize) {
		stack = new int[maxStack];
		values = new int[stackSize * maxStack];
		this.stackSize = stackSize;
	}
	
	public void push(int stackNum, int val) throws ArrayIndexOutOfBoundsException{
		try {
		if(stack[stackNum] == stackSize)
			throw new ArrayIndexOutOfBoundsException();
	
		stack[stackNum]++;
		int top = findTop(stackNum) ;
		values[top] = val;
		System.out.println("Pushed:"+ val);
		}catch(Exception e) {
			System.err.println("An exception was thrown");
		}
		
	}
	
	public int pop(int stackNum) {
		int top   = findTop(stackNum);
		int value = 0;
		try {
			if(values[top] == 0)
				throw new EmptyStackException();
			 value = values[top];
			stack[stackNum]--;
			values[top]=0;
			System.out.println("Popped:"+ value);
		}catch(EmptyStackException e) {
			System.err.println("An exception was thrown");
		}
	
		return value;
	}
	
	private int findTop(int stackNum) {
		int offset = stackNum* stackSize;
		int size = stack[stackNum];
		return offset +  size -1 ;
	}

	public static void main(String[] args) {
		MultiStack multiStack = new MultiStack(3);
		multiStack.push(0, 1);
		multiStack.push(0, 2);
		multiStack.push(0, 3);
		//multiStack.pop(0);
		multiStack.push(0, 4);
		
		multiStack.push(1, 10);
		multiStack.push(1, 20);
		multiStack.push(1, 30);
		//multiStack.pop(1);
		multiStack.push(1, 40);
		
		multiStack.push(2, 100);
		multiStack.push(2, 200);
		multiStack.push(2, 300);
		//multiStack.pop(2);
		multiStack.push(2, 400);

	}

}
