package com.crack.Stack;

import java.util.Stack;

public class StackSort {

	public void sort(Stack<Integer> stack) {
		Stack<Integer> tempStack = new Stack();
		while(!stack.isEmpty()) {
			int temp = stack.pop();
			while(!tempStack.isEmpty() && tempStack.peek()> temp) {
				stack.push(tempStack.pop());
			}
			tempStack.push(temp);
		}
		
		while(!tempStack.isEmpty()) {
			stack.push(tempStack.pop());
		}
	}
	
	public static void main(String[] args) {
		Stack<Integer> stack = new Stack();
		stack.push(1);
		stack.push(5);
		stack.push(3);
		stack.push(10);
		stack.push(2);
		new StackSort().sort(stack);
		System.out.println("Completed");
	}

}
