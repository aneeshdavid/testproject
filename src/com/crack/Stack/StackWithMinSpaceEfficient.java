package com.crack.Stack;

import java.util.EmptyStackException;
import java.util.Stack;

public class StackWithMinSpaceEfficient extends Stack<Integer>{
	int min = Integer.MAX_VALUE;
	Stack<Integer> s2;
	
	public StackWithMinSpaceEfficient() {
		s2 = new Stack<>();
	}
	public void push(int value) {
		if(value<min()) {
			s2.push(value);
		}
		super.push(value);
	}
	
	public Integer pop() {
		int val=0;
		try {
			if(s2.isEmpty())
				throw new EmptyStackException();
			val = super.pop();
			if(val==min()) {
				s2.pop();
			}
		}catch(EmptyStackException e) {
			System.out.println("Stack is empty");
		}
		return val;
	}
	
	public int min() {
		int val=0;
		try {
			if(s2.isEmpty()) {
				val = Integer.MAX_VALUE;
			}
			else {
			val = s2.peek();
			}
		}catch(EmptyStackException e) {
			System.out.println("Stack is empty");
		}
		return val;
	}
	
	public static void main(String[] args) {
		StackWithMinSpaceEfficient stack = new StackWithMinSpaceEfficient();
		stack.push(5);
		stack.push(6);
		stack.push(3);
		stack.push(7);
		stack.push(1);
		System.out.println("Min:"+stack.min());
		stack.pop();
		System.out.println("Min:"+stack.min());
		stack.pop();
		System.out.println("Min:"+stack.min());
		stack.pop();
		System.out.println("Min:"+stack.min());
	}

}
