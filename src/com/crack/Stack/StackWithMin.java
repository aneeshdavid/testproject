package com.crack.Stack;

import java.util.EmptyStackException;

public class StackWithMin {
	int min = Integer.MAX_VALUE;
	StackNode top;
	static class StackNode{
		int value;
		StackNode next;
		int min;
		public StackNode(int value, int min) {
			this.value = value;
			this.min = min;
		}
	}
	
	public void push(int value) {
		min = Math.min(min, value);
		StackNode node = new StackNode(value, min);
		if(top==null) {
			top = node;
		}else {
			node.next = top;
			top = node;
		}
	}
	
	public int pop() {
		int val = 0;
		try {
			if(top==null)
				throw new EmptyStackException();
			val = top.value;
			top = top.next;
		}catch(EmptyStackException ex) {
			System.out.println("Stack is Empty");
		}
		return val;
	}
	
	public int min() {
		try {
			if(top==null)
				throw new EmptyStackException();
			
			
		}catch(EmptyStackException ex) {
			System.out.println("Stack is Empty");
		}
		System.out.println("Min:"+ top.min);
		return top.min;
	}
	
	public static void main(String[] args) {
		StackWithMin stack = new StackWithMin();
		stack.push(5);
		stack.push(6);
		stack.push(3);
		stack.push(7);
		stack.min();
		stack.pop();
		stack.min();
		stack.pop();
		stack.min();
	}

}
