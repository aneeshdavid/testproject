package com.crack.Stack;

import java.util.EmptyStackException;

public class MultiStackDynamicSize<T> {
	int stackSize;
	StackNode<T>[]  stackTopNodeArray;
	
	public MultiStackDynamicSize(int stackSize) {
		this.stackSize = stackSize;
		stackTopNodeArray = new StackNode[stackSize];
		
	}
	
	static class StackNode<T>{
		T value;
		StackNode<T> next;
		public StackNode(T value) {
			this.value = value;
		}
	}
	
	
	public void push(int stackNumber, T value) {
		try {
			if(stackNumber<0 || stackNumber>this.stackSize) {
				throw new ArrayIndexOutOfBoundsException();
			}
			if(stackTopNodeArray[stackNumber]==null) {
				StackNode<T> stack = new StackNode<T>(value);
				stackTopNodeArray[stackNumber] = stack;
			}else {
				StackNode<T> top = stackTopNodeArray[stackNumber];
				StackNode<T> stack = new StackNode<T>(value);
				stack.next=top;
				stackTopNodeArray[stackNumber] =stack;
			}
			System.out.println("Pushed:" + value);
		}catch(ArrayIndexOutOfBoundsException ex) {
			System.out.println("Invalid Stack Number");
		}
		
	}
	
	public T peek(int stackNumber) {
		T value = null;
		try {
			if(stackNumber<0 || stackNumber>this.stackSize) {
				throw new ArrayIndexOutOfBoundsException();
			}
			if(stackTopNodeArray[stackNumber]==null) {
				throw new EmptyStackException();
			}
			StackNode<T> top = stackTopNodeArray[stackNumber];
			value = top.value;
			stackTopNodeArray[stackNumber] =top.next;
			System.out.println("Popped:" + value);
		}catch(ArrayIndexOutOfBoundsException ex) {
			System.out.println("Invalid Stack Number");
		}
		catch(EmptyStackException ex) {
			System.out.println("Stack is empty");
		}
		return value;
	}
	
	public static void main(String[] args) {
		
		MultiStackDynamicSize<Integer> multiStack = new MultiStackDynamicSize<Integer>(3);
		multiStack.push(0, 1);
		multiStack.push(0, 2);
		multiStack.push(0, 3);
		multiStack.push(0, 4);
		multiStack.push(0, 5);
		multiStack.push(0, 6);
		multiStack.peek(0);
		multiStack.peek(0);
		multiStack.peek(0);
		multiStack.peek(0);
		multiStack.peek(0);
		multiStack.peek(0);
		multiStack.peek(0);
		multiStack.peek(0);
		
		multiStack.push(1, 10);
		multiStack.push(1, 20);
		multiStack.peek(1);
		
		multiStack.push(2, 100);
		multiStack.push(2, 200);
		multiStack.peek(2);
		
		multiStack.push(3, 1000); // Invalid Stack Number
	}

}
