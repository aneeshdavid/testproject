package com.crack.LinkedList;

import com.datastructures.linkedlist.MyLinkedList;

public class DeleteMiddleNode {

	
	private static void deleteMiddle(MyLinkedList<String> list) {
		
		MyLinkedList.Node<String> node = list.head;
		int length = 0;
		while(node!=null) {
			node = node.next;
			length++;
		}
		
		int count =1;
		 node = list.head;
		while(node!=null && node.next!=null) {
			if(count>=(length/2)-1) {
				node.next = node.next.next;
				break;
			}
			node = node.next;
			count++;
		}
		
	}
	
	public static void main(String[] args) {
		MyLinkedList<String> list = new MyLinkedList<>();
		list.insert("A");
		list.insert("B");
		list.insert("C");
		list.insert("D");
		list.insert("E");
		list.insert("F");
		list.print(list.head);
		
		deleteMiddle(list);
		System.out.println("After delete middle");
		
		
		list.print(list.head);

	}


}
