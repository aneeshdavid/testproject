package com.crack.LinkedList;

public class LinkedListHasLoop {
	
	private static Node findLoopNode(Node head) {
		Node slow_p = head;
		Node fast_p = head;
		
		while(fast_p!=null && fast_p.next!=null) {
			slow_p = slow_p.next;
			fast_p = fast_p.next.next;
			if(slow_p==fast_p) {
				break;
			}
		}
		
		if(fast_p==null || fast_p.next==null) {
			return null;
		}
		
		slow_p = head;
		
		while(slow_p!=fast_p) {
			slow_p = slow_p.next;
			fast_p = fast_p.next;
		}
		
		return fast_p;
		
	}
	
	
	static class Node{
		int val;
		Node next;
		public Node(int val) {
			this.val=val;
		}
	}
	public static void main(String[] args) {
		Node head = new Node(1);
		head.next = new Node(2);
		head.next.next = new Node(3);
		Node loopNode = new Node(4);
		head.next.next.next = loopNode;
		head.next.next.next.next = new Node(5);
		head.next.next.next.next.next = new Node(6);
		head.next.next.next.next.next.next = new Node(6);
	//	head.next.next.next.next.next.next.next = loopNode;
		
		Node loop = findLoopNode(head);
		if(loop==null)
			System.out.println("Null");
		else
			System.out.println(loop.val);
		
	}

}
