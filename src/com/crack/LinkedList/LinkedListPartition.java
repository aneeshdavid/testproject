package com.crack.LinkedList;

import com.datastructures.linkedlist.MyLinkedList;

public class LinkedListPartition {
	
	private static MyLinkedList doPartitions(MyLinkedList<Integer> list, int k) {
		MyLinkedList.Node<Integer> node = list.head;
		
		MyLinkedList.Node<Integer> head = node;
		MyLinkedList.Node<Integer> tail = node;
		while(node!=null) {
			MyLinkedList.Node<Integer> next = node.next;
			if(node.data < k) {
				node.next = head;
				head = node;
			}else {
				tail.next = node;
				tail = node;
			}
			
			node =next;
		}
		tail.next = null;
		MyLinkedList<Integer> linkedList = new MyLinkedList<Integer>();
		linkedList.head = head;
		return linkedList;
	}
	
	public static void main(String[] args) {
		MyLinkedList<Integer> list = new MyLinkedList<>();
		list.insert(3);
		list.insert(5);
		list.insert(8);
		list.insert(5);
		list.insert(10);
		list.insert(2);
		list.insert(1);
		
		list.print(list.head);
		
		 MyLinkedList outlist  = doPartitions(list, 5);
		System.out.println("After partition");
		list.print(outlist.head);

	}

}
