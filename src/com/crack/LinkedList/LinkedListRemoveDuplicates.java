package com.crack.LinkedList;

import com.datastructures.linkedlist.MyLinkedList;

public class LinkedListRemoveDuplicates {

	private static  MyLinkedList<Integer> removeDuplicates(MyLinkedList<Integer> list){
		
		MyLinkedList.Node ptr1  = list.head;
		while (ptr1!= null && ptr1.next != null) {
			MyLinkedList.Node<Integer> ptr2  = ptr1;
			while (ptr2!=null && ptr2.next!=null) {
				if(ptr1.data.equals(ptr2.next.data)) {
					ptr2.next = ptr2.next.next;
				}
				ptr2 = ptr2.next;
				
			}
			
			ptr1 = ptr1.next;
		}
		
		
		
		return list;
	}
	
	public static void main(String[] args) {
		
		MyLinkedList<Integer> list = new MyLinkedList<>();
		list.insert(1);
		list.insert(2);
		list.insert(1);
		list.insert(4);
		list.insert(4);
		list.insert(3);
		list.insert(4);
		
		list.print(list.head);
		
		removeDuplicates(list);
		System.out.println("After delete duplicates");
		
		
		list.print(list.head);
	}
	
	
	

}
