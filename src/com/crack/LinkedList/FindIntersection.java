package com.crack.LinkedList;

import com.crack.LinkedList.FindIntersection.Node.Result;

public class FindIntersection {
	
	private static Node findIntersection(Node n1, Node n2) {
		if(n1==null || n2==null) {
			return null;
		}
		
		Result result1 = findTailAndSize(n1);
		Result result2 = findTailAndSize(n2);
		if(result1.tail!=result2.tail) {
			return null;
		}
		
		Node longest = result1.size<result2.size ?n2:n1;
		Node shortest = result1.size>result2.size ?n2:n1;
		
		longest = findLongestNodeStartingPoint(longest, Math.abs(result2.size-result1.size));
		
		while(longest!=shortest) {
			longest = longest.next;
			shortest = shortest.next;
		}
		
		return longest;
		
	}
	
	private static Node findLongestNodeStartingPoint(Node longest, int diff) {
		while(longest!=null && diff>0) {
			longest = longest.next;
			diff--;
		}
		return longest;
	}

	private static Result findTailAndSize(Node n) {
		int size =0;
		while(n!=null) {
			n = n.next;
			size++;
		}
		return new Result(n,size);
	}

	static class Node{
		Node head;
		int data;
		Node next;
		public Node(int data) {
			this.data = data;
		}
		
		static class Result{
			Node tail;
			int size;
			public Result(Node tail, int size) {
				this.tail= tail;
				this.size=size;
			}
		}

	}
	public static void main(String[] args) {
		Node inter = new Node(7);
		inter.next = new Node(2);
		inter.next .next =  new Node(1);
		
		Node n1 = new Node(3);
		n1.next = new Node(1);
		n1.next .next =  new Node(5);
		n1.next .next.next =  new Node(9);
		n1.next .next.next .next=inter;
		
		Node n2 = new Node(4);
		n2.next = new Node(6);
		n2.next.next = inter;
		
		Node intersectionResult = findIntersection(n1,n2);
		String result = null;
		if(intersectionResult!=null) {
			result = intersectionResult.data+"";
		}
		System.out.println("Result: "+result);
	}

}
