package com.crack.LinkedList;

import com.datastructures.linkedlist.MyLinkedList;

public class LinkedListSum {
	
	private static  MyLinkedList doSum(MyLinkedList L1, MyLinkedList L2) {
		MyLinkedList<Integer> result = new MyLinkedList<>();
		int carry = 0;
		MyLinkedList.Node<Integer> node1= L1.head;
		MyLinkedList.Node<Integer> node2= L2.head;
		while(node1!=null || node2!=null) {
			int n1 = node1!=null && node1.data!=null ? node1.data: 0;
			int n2 = node2!=null && node2.data!=null ? node2.data: 0;
			int sum = carry + n1+n2;
			result.insert(sum%10);
			carry = sum/10;
			if(node1!=null)
			node1 = node1.next;
			if(node2!=null)
			node2 = node2.next;
		}
		if(carry>0) {
			result.insert(carry);
		}
		return result;
	}
	
	static class Node{
		Node head;
		int data;
		Node next;
		public Node(int data) {
			this.data = data;
		}

	}
	
	public static void main(String[] args) {
		MyLinkedList<Integer> L1= new MyLinkedList<>();
		L1.insert(7);
		L1.insert(1);
		L1.insert(6);
		
		MyLinkedList<Integer> L2 = new MyLinkedList<>();
		//L2.insert(5);
		L2.insert(9);
		L2.insert(2);
		
		MyLinkedList<Integer> result = doSum(L1, L2);
		result.print(result.head);
	}
}
