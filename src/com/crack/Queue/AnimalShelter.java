package com.crack.Queue;

public class AnimalShelter {

	static LinkedListNode front;
	static LinkedListNode rear;
	
	public void enqueue(Animal animal) {
		LinkedListNode newNode= new LinkedListNode(animal);
		if(front==null) {
			front = newNode;
			rear = newNode;
		}else {
			rear.next = newNode;
			rear = newNode;
		}
	}
	
	public Animal deQueue() {
		Animal animal = null;
		try {
			if(front==null) {
				throw new Exception();
			}
			animal = front.animal;
			front = front.next;
		}catch(Exception ex) {
			System.out.println("Shelter is empty");
		}
		return animal;
	}
	
	public Animal deQueueByAnimal(String animalType) {
		Animal animal = null;
		try {
			if(front==null) {
				throw new ShelterEmptyException();
			}
			
			LinkedListNode tempFront = front;
			LinkedListNode prev = null;
			while(tempFront!=null) {
				if(tempFront.animal.getType().equals(animalType)) {
					animal = tempFront.animal;
					prev.next = tempFront.next;
					break;
				}
				prev = tempFront;
				tempFront = tempFront.next;
			}
			
			
		if(animal==null)	{
			throw new AnimalNotFoundException();
		}
		}catch(ShelterEmptyException ex) {
			System.out.println("Shelter is empty");
		}catch(AnimalNotFoundException ex) {
			System.out.println(String.format("There is no %s in shelter", animalType));
		}
		return animal;
	}
	
	class LinkedListNode{
		Animal animal;
		LinkedListNode next;
		
		public LinkedListNode(Animal animal) {
			this.animal = animal;
		}
	}
	
	
	static  abstract class Animal{
		abstract String getType();
	}
	static class Cat extends Animal{
		@Override
		String getType() {
			return "Cat";
		}
	}
	  static class Dog extends Animal{
		
		@Override
		String getType() {
			return "Dog";
		}
	}
	
	
	class ShelterEmptyException extends Exception{
		
	}
	class AnimalNotFoundException extends Exception{
		
	}
	
	public static void main(String[] args) {
		AnimalShelter shelter = new AnimalShelter();
		AnimalShelter.Animal dog = new AnimalShelter.Dog();
		 shelter.enqueue(dog);
		 AnimalShelter.Animal cat = new AnimalShelter.Cat();
		 shelter.enqueue(cat);

		 AnimalShelter.Animal animal = new AnimalShelter().deQueueByAnimal("Cat");
		 System.out.println(animal.getType());
	}

}
