package com.crack.Queue;

import java.util.EmptyStackException;
import java.util.Stack;

public class QueueViaStack<T> {
	Stack<T> newStack;
	Stack<T> oldStack;
	
	public QueueViaStack() {
		newStack = new Stack<>();
		oldStack = new Stack<>();
	}
	
	public void push(T item) {
		newStack.push(item);
	}
	
	public T pop() {
		T item =null;
		shiftItems();
		try {
			if(oldStack.isEmpty())
				throw new EmptyStackException();
			
			item = oldStack.pop();
		}catch(EmptyStackException e) {
			System.out.println("Stack is empty");
		}
		return item;
	}
	
	private void shiftItems() {
		if(oldStack.isEmpty()) {
			while(!newStack.isEmpty()) {
				oldStack.push(newStack.pop());
			}
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
