package com.crack.Array;

import java.util.Arrays;

public class MergeSortedArrays {

  public static void main(String[] args) {
   int[] longArr = new int[] {4,5,6,7,8,0,0,0};
   int[] shortArr = new int[] {1,2,3,4};
   int resutl[] = new MergeSortedArrays().mergeArrays(longArr, shortArr);
   System.out.println(Arrays.toString(resutl));
  }
  
  int[] mergeArrays(int[] longArr, int[] shortArr ) {
    
    if(longArr.length<shortArr.length) {
      throw new ArrayIndexOutOfBoundsException("Not enough space in longArr");
    }
    
    int j = 0;
    for(int i =0; i<longArr.length; i++) {
      if(j==shortArr.length) {
        j=0;
      }
      if(longArr[i]>shortArr[j]) {
        int temp = longArr[i];
        longArr[i] = shortArr[j];
        shortArr[j] = temp;
        j++;
      }
      
      if(i!=0 && longArr[i] < longArr[i-1]) {
        longArr[i] = shortArr[j];
        j++;
      }
    }
    return longArr;
  }

}
