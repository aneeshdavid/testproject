package com.crack.TreeGraph;

import java.util.LinkedList;

public class CheckSubTree {

  public static void main(String[] args) {
    TreeNode<Integer> root = new TreeNode<>(20);
    root.left = new TreeNode<>(10);
    root.right = new TreeNode<>(30);
    root.left.left = new TreeNode<>(5);
    root.left.right = new TreeNode<>(15);
    root.left.right.right = new TreeNode<>(17);
    root.left.left.left = new TreeNode<>(3);
    root.left.left.right= new TreeNode<>(7);
        
    boolean isSubTree = checkSubTree(root, root.left.left.right);
    System.out.println(isSubTree);
  }

  private static boolean checkSubTree(TreeNode<Integer> root, TreeNode<Integer> subTree) {
    
    if(root == null||subTree==null) {
      return false;
    }
    
    if(root.equals(subTree)) {
      return true;
    }
    
    LinkedList<TreeNode<Integer>> queue = new LinkedList<TreeNode<Integer>>();
    queue.add(root);
    while(!queue.isEmpty()) {
      TreeNode<Integer> firstNodeInQueue  = queue.removeFirst();
      if(firstNodeInQueue.equals(subTree)) {
        return true;
      }
      if(firstNodeInQueue.left!=null) {
        queue.add(firstNodeInQueue.left);
      }
      if(firstNodeInQueue.right!=null) {
        queue.add(firstNodeInQueue.right);
      }
    }
    return false;
  }

}
