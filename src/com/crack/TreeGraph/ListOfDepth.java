package com.crack.TreeGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ListOfDepth {

	public static void main(String[] args) {
		List<LinkedList<TreeNode<Integer>>> list = new ArrayList<>();
		TreeNode<Integer> root = new TreeNode<>(10);
		root.left = new TreeNode<>(5);
		root.right = new TreeNode<>(20);
		root.left.left = new TreeNode<>(9);
		root.left.right = new TreeNode<>(18);
		root.right .left= new TreeNode<>(3);
		root.right .right= new TreeNode<>(7);
		getListOfdepth(root, list, 0);
		System.out.println(list.size());
		
		list = getListOfDepthAlternative(root);
		System.out.println(list.size());
	}

	private static void getListOfdepth(TreeNode<Integer> treeNode,List<LinkedList<TreeNode<Integer>>> depthList, int level) {
		if(treeNode==null)
			return;
		
		if(depthList.size() == level) {
			LinkedList<TreeNode<Integer>> list  = new LinkedList<>();
			list.add(treeNode);
			depthList.add(list);
		}else {
			depthList.get(level).add(treeNode);
		}
		getListOfdepth(treeNode.left, depthList, level+1);
		getListOfdepth(treeNode.right, depthList, level+1);
		
	}
	
	private static List<LinkedList<TreeNode<Integer>>> getListOfDepthAlternative(TreeNode root){
		List<LinkedList<TreeNode<Integer>>> resultList = new ArrayList<>();
		LinkedList<TreeNode<Integer>> currentList = new LinkedList<>();
		if(root!=null) {
			currentList.add(root);
		}
		while(currentList.size()>0) {
			resultList.add(currentList);
			LinkedList<TreeNode<Integer>> parentList = currentList;
			currentList = new LinkedList<>();
			for (TreeNode treeNode : parentList) {
				if(treeNode.left!=null) {
					currentList.add(treeNode.left);
				}
				if(treeNode.right!=null) {
					currentList.add(treeNode.right);
				}
			}
		}
		return resultList;
	}

}
