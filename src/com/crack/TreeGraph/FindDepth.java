package com.crack.TreeGraph;

public class FindDepth {

	public static void main(String[] args) {
		TreeNode<Integer> root = new TreeNode<>(10);
		root.left = new TreeNode<>(5);
		root.right = new TreeNode<>(20);
		root.left.left = new TreeNode<>(9);
		root.left.right = new TreeNode<>(18);
		root.right .left= new TreeNode<>(3);
		root.right .right= new TreeNode<>(7);
		root.right .right.left = new TreeNode<>(20);
		root.right .right.right = new TreeNode<>(21);
		int depth = findDepth(root, 0);
		System.out.println(depth);
	}

	private static int findDepth(TreeNode<Integer> root, int level) {
		if(root==null)
			return level;
		
		int leftDepth = findDepth(root.left, level+1);
		int rightDepth = findDepth(root.right, level+1);
		
		return leftDepth > rightDepth ? leftDepth : rightDepth;
	}

}
