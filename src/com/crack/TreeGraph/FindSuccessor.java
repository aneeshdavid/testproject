package com.crack.TreeGraph;

public class FindSuccessor {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(10);
		root.left = new TreeNode(5);
		root.right = new TreeNode(20);
		root.left.left = new TreeNode(9);
		root.left.right = new TreeNode(18);
		root.right .left= new TreeNode(3);
		root.right .right= new TreeNode(7);
		root.right .right.left = new TreeNode(20);
		root.right .right.right = new TreeNode(21);
		TreeNode successor = findSuccessor(root.left.right);

	}
	
	private static TreeNode findSuccessor(TreeNode node) {
		
		if(node.right!=null) {
			return findLeftMostNode(node.right);
		}else {
			TreeNode current = node;
			TreeNode parent = node.parent;
			
			while(parent!=null && current!=parent.left) {
				current = parent;
				parent = current.parent;
			}
			return parent;
		}
		
	}

	private static TreeNode findLeftMostNode(TreeNode node) {
		while(node!=null) {
			node = node.left;
		}
		return node;
	}

	static class TreeNode{
		int item;
		TreeNode left;
		TreeNode right;
		TreeNode parent;
		public TreeNode(int item) {
			this.item= item;
		}
	}

}
