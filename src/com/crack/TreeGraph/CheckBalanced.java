package com.crack.TreeGraph;

public class CheckBalanced {

	public static void main(String[] args) {
		TreeNode<Integer> root = new TreeNode<>(10);
		root.left = new TreeNode<>(5);
		root.right = new TreeNode<>(20);
		root.left.left = new TreeNode<>(9);
		root.left.right = new TreeNode<>(18);
		root.right .left= new TreeNode<>(3);
		root.right .right= new TreeNode<>(7);
		root.right .right.left = new TreeNode<>(20);
		root.right .right.right = new TreeNode<>(21);
		
		root.right .right.right.right = new TreeNode<>(301);
		boolean isBalanced = isBalanced(root);
		System.out.println(isBalanced);
	}

	private static boolean isBalanced(TreeNode<Integer> root) {
		int heightDiff= findHeightDiff(root, 0);
		return heightDiff>1 ? false : true;
	}

	private static int findHeightDiff(TreeNode<Integer> root, int height) {
		if(root==null){
			return height;
		}
		int leftHeight = findHeightDiff(root.left, height+1);
		int rightHeight = findHeightDiff(root.right, height+1);
		int diff = leftHeight - rightHeight;
		return Math.abs(diff);
	}

}
