package com.crack.TreeGraph;

public class MinimalBinarySearchTree {

	public static void main(String[] args) {
		int[] arr = new int[]{1,2,3,4,5,6,7,8,9};
		createMinimalBST(arr);

	}

	private static void createMinimalBST(int[] arr) {
		
		TreeNode<Integer> treeNode= minimalBSTHelper(arr, 0, arr.length-1);
		printTree(treeNode);
	}
	
	public static void printTree(TreeNode treeNode){
		if(treeNode!=null) {
			System.out.println(treeNode.item);
			printTree(treeNode.left);
			printTree(treeNode.right);
		}
	}

	private static TreeNode<Integer>  minimalBSTHelper(int[] arr, int start, int end) {
		if(start>end) {
			return null;
		}
		int mid = (start+end)/2;
		TreeNode<Integer> node = new TreeNode<>(arr[mid]);
		node.left = minimalBSTHelper(arr, start, mid-1);
		node.right = minimalBSTHelper(arr, mid+1, end);
		return node;
	}

}
