package com.crack.TreeGraph;

public class Graph<T> {

	Node<T>[] nodes;
	
	static class Node<T>{
		T item;
		boolean isVisited = false;
		Node<T>[] adjacentNodes;
		public Node(T item) {
			this.item = item;
		}
	}
}
