package com.crack.TreeGraph;

import sun.misc.Queue;

public class TreeSearch {
	static int count =0;
	/**DFS**/
	public  void depthFirstSearch(TreeNode<Integer> root, int itemToFind) {
		count++;
		if(root==null)
			return ;
		if(root.item.equals(itemToFind)) {
			System.out.println("DFS:"+root.item);
			return;
		}
		
		if(root.left!=null){
			depthFirstSearch(root.left, itemToFind);
		}
		if(root.right!=null){
			 depthFirstSearch(root.right, itemToFind);
		}
			

	
	}
	
	/**BFS**/
	public void breadthFirstSearch(TreeNode root, int itemToFind) {
		
		Queue<TreeNode> queue = new Queue<>();
		queue.enqueue(root);
		try{
			while(!queue.isEmpty()) {
				count++;
				TreeNode frontItem = queue.dequeue();
				if(frontItem.item.equals(itemToFind)) {
					System.out.println("BFS:"+ frontItem.item);
					return;
				}
				queue.enqueue(frontItem.left);
				queue.enqueue(frontItem.right);
			}
		}catch(Exception e) {
			
		}
		
	}
	
	
	public static void main(String[] args) {
		TreeNode<Integer> root = new TreeNode<>(10);
		root.left = new TreeNode<>(5);
		root.right = new TreeNode<>(20);
		root.left.left = new TreeNode<>(9);
		root.left.right = new TreeNode<>(18);
		root.right .left= new TreeNode<>(3);
		root.right .right= new TreeNode<>(7);
	
		new TreeSearch().depthFirstSearch(root, 20);
		System.out.println("DFS call stack count:"+ count);
		count =0;
		new TreeSearch().breadthFirstSearch(root, 20);
		System.out.println("BFS  call stack count::"  +count);
		
	}
	
}
