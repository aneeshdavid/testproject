package com.crack.TreeGraph;

import java.util.HashMap;

public class PathsWithSum {

  public static void main(String[] args) {
    TreeNode<Integer> root = new TreeNode<>(10);
    root.left = new TreeNode<>(5);
    root.right = new TreeNode<>(-3);
    root.left.left = new TreeNode<>(3);
    root.left.right = new TreeNode<>(2);
    root.left.right.right = new TreeNode<>(1);
    root.left.left.left= new TreeNode<>(3);
    root.left.left.right= new TreeNode<>(-2);
    
    root.right = new TreeNode<>(-3);
    root.right.right = new TreeNode<>(11);
    
    int targetSum = 8;

    int resultPath = findPaths(root, targetSum, 0);
    /*if(targetSum == pathSum) {
      System.out.println("Path: "+ path);
    }else {
      System.out.println("Path: "+ 0);
    }*/
    
   System.out.println("Path: "+ resultPath);
    
   
   resultPath = new PathsWithSum().countPath(root, targetSum, 0, new HashMap<Integer, Integer>());
   System.out.println("Path: "+ resultPath);
   
  }

  private static int findPaths(TreeNode<Integer> root, int targetSum, int pathSum) {
     if(root == null) {
        return 0;
     }
     int pathFromRoot = findPathsFromRoot(root,targetSum,0);
    
    int leftPath = findPaths(root.left, targetSum, pathSum);
    int rightPath = findPaths(root.right, targetSum, pathSum);
    
    return pathFromRoot+leftPath+rightPath;
   
  }

  private static int findPathsFromRoot(TreeNode<Integer> root, int targetSum, int pathSum) {
    if(root == null) {
      return 0;
   }
     pathSum +=  (Integer)root.item;
     int path = 0;
     if(pathSum == targetSum) {
       path++;
     }
     
     path+=findPathsFromRoot(root.left,targetSum, pathSum);
     path+=findPathsFromRoot(root.right,targetSum, pathSum);
     
    return path;
  }
  
  
  /**Method2**/
  
  private int countPath(TreeNode<Integer> root, int targetSum, int pathSum, HashMap<Integer, Integer> pathmap) {
    if(root==null)
      return 0;
    pathSum+=root.item;
    int path = pathmap.getOrDefault(pathSum-targetSum, 0);
    if(pathSum==targetSum) {
      path++;
    }
    updatePathMap(pathmap, pathSum, 1);
    path+=countPath(root.left, targetSum, pathSum, pathmap);
    path+=countPath(root.right, targetSum, pathSum, pathmap);
    //updatePathMap(pathmap, pathSum, -1);
    return path;
    
    
  }

  private void updatePathMap(HashMap<Integer, Integer> pathmap, int pathSum, int i) {
   int pathCount = pathmap.getOrDefault(pathSum, 0)+i;
   if(pathCount==0) {
     pathmap.remove(pathSum);
    }else {
      pathmap.put(pathSum, pathCount);
    }
   
   
  }

}
