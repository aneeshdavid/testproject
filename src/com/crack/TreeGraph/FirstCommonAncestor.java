package com.crack.TreeGraph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Stack;

import org.junit.Before;
import org.junit.Test;

public class FirstCommonAncestor {

  public static void main(String[] args) {
    TreeNode<Integer> root = new TreeNode(2);
    root.left = new TreeNode(50);
    root.left.left = new TreeNode(55);
    root.left.right = new TreeNode(90);
    
    root.right = new TreeNode(4);
    root.right.left = new TreeNode(87);
    root.right.right = new TreeNode(40);
    
    root.right.left.left = new TreeNode(20);
    root.right.left.right = new TreeNode(5);
    
    root.right.left.left.right = new TreeNode(3);
    
    
    TreeNode targetNode1 =  root.right.left.right;//55
    TreeNode targetNode2 = root.right.left.left.right;//3
    
    TreeNode commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    if(commonAncestor!=null) {
      System.out.println(commonAncestor.item);
    }else {
      System.out.println("No Common Ancestor found");
    }
  }

  private static TreeNode findFirstAncestor(TreeNode<Integer> root, TreeNode targetNode1, TreeNode targetNode2) {
     Stack<Integer> stack_1 = new Stack<>();
     Stack<Integer> stack_2 = new Stack<>();
     collectRootNodes(root,targetNode1, stack_1);
     collectRootNodes(root,targetNode2, stack_2);
     
     Stack<Integer> minStack = stack_1.size() > stack_2.size() ? stack_2 : stack_1;
     Stack<Integer> maxStack = stack_1.size() > stack_2.size() ? stack_1 : stack_2;
     
     Integer lastMached = null;
     
     while (!minStack.empty()) {
     
       Integer minStackVal = minStack.pop();
       Integer maxStackVal = maxStack.pop();
       if(minStackVal.equals(maxStackVal)) {
         lastMached = minStackVal;
       }else if(lastMached!=null){
         break;
       }
     
     }
     
     TreeNode<Integer> resultNode= null;
     if(lastMached!=null) {
       resultNode = new TreeNode(lastMached);
     }
     
    return resultNode;
  }

  private static boolean collectRootNodes(TreeNode<Integer> root, TreeNode targetNode, Stack<Integer> stack) {
    if(root==null ||targetNode == null)
      return false;
    
    if(root.item.equals(targetNode.item)) {
      stack.push(root.item);
      return true;
    }
    
    boolean left = collectRootNodes(root.left, targetNode, stack);
    boolean right = collectRootNodes(root.right, targetNode, stack);
    

    if(left||right) {
      stack.push(root.item);
      return true;
    }
    
    return false;
  }
  
  /**TEST METHODS*/
  
  TreeNode<Integer> root =null;
  
  @Before
  public void init() {
    root = new TreeNode(2);
    root.left = new TreeNode(50);
    root.left.left = new TreeNode(55);
    root.left.right = new TreeNode(90);
    
    root.right = new TreeNode(4);
    root.right.left = new TreeNode(87);
    root.right.right = new TreeNode(40);
    
    root.right.left.left = new TreeNode(20);
    root.right.left.right = new TreeNode(5);
    
    root.right.left.left.right = new TreeNode(3);
  }
  
  @Test
  public void testFirstAncestor() {

    TreeNode targetNode1 =  root.left.left;//55
    TreeNode targetNode2 = root.right.left.left.right ;//3
    TreeNode commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(2, commonAncestor.item);
    
    targetNode1 =  root.right.left.right;//5
    targetNode2 = root.right.left.left.right ;//3
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(87, commonAncestor.item);
    
    targetNode1 =  root.right.left.left;//5
    targetNode2 = root.right.right;//40
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(4, commonAncestor.item);
    
    targetNode1 =  root.left.right;//90
    targetNode2 = root.right.right;//40
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(2, commonAncestor.item);
    
    targetNode1 =  root.left.left;//90
    targetNode2 = root.right.right;//40
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(2, commonAncestor.item);
    
    targetNode1 =  root;//2
    targetNode2 = root;//2
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(2, commonAncestor.item);
    
    targetNode1 =  null;
    targetNode2 = root;//2
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertNull(commonAncestor);
    
    targetNode1 =  null;
    targetNode2 = null;
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertNull(commonAncestor);
    
    targetNode1 = root;//2
    targetNode2 = root;//2
    commonAncestor = findFirstAncestor(null,targetNode1, targetNode2);
    assertNull(commonAncestor);
    
    targetNode1 = root.right.left ;//87
    targetNode2 =  root.right.left.left;//20
    commonAncestor = findFirstAncestor(root,targetNode1, targetNode2);
    assertEquals(87, commonAncestor.item);
    
  }

}
