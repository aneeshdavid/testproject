package com.crack.TreeGraph;

import java.util.ArrayList;
import java.util.List;

class ValidateBST {

	public static void main(String[] args) {
		TreeNode<Integer> root = new TreeNode<>(20);
		root.left = new TreeNode<>(10);
		root.right = new TreeNode<>(30);
		root.left.left = new TreeNode<>(5);
		root.left.right = new TreeNode<>(15);
		root.left.right.right = new TreeNode<>(17);
		root.left.left.left = new TreeNode<>(3);
		root.left.left.right= new TreeNode<>(70);
		
		boolean isBST = isBinarySerachTree(root, null, null);
		System.out.println(isBST);
		
		System.out.println("BruteForce");
		isBST = isBinarySerachTreeBrutForce(root);
		System.out.println(isBST);
	}

	private static boolean  isBinarySerachTree(TreeNode<Integer> root, Integer min, Integer max) {
		if(root == null) {
			return true;
		}
		
		if((min!=null && min >= root.item) || (max!=null && max<root.item)) {
			return false;
		}
		
		if(!isBinarySerachTree(root.left, min , root.item) || !isBinarySerachTree(root.right, root.item,max)) {
			return false;
		}
		
		return true;
	}
	
	private static boolean isBinarySerachTreeBrutForce(TreeNode<Integer> root) {
		List<Integer> list   = new ArrayList<>();
		 binarySerachTreeBruteForceList(root, list);
		for (int i = 1; i < list.size(); i++) {
			if(list.get(i) <= list.get(i-1)) {
				return false;
			}
		}
		return true;
	}
	
	private static void  binarySerachTreeBruteForceList(TreeNode<Integer> root, List<Integer> list) {
		if(root==null) {
			return ;
		}
		binarySerachTreeBruteForceList(root.left, list);
		list.add(root.item);
		binarySerachTreeBruteForceList(root.right, list);
	}
	
	

}
