package com.crack.TreeGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class BuildOrder {
  List<Project> projectList = new ArrayList<>();
  public void addProjects(String[] projects) {
    for (String projectName : projects) {
      this.projectList.add(new Project(projectName));
    }
  }
  
  public void addDependencies(String[][] dependencies) {
    for (String[] dependency : dependencies) {
      String  project = dependency[1];
      String  depen= dependency[0];
      Project proj= this.projectList.stream().filter(pro->pro.projectName.equals(project)).findAny().orElse(null);
      if(proj!=null) {
        proj.addDependent(depen);
      }
    }
    
    
  }
  
  static class Project{
    String projectName;
    List<Project> dependencies = new ArrayList<>();

    public Project(String projectName) {
     this.projectName = projectName;
    }
    
    public void addDependent(String projectName) {
      dependencies.add(new Project(projectName));
    }

  }
  
  public static void main(String args[]) {
    
    String[] projects = new String[] {"a","b","c","d","e","f"};
    BuildOrder buildOrder = new BuildOrder();
    buildOrder.addProjects(projects);
    
    String[][] dependencies = new String[][] {{"a","d"},{"f","b"},{"b","d"},{"f","a"},{"d","c"}};
    buildOrder.addDependencies(dependencies);
    System.out.println("Completed Initial Setup");
    
    LinkedList<String> queue = new LinkedList<>();
    buildOrder.addIndependentProjects(buildOrder.projectList, queue);
    buildOrder.buildProjects(buildOrder.projectList, queue);
    for (String project : queue) {
      System.out.print(project+" ");
    }
  }

  private void addIndependentProjects(List<Project> projectList, LinkedList<String> queue) {
    List<Project> independentProjects = this.projectList.stream().filter(pro->pro.dependencies.isEmpty()).collect(Collectors.toList());
    for (Project project : independentProjects) {
      queue.add(project.projectName);
    }
  }

  private void buildProjects(List<Project> projectLists, LinkedList<String> queue) {
    
    for(Project project: projectLists) {
      if(project.dependencies.size()>0) {
        buildProjects(project.dependencies, queue);
      }
      
      if(!queue.contains(project.projectName)){
        queue.add(project.projectName);
      }
    }
    
  }
  
  
  
  
 
}
