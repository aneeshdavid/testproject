package com.crack.TreeGraph;

import java.util.Random;

public class RandomNodeBST {

  static class TreeNode{
    int item;
    TreeNode left;
    TreeNode right;
    int size;
    
    public TreeNode(int item) {
      this.item = item;
      this.size=1;
    }
    public TreeNode() {
      
    }
    
    public void insertNode(int item) {
      if(this.item<=item) {
        if(this.left==null) {
          this.left = new TreeNode(item);
        }else {
          this.left.insertNode(item);
        }
      }else {
        if(this.right==null) {
          this.right = new TreeNode(item);
        }else {
          this.right.insertNode(item);
        }
      }
      this.size++;
    }
    
    public TreeNode getRandomNode() {
      int leftSize = this.left!=null ? this.left.size : 0;
      Random random  = new Random();
      int index = random.nextInt(this.size);
      if(index < leftSize) {
        return this.left.getRandomNode();
      }else if(index == leftSize) {
        return this;
      }else {
        return this.right.getRandomNode();
      }
    }
  }
  
 
  public static void main(String[] args) {
    TreeNode treeNode = new TreeNode();
    treeNode.insertNode(20);
    treeNode.insertNode(10);
    treeNode.insertNode(30);
    treeNode.insertNode(15);
    treeNode.insertNode(5);
    treeNode.insertNode(3);
    treeNode.insertNode(7);
    treeNode.insertNode(17);
    treeNode.insertNode(30);
    treeNode.insertNode(35);
    
    
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    System.out.println(treeNode.getRandomNode().item);
    
    System.out.println(treeNode.getRandomNode().item);
    
    
  }

}
