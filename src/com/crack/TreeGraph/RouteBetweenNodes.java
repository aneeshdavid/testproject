package com.crack.TreeGraph;

import java.util.LinkedList;

import com.crack.TreeGraph.Graph.Node;

public class RouteBetweenNodes {

	public boolean hasRouteBetweenNodes(Graph<Integer> graph, Node<Integer> start, Node<Integer> end) {
	
		LinkedList<Node> queue = new LinkedList<>();
		queue.add(start);
		while(!queue.isEmpty()) {
			Node firstNode = queue.removeFirst();
			if(firstNode!=null) {
			for(Node n: firstNode.adjacentNodes)	{
				if(!firstNode.isVisited) {
					if(firstNode.equals(end)){
						return true;
					}else {
						n.isVisited = true;
						queue.add(n);
					}
				}
			}
			
		}
			
			
			
		}
		
		return false;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
