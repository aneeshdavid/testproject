package com.crack.TreeGraph;

import java.util.LinkedList;
import java.util.List;

public class BSTSequences {

  public static void main(String[] args) {
    
    TreeNode<Integer> root = new TreeNode<>(20);
    root.left = new TreeNode<>(10);
    root.right = new TreeNode<>(30);
    root.left.left = new TreeNode<>(5);
    root.left.right = new TreeNode<>(15);
    root.left.right.right = new TreeNode<>(17);
    root.left.left.left = new TreeNode<>(3);
    root.left.left.right= new TreeNode<>(7);
    
   List<LinkedList<Integer>> results = findBSTSequences(root);
    System.out.println("Sequence Started");
    for (LinkedList<Integer> result : results) {
      String seq= "";
      for (Integer sequences : result) {
        seq+=sequences+" ";
      }
      System.out.println(seq);
    }
    System.out.println("End");
  }

  private static List<LinkedList<Integer>> findBSTSequences(TreeNode<Integer> root) {
    LinkedList<LinkedList<Integer>> results = new LinkedList<LinkedList<Integer>>();
    if(root==null) {
      results.add(new LinkedList<>());
      return results;
    }
    LinkedList<Integer> prefixList= new LinkedList<>();
    prefixList.add(root.item);
    
    List<LinkedList<Integer>> leftList = findBSTSequences(root.left);
    List<LinkedList<Integer>> rightList = findBSTSequences(root.right);
    
    for (LinkedList<Integer> left : leftList) {
      for (LinkedList<Integer> right : rightList) {
        List<LinkedList<Integer>> mergedList = new LinkedList<>();
        doMerge(left, right, prefixList, mergedList);
        results.addAll(mergedList);
      }
    }
    
    return results;
  }

  private static void doMerge(LinkedList<Integer> left, LinkedList<Integer> right, LinkedList<Integer> prefixList, List<LinkedList<Integer>> mergedList) {
    if(left.isEmpty() || right.isEmpty()) {
      LinkedList<Integer> result = new LinkedList<>();
      result.addAll(prefixList);
      result.addAll(left);
      result.addAll(right);
      mergedList.add(result);
      return;
    }
    

      Integer leftFirst = left.removeFirst();
      prefixList.addLast(leftFirst);
      doMerge(left,right,prefixList, mergedList);
      left.addFirst(leftFirst);
      prefixList.removeLast();


      Integer rightFirst = right.removeFirst();
      prefixList.addLast(rightFirst);
      doMerge(left,right,prefixList, mergedList);
      right.addFirst(rightFirst);
      prefixList.removeLast();

    
  }

}
