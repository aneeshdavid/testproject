package com.crack.BitManupulation;

public class BinaryToString {

  public static void main(String[] args) {
   String binaryToString = new BinaryToString().convert(0.101);
   System.out.println(binaryToString);
  }
  
  private String convert(double num) {
    if(num>=1 || num<=0) {
      return "ERROR";
    }
    StringBuilder binary = new StringBuilder();
    binary.append(".");
    
    while(num > 0) {
      
      if(binary.length()>=32) {
        return "ERROR";
      }
      
      double r = num*2;
      
      if(r>=1) {
        binary.append("1");
        num = r-1;
      }else {
        binary.append("0");
        num=r;
      }
      
    }
    
    return binary.toString();
  }

}
