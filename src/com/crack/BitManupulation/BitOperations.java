package com.crack.BitManupulation;

public class BitOperations {
  
  boolean getBit(int num, int i) {
    boolean result = false;
    
    int leftShift = 1 << i;
    System.out.println("leftShift: "+leftShift);
 
    int andOper = num & leftShift;
    System.out.println("andOper: "+andOper);
    
    result = (andOper != 0);
    
    return result;
  }
  
  
  int setBit(int num, int i) {
    int leftShift = (1 << i);
    System.out.println("leftShift: "+leftShift);
 
    int orOper = num | leftShift;
    System.out.println("orOper: "+orOper);
    return orOper;
  }

  public static void main(String[] args) {
    BitOperations bitOper = new BitOperations();
    System.out.println(bitOper.getBit(10, 5));
    
    //System.out.println(bitOper.setBit(10, 5));
  }

}
