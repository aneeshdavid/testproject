package com;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author adavid
 *
 * May 30, 2018
 */

public class TestClass {

	public static void main(String a[]) {
		
		
		    
		    Set<Product> lis1 =new LinkedHashSet<Product>();
		    Product p = new Product();
		    p.setId(1L);
		    lis1.add(p);
		    p = new Product();
		    p.setId(2L);
		    lis1.add(p);
		    p = new Product();
		    p.setId(3L);
		    lis1.add(p);
		    p = new Product();
		    p.setId(4L);
		    lis1.add(p);
		    
		    List<Long> lis = new ArrayList<>();
		    lis.add(1L);
		    lis.add(2L);
		    printList(lis1);
		    
		    Set<Product> result = lis1.stream().filter(pe -> !lis.contains(pe.getId())).collect(Collectors.toSet());
		    System.out.println("After Filter");
		    printList(result);


	}
	private static void printList(Set<Product> resList) {
	for (Product product : resList) {
    System.out.println(product.getId());
  }
	}
}

