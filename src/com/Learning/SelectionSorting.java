package com.Learning;

import java.util.Arrays;

public class SelectionSorting {

	public static void main(String[] args) {
		int arr[] = {10,2,4,9,3};
		selectionSort(arr);

	}
	
	private static void selectionSort(int[] arr) {
		
		for(int i=0;i<arr.length;i++) {
			int index = i;
			for(int j=i+1;j<arr.length;j++) {
				if(arr[index] > arr[j]) {
					index = j; 
				}
			}
			
			if(index!=i) {
				int temp = arr[index];
				arr[index] = arr[i];
				arr[i]  = temp;
			}
		}
		System.out.println(Arrays.toString(arr));
		
	}

}
