package com.Learning;

/**
 * Find fibonacci sequence of input n 
 * */
public class Recursion_DP {

	public static void main(String args[]) {
		//Brute Force
		int input = 35;
		long startTime = System.currentTimeMillis();
		System.out.println("Brute Force -> input :"+input + "Answer:"+fib_BF(input));
		System.out.println("Time Taken:"+ (System.currentTimeMillis()- startTime));
		System.out.println("########################");
		startTime = System.currentTimeMillis();
		long[] memoizationArr = new long[input+1];
		System.out.println("Memoization -> input :"+input + "Answer:"+fib_memoization(input, memoizationArr));
		System.out.println("Time Taken:"+ (System.currentTimeMillis()- startTime));
		System.out.println("########################");
		startTime = System.currentTimeMillis();
		System.out.println("Bottom-Up -> input :"+input + "Answer:"+fib_BottomUp(input));
		System.out.println("Time Taken:"+ (System.currentTimeMillis()- startTime));
		

	}
	
	/**Brut force approach**/
	
	private static long fib_BF(int n){
		if(n==0)
			return 0;
		if(n==1||n==2)
			return 1;
		
		return fib_BF(n-1)+fib_BF(n-2);
		
	}
	
	
	/** Memoization approach **/
	
	private static long fib_memoization(int n, long[] memoizationArr ) {
		
		if(memoizationArr[n] != 0) {
			  return memoizationArr[n];
		  }
		
		if(n==0)
			return 0;
		if(n==1||n==2)
			return 1;
	  
	  long ans = fib_memoization(n-1, memoizationArr)+fib_memoization(n-2, memoizationArr);
	  memoizationArr[n] = ans;
	  return ans;
		
	}
	
	/**Bottom-Up Approach**/
	private static long fib_BottomUp(int n) {
		if(n==0)
			return 0;
		if(n==1||n==2)
			return 1;
		
		long[] memoizationArr = new long[n+1];
		 memoizationArr[1] = 1;
		 memoizationArr[2] = 1;
		
		for(int i=3;i<=n;i++) {
			memoizationArr[i] = memoizationArr[i-1]+ memoizationArr[i-2];
		}
		return memoizationArr[n];
	}
	
	
}
