package com.Learning;

public class Permutation {

	public static void main(String args[]) {
		 String s = "GOD"; 
	       printPermutn(s, ""); 
	       System.out.println("========");
		 perm(s,"");
	}
	
	 static void printPermutn(String str, String ans) 
	    { 
	  
	        // If string is empty 
	        if (str.length() == 0) { 
	            System.out.println(ans + " "); 
	            return; 
	        } 
	  
	        for (int i = 0; i < str.length(); i++) { 
	  
	            // ith character of str 
	            char ch = str.charAt(i); 
	  
	            // Rest of the string after excluding  
	            // the ith character 
	            String ros = str.substring(0, i) +  str.substring(i + 1); 
	      	  
	            // Recurvise call 
	            printPermutn(ros, ans + ch); 
	        } 
	    } 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 private static void perm(String str, String ans) {
		 
		 if(str.length()==0) {
			 System.out.println(ans);
			 return;
		 }
		 
		 for(int i =0; i<str.length();i++) {
			 char ch = str.charAt(i);
			 String remainingString = str.substring(0,i) + str.substring(i+1);
			 perm(remainingString, ans+ch);
		 }
		 
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
}
