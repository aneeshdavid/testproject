package com.Learning;

public class MyQueue<T> {
  Object queueArray[];
  int front = 0;
  int rear = -1;
	
	public MyQueue() {
		queueArray = new Object[5];
	}
	
	private void enQueue(T ele) {
		rear++;
		queueArray[rear] = ele;
	}
	
	private void deQueue() {
		if(rear!=-1) {
			front++;
		}
	}
	
	private boolean isEmpty() {
		if(rear==-1) {
			return true;
		}
		return false;
	}
	
	private void printQueue() {
		for(int i =front; i<=rear;i++) {
			System.out.println(queueArray[i]);
		}
	}

	
	public static void main(String[] args) {
		MyQueue<String> queue = new MyQueue<>();
		queue.enQueue("A");
		queue.enQueue("B");
		queue.enQueue("C");
		queue.printQueue();
		queue.deQueue();
		System.out.println("After Dequeue");
		queue.printQueue();
	}
}
