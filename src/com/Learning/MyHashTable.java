package com.Learning;

import java.util.LinkedList;

public class MyHashTable<K,V> {
	static LinkedList hashArray[] ;
	int size = 0;
	public MyHashTable() {
		hashArray = new LinkedList[10];
	}
	
	private void put(Object key, Object value) {
		int hashIndex = getHashIndex(key);
		Entry entry = new Entry(key, value);
		if(hashArray[hashIndex] == null) {
			LinkedList<Entry> list = new LinkedList<>();
			list.add(entry);
			hashArray[hashIndex] = list;
			size++;
		}else {
			LinkedList<Entry> list = hashArray[hashIndex];
			Entry ext_entry = getEntry(key);
			if(ext_entry!= null) {
				list.remove(ext_entry);
				list.add(entry);
			}else {
				list.add(entry);
				size++;
			}
		}
	}
	
	private Object get(Object key) {
		Entry entry = getEntry(key);
		if(entry != null) {
		return getEntry(key).value;
		}else {
			return null;
		}
	}
	
	private static Entry getEntry(Object key) {
		if(key == null)
			return null;
		
		int hashIndex = getHashIndex(key);
		if(hashArray[hashIndex] == null) {
			return null;
		}
		
		LinkedList<Entry> list = hashArray[hashIndex];
		for (Entry entry : list) {
			if(entry.getKey().equals(key)) {
				return entry;
			}
		}
		return null;
		
	}
	
	private void remove(Object key) {
		Entry ext_entry = getEntry(key);
		if(ext_entry != null) {
			int hashIndex = getHashIndex(key);
			LinkedList<Entry> list = hashArray[hashIndex];
			Entry toBeRemoved = null;
			for (Entry entry : list) {
				if(entry.getKey().equals(key)) {
					toBeRemoved = entry;
				}
			}
			if(toBeRemoved != null) {
				list.remove(toBeRemoved);
			}
		}
	}
	
	private Integer getSize() {
		return size;
	}
	
	private static Integer getHashIndex(Object key) {
		if(key == null)
			return null;
		
		return key.hashCode() % 10;
	}
	
	
	
	
	static class Entry{
		Object key;
		Object value;
		
		public Entry(Object key, Object value) {
			this.key = key;
			this.value = value;
		}
		public Object getKey() {
			return key;
		}
		public void setKey(Object key) {
			this.key = key;
		}
		public Object getValue() {
			return value;
		}
		public void setValue(Object value) {
			this.value = value;
		}
		
	}
	
	
	
	public static void main(String args[]){
		MyHashTable<String,String> map = new MyHashTable<>();
		System.out.println("Size:"+map.getSize());
		map.put("A", "Apple");
		System.out.println("Size:"+map.getSize());
		System.out.println("Get A:"+ map.get("A"));
		map.put("B", "Banana");
		System.out.println("Size:"+map.getSize());
		System.out.println("Get B:"+ map.get("B"));
		map.put("A", "Apple");
		map.put("B", "Ball");
		System.out.println("Get B:"+ map.get("B"));
		map.remove("B");
		System.out.println("Get B:"+ map.get("B"));
	}
	
}
