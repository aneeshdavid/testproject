package com.Learning;

import java.util.Arrays;

public class BubbleSorting {

	public static void main(String[] args) {
		int arr[] = {10,2,4,9,3};
		bubbleSort(arr);

	}
	
	private static void bubbleSort(int[] arr) {
		
		int len = arr.length;
		int temp = 0;
		
		for(int i =0;i<len;i++) {
			for(int j =0;j<len-1-i;j++) {
				if(arr[j]>arr[j+1]) {
					temp = arr[j+1];
					arr[j+1] = arr[j];
					 arr[j]= temp;
				}
			}
		}
		
		
		System.out.println(Arrays.toString(arr));
	}

}
