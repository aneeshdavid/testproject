package com.Learning;

public class RotateArray90InPlace {

	public static void main(String[] args) {
		 // NOTE: The following input values will be used for testing your solution.
        int a1[][] = {{1, 2, 3},
                      {4, 5, 6},
                      {7, 8, 9}};
        new RotateArray90InPlace().rotate90(a1, 3);
        //rotate(a1, 3);
        printArray(a1,3);
        //should return:
        // [[7, 4, 1],
        //  [8, 5, 2],
        //  [9, 6, 3]]

        int a2[][] = {{1, 2, 3, 4},
                      {5, 6, 7, 8},
                      {9, 10, 11, 12},
                      {13, 14, 15, 16}};
      
        System.out.println("==============");
         new RotateArray90InPlace().rotate90(a2, 4);
        //rotate(a2, 4);
         printArray(a2,4);
        //should return:
        // [[13, 9, 5, 1],
        //  [14, 10, 6, 2],
        //  [15, 11, 7, 3],
        //  [16, 12, 8, 4]]
	}
	
	private  int[][] rotate90(int arr[][], int len){
		for(int i=0;i<len/2+len%2;i++) {
			for(int j=0;j<len/2;j++) {
				int current_i = i;
				int current_j = j;
				int [] tempArr = new int[4];
				for(int k=0;k<=3;k++) {
					tempArr[k] = arr[current_i][current_j];
					int[] nextCordinates = getNextCordinates(current_i, current_j, len);
					current_i = nextCordinates[0];
					current_j = nextCordinates[1];
				}
				for(int k=0;k<=3;k++) {
					arr[current_i][current_j] = tempArr[(k+3)%4];
					int[] nextCordinates = getNextCordinates(current_i, current_j, len);
					current_i = nextCordinates[0];
					current_j = nextCordinates[1];
				}
			}
		}
		return arr;
	}
	
	private static int[] getNextCordinates(int i, int j, int length) {
		int[] newCordinates = new int[2];
		newCordinates[0] = j;
		newCordinates[1] = length-1-i;
		return newCordinates;
	}
	
	
	private static void printArray(int arr[][], int len) {
		
		for(int i=0;i<len;i++) {
			String arrStr ="[";
			for(int j=0;j<len;j++) {
				arrStr+=arr[i][j]+" ";
			}
			arrStr +="]";
			System.out.println(arrStr);
		}
	}
	
	
	
	
	
	
	
	////////////////////////
	
	public static int[][] rotate(int[][] a, int n) {
        // n/2 gives us floor(n/2)
        // and n/2 + n%2 gives us ceiling(n/2)
        for (int i = 0; i < n / 2 + n % 2; i++) {
            for (int j = 0; j < n / 2; j++) {
                int[] tmp = new int[4];
                int currentI = i;
                int currentJ = j;
                for (int k = 0; k < 4; k++) {
                    tmp[k] = a[currentI][currentJ];
                    int[] newCoordinates = rotateSub(currentI, currentJ, n);
                    currentI = newCoordinates[0]; currentJ = newCoordinates[1];
                }
                for (int k = 0; k < 4; k++) {
                    a[currentI][currentJ] = tmp[(k + 3) % 4];
                    int[] newCoordinates = rotateSub(currentI, currentJ, n);
                    currentI = newCoordinates[0]; currentJ = newCoordinates[1];
                }
            }
        }
        return a;
    }

    public static int[] rotateSub(int i, int j, int n) {
        int[] newCoordinates = new int[2];
        newCoordinates[0] = j;
        newCoordinates[1] = n - 1 - i;
        return newCoordinates;
    }

}
