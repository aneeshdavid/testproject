package com.Learning;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LongestSubstring {

	public static void main(String[] args) {
		
		//int len = new LongestSubstring().lengthOfLongestSubstring(" ");
		int len = new LongestSubstring().lengthOfLongestSubstring_SlidingWindow(" ");

		System.out.println(len);
	}
	//Brute Force
	public int lengthOfLongestSubstring(String str) {
        int longestLength = 0;
        for(int i =0;i<str.length();i++){
            for(int j =i+1;j<=str.length();j++){
	            if(!hasDuplicate(str,i,j)){
	                longestLength = Math.max(longestLength, j-i);
	            }
            }
        }
        
        return longestLength;
    }
    
    private boolean hasDuplicate(String str, int start, int end){
        Set<Character> charSet = new HashSet<>();
    	for(int i =start;i<end;i++){
          if(charSet.contains(str.charAt(i))) {
        	 return true; 
          }else {
        	  charSet.add(str.charAt(i));
          }
        }
        return false;
    }
//###########################
    
    //Sliding window "abcabcbb"
    
    public int lengthOfLongestSubstring_SlidingWindow(String s) {
        int n = s.length(), ans = 0;
        Map<Character, Integer> map = new HashMap<>(); // current index of character
        // try to extend the range [i, j]
        for (int j = 0, i = 0; j < n; j++) {
            if (map.containsKey(s.charAt(j))) {
                i = Math.max(map.get(s.charAt(j)), i);
            }
            ans = Math.max(ans, j - i+1);
            map.put(s.charAt(j), j+1);
        }
        return ans;
    }
    
}
