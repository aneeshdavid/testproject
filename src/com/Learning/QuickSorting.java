package com.Learning;

import java.util.Arrays;

public class QuickSorting {

	public static void main(String[] args) {
		int arr[] = {1,50,30,10,60,80};
		quickSort(arr,0,arr.length-1);
		System.out.println(Arrays.toString(arr));

	}
	
	private static void quickSort(int[] arr, int low, int high) {
		if(low>high)
			return;
		
		int mid = low+(high-low)/2;
		int pivot = arr[mid];
		int i = low;
		int j = high;
		while(i<=j) {
			while(arr[i]<pivot) {
				i++;
			}
			while(arr[j]>pivot) {
				j--;
			}
			if(i<=j) {
				int temp = arr[j];
				arr[j] = arr[i];
				arr[i] = temp;
				i++;
				j--;
			}
		}
		
		if(low<j) {
			quickSort(arr,low,j);
		}
		if(high>i) {
			quickSort(arr,i,high);
		}
		
		
	}

}
