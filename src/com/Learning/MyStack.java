package com.Learning;

public class MyStack<T> {
	
	Object[] stackArr ;
	int top = -1;
	
	public MyStack(){
		stackArr  = new Object[5];
	}
	
	private void push(T obj) {
		if(top<4) {
		top++;
		stackArr[top] = obj;
		}else {
			System.out.println("Stack is full");
		}
	}
	
	private void pop() {
		if(top>=0) {
			top--;
		}
	}
	
	private void printStack() {
		for(int i=0;i<=top;i++) {
			System.out.println(stackArr[i]);
		}
	}
	
	
	
	public static void main(String args[]) {
		MyStack<String> stack = new MyStack<>();
		stack.push("A");
		stack.push("B");
		stack.push("C");
		//stack.printStack();
		//System.out.println("After Pop");
		//stack.pop();
		stack.push("D");
		stack.push("E");
		
		stack.push("F");
		stack.printStack();
	}

}
