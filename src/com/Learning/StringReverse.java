package com.Learning;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class StringReverse {

	public static void main(String args) {
		
	}
	
	public static String reverse(String str) {
		if(str == null || str.isEmpty()) {
			return str;
		}
		char[] strChar = str.toCharArray();
		int i = 0;
		int j = str.length() - 1;
		while(i<j) {
			swap(strChar, i ,j);
			i++;
			j--;
		}
		
		return new String(strChar);
		
	}

	private static void swap(char[] strChar, int i, int j) {
		char temp = strChar[i];
		strChar[i] = strChar[j];
		strChar[j] = temp;
	}
	
	@Test
	public void reverseEmptyString() {
		assertEquals("", reverse(""));
	}
	
	@Test
	public void reverseString() {
		assertEquals("cba", reverse("abc"));
	}
	
	@Test
	public void reverseAnagramString() {
		assertEquals("mary", reverse("yram"));
	}
	
	@Test
	public void reverseSameString() {
		assertEquals("aaa", reverse("aaa"));
	}
	
	@Test
	public void reversePalindromeString() {
		assertEquals("aba", reverse("aba"));
	}
	
}
