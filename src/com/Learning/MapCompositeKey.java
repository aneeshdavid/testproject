package com.Learning;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

public class MapCompositeKey {

	
	static class CompositeKey{
		List<Object> objList;
		public  CompositeKey(Object...objects) {
			objList = new ArrayList<>();
			for (Object obj : objects) {
				objList.add(obj);
			}
		}
		
		@Override
		public int hashCode() {
			int hashCode =0;
			for (Object object : objList) {
				hashCode+=object.hashCode();
			}
			return hashCode;//this.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			/*int thisHashCode =0;
			for (Object object : objList) {
				thisHashCode+=object.hashCode();
			}
			
			for (Object object : objList) {
				thisHashCode+=object.hashCode();
			}*/
			
			if(this== obj)
				return true;
			if(obj==null || obj.getClass()!=getClass())
				return false;
		
			CompositeKey key = ( CompositeKey)obj;
			
			return this == key;
			
		}
	}
	
	
	
	public static void main(String[] args) {
		
	}
	
	@Test
	public void testGetMap() {
		Map<CompositeKey, String> map= new HashMap<>();
		CompositeKey key1 = new CompositeKey("A","Apple", new Date(2));
		map.put(key1, "value_01");
		
		assertEquals("value_01", map.get(new CompositeKey("A","Apple", new Date(2))));
		
	}

}
