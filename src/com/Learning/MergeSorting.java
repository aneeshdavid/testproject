package com.Learning;

import java.util.Arrays;

public class MergeSorting {

	public static void main(String[] args) {
		int arr[] = {1,50,30,10,60,80};
		mergeSort(arr,0,arr.length-1);
		System.out.println(Arrays.toString(arr));

	}
	
	private static void mergeSort(int[] arr, int low, int high) {
		if(low<high)
		{
			int mid = low+(high-low)/2;
			
			mergeSort(arr, low, mid);
			mergeSort(arr, mid+1, high);
			
			merge(arr,low,mid,high);
		}
		
	}

	private static void merge(int[] arr, int low, int mid, int high) {
		int l_len = mid-low+1;
		int r_len = high-mid;
		int[] arr_l = new int[l_len];
		int[] arr_r = new int[r_len];
		
		for(int i=0; i<l_len; i++) {
			arr_l[i] = arr[low+i];
		}
		for(int j=0; j<r_len; j++) {
			arr_r[j] = arr[mid+1+j];
		}
		
		int i = 0;
		int j= 0;
		int k = low;
		while(i<l_len && j<r_len) {
			if(arr_l[i] <= arr_r[j]) {
				arr[k] = arr_l[i];
				i++;
			}else {
				arr[k] = arr_r[j];
				j++;
			}
			k++;
		}
		
		while(i<l_len) {
			arr[k] = arr_l[i];
			i++;
			k++;
		}
		
		while(j<r_len) {
			arr[k] = arr_r[j];
			j++;
			k++;
		}
		
	}

}
