package com.Learning;

public class BinarySearch {

	public static void main(String[] args) {
		int arr [] = {1,2,3,4,5,6,7,8};
		
		find(arr,11);
		
	}

	private static void find(int[] arr, int ele) {
		
		int low = 0;
		int high = arr.length-1;
		int mid = 0;
		boolean isFound = false;
		while(!isFound) {
			if(low>high)
				break;
			
			//mid = low+(high-low)/2;
			/**Interpolation Search**/
			mid = Math.round(low+((high-low)/(arr[high]-arr[low]))*ele-arr[low]);
			if(mid >= arr.length)
				break;
			System.out.println("Low:"+low +" High:"+high);
			if(arr[mid] == ele) {
				isFound = true;
				break;
			}
			if(arr[mid]<ele) {
				low = mid+1;
			}
			
			if(arr[mid]>ele) {
				high = mid-1;
			}
		}
		
		if(!isFound) {
			System.out.println("Not Found");
		}else {
			System.out.println("Found");
		}
		
	}

}
