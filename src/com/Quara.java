package com;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * @author adavid
 *
 * Feb 26, 2019
 */

public class Quara {

  public static boolean possibleCombination(List<Integer> skillPoints){
    int i = skillPoints.size()-2;
    while(i>=0 && skillPoints.get(i)>=skillPoints.get(i+1))
    i--;

    if(i<0){
    return false;
    }
    int j = skillPoints.size()-1;
    while(skillPoints.get(i) >= skillPoints.get(j))
    j--;

    Collections.swap(skillPoints, i, j);
    Collections.reverse(skillPoints.subList(i+1, skillPoints.size()));
    return true;
}
private static int getSum( List<Integer> skillPoints){
     Integer sum=0;
   // skillPoints.forEach(point -> sum=+point);
    for(Integer point : skillPoints){
        sum=+point;
    }
    return sum;
}
public static void main(String args[] ) throws Exception {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
  //‎⁨/Users/adavid/Desktop/input000.txt
    Scanner input =new Scanner( new File("/Users/adavid/Desktop/input000.txt"));
    
       int numberOfStudents =  Integer.parseInt(input.nextLine());
       
        List<Integer> skillPoints =Arrays.asList(input.nextLine().toString().split(" ")).stream().map          (p->Integer.parseInt(p)).collect(Collectors.toList());
  
        int k = Integer.parseInt(input.nextLine());

int possibleWays = 0;

do{
    //logic to meet the total points K
    if(getSum(skillPoints) >=  k){
            possibleWays++;
    }
    

}while(possibleCombination(skillPoints));

System.out.println("possibleWays: "+ possibleWays);
}
}

