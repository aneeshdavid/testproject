package com.leet;

public class ReverseWords {

	 public String reverseWords(String s) {
	        String[] strArr = s.trim().split(" ");
	        String result="";
	        for(int i=strArr.length-1; i>=0; i-- ){
	            result +=" "+strArr[i];
	        }
	        System.out.println(result);
	        return result;
	    }
	
	 
	 public static void main(String args[]) {
		 String s = "a good   example";
		 new ReverseWords().reverseWords(s);
	 }
}
