package com.leet;

public class LongestPalindromicSubstring {

	public String longestPalindrome(String str) {
		int max = 0;
		String longestPalindrome = null;
		
		if(str==null || str.equals(""))
			return "";
		if(str.length()==1)
			return str;
		
		for (int i = 0; i < str.length(); i++) {
			for (int j = i+1; j< str.length(); j++) {
				String sub = str.substring(i,j);
				if(isPalindrome(sub)) {
					if(sub.length()>max) {
						max = sub.length();
						longestPalindrome = sub;
					}
				}
			}
		}
		return longestPalindrome;
	}
	private boolean isPalindrome(String sub) {
		return sub.equals(getReverse(sub));
	}
	private Object getReverse(String sub) {
		String reverse = "";
		for (int i = sub.length()-1; i >= 0; i--) {
			reverse+=sub.charAt(i);
		}
		return reverse;
	}
	public static void main(String[] args) {
		String pal = new LongestPalindromicSubstring().longestPalindrome("ac");
		System.out.println(pal);
	}

}
