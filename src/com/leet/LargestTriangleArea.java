package com.leet;

public class LargestTriangleArea {

	public static void main(String[] args) {
		int[][] points = new int[][] {{0,0},{0,1},{1,0},{0,2},{2,0}};
		//int[][] points = new int[][] {{1,0},{0,0},{0,1}};
		double maxArea = new LargestTriangleArea().largestTriangleArea(points);
		System.out.println(maxArea);
	}
	
	public double largestTriangleArea(int[][] points) {
        double maxArea = 0.0;
        if(points==null || points.length==0)
        	return maxArea;
        for (int i = 0; i < points.length; ++i) {
			for (int j = i+1; j < points.length; ++j) {
				for (int k = j+1; k < points.length; ++k) {
					maxArea = Math.max(maxArea, findArea(points[i], points[j], points[k]));
				}
			}
		}
        return maxArea;
    }

	private double findArea(int[] i, int[] j, int[] k) {
		double area = 0.0;
		area = Math.abs((j[0]-i[0]) *  (k[1]-i[1])  -   (k[0]-i[0]) *( j[1]-i[1]) );
		return area * 0.5;
	}

}
