package com.leet;
/**
 * Example 1:
Input: [1,null,0,0,1]
Output: [1,null,0,null,1]
 
Explanation: 
Only the red nodes satisfy the property "every subtree not containing a 1".
The diagram on the right represents the answer.


Example 2:
Input: [1,0,1,0,0,0,1]
Output: [1,null,1,null,1]



Example 3:
Input: [1,1,0,1,1,0,1,0]
Output: [1,1,0,1,1,null,1]



 * @author adavid
 *
 */
public class BinaryTreePruning {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(1);
		root.right	= new TreeNode(0);
		root.right.left = new TreeNode(0);
		root.right.right = new TreeNode(1);
		TreeNode outRoot = new BinaryTreePruning().pruneTree(root);
		System.out.println("success");
	}
	
	public TreeNode pruneTree(TreeNode root) {
       if(root == null)
    	   return root;
       
		if(root.left!=null) {
			root.left = pruneTree(root.left);
			
        }
        if(root.right!=null) {
        	root.right = pruneTree(root.right);
        }
        
        if(root.left == null & root.right == null && root.val==0) {
        	root = null;
        }
        
        return root;
    }
	
	static class TreeNode {
		   int val;
		   TreeNode left;
		   TreeNode right;
		   
		   TreeNode(int x) {
			   val = x;
			}
		 }

}
