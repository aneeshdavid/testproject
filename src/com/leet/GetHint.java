package com.leet;

public class GetHint {
	public static void main(String args[]) {
		String s = new GetHint().getHint("1123","0111");
		System.out.println(s);
	}
	
	public String getHint(String secret, String guess) {
        int b = 0;
        int c = 0;
         int length = secret.length();
        
        for(int i = 0;i<length;i++){
            if(secret.charAt(i)==guess.charAt(i)){
                b++;
                // System.out.println(b);
            }
        }
        
        c = length - b;
        
       // System.out.println(b+"A"+c+"B");
        String result = b+"A"+c+"B";
        return result;
        
    }
}
