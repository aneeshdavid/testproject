package com.leet;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class GenerateParentheses {

	Stack<String> value = new Stack<>();
	List<String> stringList = new ArrayList<>();
	int n;
	public static void main(String[] args) {
		new GenerateParentheses().generateParenthesis(3);

	}

	
	public List<String> generateParenthesis(int n) {
        this.n =n;
       //generateParenthesis(0, 0);
        generateParenthesis(n, n, "");
       System.out.println(stringList.toString());
       return stringList;
   }
	
	
	private void generateParenthesis(int left, int right, String paranthesis) {
		
		if(left == 0 && right == 0) {
			stringList.add(paranthesis);
			return;
		}
		
		if(left>0) {
			generateParenthesis(left-1, right, paranthesis+"(");
		}
		
		if(right>left) {
			generateParenthesis(left, right-1, paranthesis+")");
		}
		
	}
	
	
	
	
	
private void generateParenthesis(int ob, int cb) {
       if (cb == n) {
           stringList.add(String.join("", value));
           return;
       }
       if (ob > cb) {
           value.add(")");
           generateParenthesis( ob, cb + 1);
           value.pop(); //backtracking
       }
       if (n > ob) {
           value.add("(");
           generateParenthesis(ob + 1, cb);
           value.pop(); //backtracking
       }
   }
}
