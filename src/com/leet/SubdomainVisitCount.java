package com.leet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A website domain like "discuss.leetcode.com" consists of various subdomains. At the top level, we have "com", at the next level, we have "leetcode.com", and at the lowest level, "discuss.leetcode.com". When we visit a domain like "discuss.leetcode.com", we will also visit the parent domains "leetcode.com" and "com" implicitly.

Now, call a "count-paired domain" to be a count (representing the number of visits this domain received), followed by a space, followed by the address. An example of a count-paired domain might be "9001 discuss.leetcode.com".

We are given a list cpdomains of count-paired domains. We would like a list of count-paired domains, (in the same format as the input, and in any order), that explicitly counts the number of visits to each subdomain.

Example 1:
Input: 
["9001 discuss.leetcode.com"]
Output: 
["9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com"]
Explanation: 
We only have one website domain: "discuss.leetcode.com". As discussed above, the subdomain "leetcode.com" and "com" will also be visited. So they will all be visited 9001 times.

Example 2:
Input: 
["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]
Output: 
["901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com"]
Explanation: 
We will visit "google.mail.com" 900 times, "yahoo.com" 50 times, "intel.mail.com" once and "wiki.org" 5 times. For the subdomains, we will visit "mail.com" 900 + 1 = 901 times, "com" 900 + 50 + 1 = 951 times, and "org" 5 times.

 * @author adavid
 *
 */
public class SubdomainVisitCount {
	  Map<String, Integer> map = new HashMap<>();
	public static void main(String[] args) {
		String[] cpDomainArray = {"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"};
		List<String> result = new SubdomainVisitCount().subdomainVisits(cpDomainArray);
		System.out.println(result.toString());
	}
	
	 public List<String> subdomainVisits(String[] cpdomains) {
		 for (String cpDomain : cpdomains) {
			 int count  =Integer.parseInt (cpDomain.split(" ")[0]);
			String domain = cpDomain.split(" ")[1];
			calculateDomain(domain, count);
		}
		 
		 List<String> resultList = new ArrayList<>();
		 map.forEach((k,v) ->{
			 resultList.add(v+" "+k);
		 });
		 return resultList;
	 }
	 
	 private void calculateDomain(String domain, int count) {
		 addToMap(domain, count);
		 if(domain.indexOf('.')>=0) {
			 String subDomain = domain.substring(domain.indexOf('.')+1) ;
			 calculateDomain(subDomain, count);
		 }
	 }

	private void addToMap(String domain, int count) {
		if(map.containsKey(domain)) {
			map.put(domain, map.get(domain)+count);
		}else {
			map.put(domain, count);
		}
	}
}
