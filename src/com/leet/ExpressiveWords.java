package com.leet;
/**
 * Example:
Input: 
S = "heeellooo"
words = ["hello", "hi", "helo"]
Output: 1
Explanation: 
We can extend "e" and "o" in the word "hello" to get "heeellooo".
We can't extend "helo" to get "heeellooo" because the group "ll" is not size 3 or more.
 * @author adavid
 *
 */
public class ExpressiveWords {

	public static void main(String[] args) {
		//String str = "heeellooo";
		//String[] words = {"hello", "hi", "helo"};
		String str = "aaa";
		String[] words = {"aaaa"};
		int count = expressiveWords(str, words);
		System.out.println(count);
	}
	
	public static int expressiveWords(String str, String[] words) {
		int ans = 0;
		int[] strCharMap = getCharMap(str);
		
		for(String word: words) {
			boolean isMatched = true;
			int[] wordCharMap = getCharMap (word);
			if(strCharMap.length!=wordCharMap.length)
				continue;
			
			for(int i=0;i<wordCharMap.length; i++) {
				if(wordCharMap[i]==0)
					continue;
				if(wordCharMap[i]==strCharMap[i] ||  (strCharMap[i]>=3 && wordCharMap[i] <= strCharMap[i] )) {
					
				}else {
					isMatched = false;
					break;
				}
			}
			
			if(isMatched)
				ans++;
		}
		
		return ans;
	}

	private static int[] getCharMap(String str) {
		int[] charMap = new int[26];
		for(int i=0;i<str.length(); i++) {
			int pos =(str.charAt(i) - 'a') ;
			charMap[pos] = charMap[pos] +1;
		}
		return charMap;
	}

}
