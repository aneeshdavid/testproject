package com.leet;

public class NumberToWords {

	public static void main(String args[]) {
		String result = new NumberToWords().numberToWords(12000);
		System.out.println(result);
	}
	
	public String numberToWords(int num) {
		if(num==0)
			return "Zero";
		StringBuilder sb = new StringBuilder();
		int billions = num/1000000000;
		num = num%1000000000;
		int millions = num/1000000;
		num = num%1000000;
		int thousands = num/1000;
		num=num%1000;
		
		if(billions>0) {
			sb.append(helper(billions) + " Billion " );
		}
		if(millions>0) {
			sb.append(helper(millions) + " Million " );
		}
		if(thousands>0) {
			sb.append(helper(thousands) + " Thousand " );
		}
		if(num>0) {
			sb.append(helper(num));
		}
		return sb.toString().trim();
	}
	
	String[] upto19 = {"","One","Two","Three","Four","Five","Six","Seven","Eight","Nine",
			"Ten","Eleven","Twelve","Thirteen","Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
	String[] tens = {"","","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"};
	public String helper(int num) {
		StringBuilder sb = new StringBuilder();
		int hundreds = num/100;
		num = num%100;
		if(hundreds!=0) {
			sb.append(upto19[hundreds]+" Hundred ");
		}
		if(num<20) {
			sb.append(upto19[num]);
		}else {
			int ten = num/10;
			num = num%10;
			sb.append(tens[ten]).append(" ").append(upto19[num]);
		}
		
		
		return sb.toString().trim();
	}
}
