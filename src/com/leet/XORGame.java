package com.leet;

public class XORGame {

	public boolean xorGame(int[] nums) {
		int xor=0;
		for (int i : nums) {
			xor ^= i;
		}
		
		if(xor==0  || nums.length==2 ) {
			return true;
		}
		
		return false;
	}
	public static void main(String[] args) {
		boolean result  = new XORGame().xorGame(	new int[] {1, 1, 2});
		System.out.println(result);
	}

}
