package com.leet;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CampusBikes {

	public static void main(String[] args) {
		int [][] workers = {{0,0},{2,1}} ;
		int [][] bikes = {{1,2},{3,3}} ;
	int result[] = new CampusBikes().assignBikes(workers, bikes);
	System.out.println(Arrays.toString(result));
	}
	public int[] assignBikes(int[][] workers, int[][] bikes) {
		Relation[] relations = new Relation[workers.length*bikes.length];
		int index =0;
		for(int i =0; i<workers.length; i++) {
			for(int j =0; j<bikes.length; j++) {
				int distance = findDistance(workers[i], bikes[j]);
				relations[index++] = new Relation(i,j,distance);
			}
		}
		
		Arrays.sort(relations);
		
		Map<Integer,Integer> map = new HashMap<>();
		for(Relation relation : relations) {
			if(map.get(relation.getWorker())==null && !map.values().contains(relation.getBike())) {
				map.put(relation.getWorker(), relation.getBike());
			}
		}
		int[] result = new int[workers.length];
		
		for(int i=0;i<workers.length;i++) {
			result[i] =map.get(i);
		}
		
		return result;
	}

	private int findDistance(int[] w, int[] b) {
		return Math.abs(w[0] -b[0]) + Math.abs(w[1] -b[1]) ;
	}

	class Relation implements Comparable<Relation>{
		int worker;
		int bike;
		int distance;
		public int getWorker() {
			return worker;
		}
		public void setWorker(int worker) {
			this.worker = worker;
		}
		public int getBike() {
			return bike;
		}
		public void setBike(int bike) {
			this.bike = bike;
		}
		public int getDistance() {
			return distance;
		}
		public void setDistance(int distance) {
			this.distance = distance;
		}
		public Relation(int worker, int bike, int distance) {
			this.worker = worker;
			this.bike = bike;
			this.distance = distance;
		}
		@Override
		public int compareTo(Relation r) {
			if(this.distance!=r.distance) {
				return this.distance-r.distance;
			}else if(this.worker!=r.worker) {
				return this.worker-r.worker;
			}else {
				return this.bike-r.bike;
			}
			
		}
		
	
	}



}
