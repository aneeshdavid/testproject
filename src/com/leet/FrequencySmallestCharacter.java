package com.leet;

import java.util.Arrays;

public class FrequencySmallestCharacter {

	public static void main(String[] args) {
		//String[] queries = {"cbd"};
		//String[]  words = {"zaaaz"};
		String[] queries = {"bbb","cc"};
		String[]  words = {"a","aa","aaa","aaaa"};
		int res[] = numSmallerByFrequency(queries, words);
		System.out.println(Arrays.toString(res));
	}

	
public static int[] numSmallerByFrequency(String[] queries, String[] words) {
        
        int[] resArr = new int[queries.length];
        
        for(int i =0; i<queries.length; i++){
            int wordsMax = 0;
            for(int j =0; j<words.length; j++){
            	wordsMax = Math.max(wordsMax, f(words[j]));
            }
           int queryFreq =  f(queries[i]);
            resArr[i] = Math.min(wordsMax, queryFreq);
        }
       return resArr; 
    }
    
    private static int f(String str){
        int[] asciiArr = new int[128];
        for(int i = 0;i<str.length(); i++ ){
            int ascii = (int)str.charAt(i);
            asciiArr[ascii]  = asciiArr[ascii]+1;
        }
        Arrays.sort(asciiArr);
        return asciiArr[asciiArr.length-1];
    }
}
