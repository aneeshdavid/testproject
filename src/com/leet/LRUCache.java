package com.leet;

import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;

public class LRUCache {

	int size;
    int counter = 0;
    Map<Integer, Integer> map = new HashMap<>();
    LinkedList<Integer> que = new LinkedList<>() ;
    public LRUCache(int capacity) {
        this.size = capacity;
    }
    
    public int get(int key) {
        int res = -1;
       
        if(que.contains(key) && map.containsKey(key)){
           res = map.get(key);
           que.addFirst(key);
        }
        if(que.size()>size){
        	 map.remove(que.getLast());
             que.removeLast();  
        }
        return res;  
    }
    
    public void put(int key, int value) {
        if(counter == size){
          map.remove(que.getLast());
          que.removeLast();  
          counter--;
        }
        map.put(key,value);
        counter++;
        que.addFirst(key);
    }
	public static void main(String[] args) {
		
		LRUCache lruCache = new LRUCache(2);
		
		lruCache.put(2, 1);
		lruCache.put(1, 1);
		System.out.println(lruCache.get(2));
		lruCache.put(4, 1);
		System.out.println(lruCache.get(1));
		System.out.println(lruCache.get(2));
	}

}
