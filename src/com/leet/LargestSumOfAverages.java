package com.leet;

public class LargestSumOfAverages {

	public static void main(String[] args) {
		int[] A = { 9,1,2,3,9};
			int	K = 3;
			new LargestSumOfAverages().largestSumOfAverages(A, K);
	}
	
	public double largestSumOfAverages(int[] A, int K) {
        int N = A.length;
		double[] dp = new double[N];
		double sum = 0;
		for(int i = 0; i < N; i++){
			sum += A[i];
			dp[i] = sum / (i + 1);
		}
		for(int k = 1; k < K; k++){
			for(int i = N - 1; i >= k; i--){  //k=1 , i =4 , =>1239
				double max = 0;					//          i = 3, =>123
				double curr = 0;                             //i=2, =>12
				int cnt = 0;									//	 i=1 , =>1	
				for(int j = i; j >= k; j--){		//k=2    i=4 , =>239
					curr += A[j];    				//			i=3	=>23
					cnt++;								//			i=2	=>2
					max = Math.max(max, curr / cnt + dp[j - 1]);
				}
                dp[i] = max;
			}
		}
		return dp[N - 1];
    }

}
