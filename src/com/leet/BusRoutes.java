package com.leet;

import java.util.ArrayList;
import java.util.List;

public class BusRoutes {

	public static void main(String[] args) {
		/*int[][] routes = {{1, 2, 7}, {3, 6, 7}};
		int S = 1;
		int T = 6;*/
			int[][] routes = {{7,12},{4,5,15},{6},{15,19},{9,12,13}};
		int S =	15;
		int T = 12;
		int noOfRoutes = new BusRoutes().numBusesToDestination(routes, S, T);
		System.out.println(noOfRoutes);
	}

	public int numBusesToDestination(int[][] routes, int S, int T) {
		int noOfRoute = 0;
		List<int[]> startRouteList = new ArrayList<>();
        for (int[] route : routes) {
        	if(hasValue(route, S)) {
        			 startRouteList.add(route);
        	}
		}
        if(startRouteList.isEmpty()) {
        	return -1;
        }
       for (int[] route : startRouteList) {
    	   if(hasValue(route, T)) {
           	return 1;
           }
       }
        noOfRoute =  routeHelper(routes, startRouteList.get(0), startRouteList, T, noOfRoute);
        
        if(noOfRoute==0) {
        	return -1;
        }else {
        	noOfRoute++;
        }
        
        
       return noOfRoute;
    }

	private static int routeHelper(int[][] routes, int[] startRoute,  List<int[]> startRouteList, int T, int noOfRoute) {
		 if(hasValue(startRoute, T)) {
	        	return noOfRoute;
	    }
		 for (int[] route : routes) {
	        	if(startRouteList.contains(route))
	        		continue;
	        	
	        	for(int stop : startRoute) {
	        		if(hasValue(route, stop) ) {
	        			noOfRoute++;
	        			startRoute = route;
	        			routeHelper(routes, startRoute, startRouteList, T, noOfRoute);
	        		}
	        	}
	        }
		return noOfRoute;
	}

	private static boolean hasValue(int[] route, int s) {
		for (int stop : route) {
			if(stop==s) {
				return true;
			}
		}
		return false;
	}
}
