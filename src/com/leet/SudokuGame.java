package com.leet;

public class SudokuGame {
	
	int[][] board;
	
	public SudokuGame(int boardSize) {
		board = new int[boardSize][boardSize];
	}
	
	public void start() {
		solveSudoku(0,0);
		printBoard();
	}

	private void printBoard() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				System.out.print(board[i][j]);
			}
			System.out.println("");
		}
		
	}

	private boolean solveSudoku(int row, int col) {
		if(col==board.length) {
			col=0;
			row++;
			if(row==board.length) {
				return true;
			}
		}
		
		if(board[row][col]!=0) {
			return solveSudoku(row, col+1);
		}
		
		for(int val=1; val <= board.length; val++) {
			if(canFill(val, row, col)) {
				board[row][col] = val;
				if(solveSudoku(row, col+1)) {
					return true;
				}
			}
			
		}
		
		
		return false;
	}

	private boolean canFill(int val, int row, int col) {
		
		for(int[] ele:board) {
			if(ele[col] == val ) {
				return  false;
			}
		}
		
		for(int i=0;i<board.length;i++) {
			if(board[row][i]==val) {
				return false;
			}
		}
		
		int sqRoot =(int) Math.sqrt(board.length);
		
		int I = row/sqRoot;
		int J = col/sqRoot;
		
		int subBoxRow = sqRoot*I;
		int subBoxCol = sqRoot*J;
		
		for (int i = 0; i < sqRoot; i++) {
			for (int j = 0; j < sqRoot; j++) {
				if(val==board[subBoxRow+i][subBoxCol+j]) {
					return false;
				}
			}
		}
		
		
		return true;
	}

	public static void main(String[] args) {
		SudokuGame game = new SudokuGame(9);
		game.start();

	}

}
