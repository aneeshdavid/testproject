package com.leet;

public class ValidTicTacToe {

	public static void main(String[] args) {
	//	board = ["XOX", "O O", "XOX"];

	}
	
	public boolean validTicTacToe(String[] board) {
        int xCount=0;
        int oCount=0;
        for(String str : board){
            for(char c:str.toCharArray()){
              if(c=='x'){
                 xCount++; 
              } 
              if(c=='o'){
                 oCount++; 
              } 
            }
        }
        
        if(xCount!=oCount && oCount!= xCount-1){
            return true;
        }
        if(win(board,'x') && oCount!= xCount-1){
            return false;
        }
        if(win(board,'o') && oCount!= xCount){
            return false;
        }
        
        return true;
        
    }
    
    public boolean win(String[] board, char p){
        for(int i=0;i<3;i++){
            if(board[0].charAt(0)==p && board[0].charAt(1)==p &&                board[0].charAt(2)==p){
                return true;
            }
            
            if(board[0].charAt(0)==p && board[1].charAt(0)==p && board[2].charAt(0)==p){
                return true;
            }
            
            if(board[0].charAt(0)==p && board[1].charAt(1)==p && board[2].charAt(2)==p){
                return true;
            }
            
            if(board[0].charAt(2)==p && board[1].charAt(1)==p && board[2].charAt(0)==p){
                return true;
            }
            
            
            
            
        }
        return false;
    }


}
