package com.leet;

public class TicTacToeGame {
	int[][] board;
	boolean isWon;
	int whoWon;
	
	public TicTacToeGame(int n) {
		board = new int[n][n];
	}
	
	public int move(int posI, int posJ, int player) {
		if(isWon) {
			System.out.println("No more moves, the player "+whoWon +" won the game");
			return 0;
		}
		if(board[posI][posJ]!=0 ) {
			System.out.println("Can not make this move, The position is already filled " + board[posI][posJ]);
			return 0;
		}
		board[posI][posJ] = player;
		
		isWon = isWon(player);
		whoWon = player;
		printBoard();
		if(isWon)
			return 1;
		else
			return 0;
	}
	
	private void printBoard() {
		for(int i=0;i<board.length;i++) {
			for(int j=0;j<board.length;j++) {
				System.out.print(board[i][j]);
			}
			System.out.println("");
		}
		
	}

	private boolean isWon(int player) {
		boolean isWon = true;
		//check all row wise
		for(int i=0;i<board.length;i++) {
			 isWon = true;
			for(int j=0;j<board.length;j++) {
				if(board[i][j]==0 || board[i][j]!=player) {
					isWon = false;
					break;
				}
			}
			if(isWon) {
				return true;
			}
		}
		
		//check all col wise
				for(int i=0;i<board.length;i++) {
					 isWon = true;
					for(int j=0;j<board.length;j++) {
						if(board[j][i]==0 || board[j][i]!=player) {
							isWon = false;
							break;
						}
					}
					if(isWon) {
						return true;
					}
				}
	
		//corner: top-left to bottom-right
				 isWon = true;
				for(int i=0;i<board[0].length;i++) {
					if(board[i][i]==0 || board[i][i]!=player) {
						isWon = false;
						break;
					}
				}
				if(isWon) {
					return true;
				}
	
				//corner: top-right to bottom-left
				 isWon = true;
				for(int i=0;i<board.length;i++) {
					if(board[i][board.length-1-i]==0 || board[i][board.length-1-i]!=player) {
						isWon = false;
						break;
					}
				}
				if(isWon) {
					return true;
				}	
				
		return false;
	}

	public static void main(String args[]) {
		/*TicTacToeGame toe = new TicTacToeGame(3);
		System.out.println(toe.move(0, 0, 1));
		System.out.println(toe.move(0, 2, 2));
		System.out.println(toe.move(2, 2, 1));
		System.out.println(toe.move(1, 1, 2));
		System.out.println(toe.move(2, 0, 1));
		System.out.println(toe.move(1, 0, 2));
		System.out.println(toe.move(2, 1, 1));
		System.out.println(toe.move(2, 1, 1));*/
		
		TicTacToeGame toe = new TicTacToeGame(2);
		System.out.println(toe.move(0, 0, 2));
		System.out.println(toe.move(1, 1, 1));
		System.out.println(toe.move(0, 1, 2));
		
	}
}
