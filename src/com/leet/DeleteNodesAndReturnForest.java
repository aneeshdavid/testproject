package com.leet;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DeleteNodesAndReturnForest {
	List<TreeNode> forest = new ArrayList<>();
	public static void main(String[] args) {
		int[] to_del = {3,5};
		
		TreeNode root = new TreeNode(1);
		root.left = new TreeNode(2);
		root.right = new TreeNode(3);
		
		root.left.left =  new TreeNode(4);
		root.left.right =  new TreeNode(5);
		
		root.right.left = new TreeNode(6);
		root.right.right = new TreeNode(7);
		List<TreeNode>  forest = new DeleteNodesAndReturnForest().delNodes(root, to_del);
		System.out.println("Success");
	}

	
	
    public List<TreeNode> delNodes(TreeNode root, int[] to_delete) {
      
       Set<Integer> to_delete_nodes = new HashSet<>();
        for(Integer n : to_delete){
            to_delete_nodes.add(n);
        }
        removeTree(root, to_delete_nodes);
        if(!to_delete_nodes.contains(root.val)){
           forest.add(root); 
        }
      
        return forest;
    }
    
    private TreeNode removeTree(TreeNode root, Set to_delete_nodes){
        if(root == null)
            return null;
        
        root.left = removeTree(root.left, to_delete_nodes);
        root.right = removeTree(root.right, to_delete_nodes);
        
        if(to_delete_nodes.contains(root.val)){
            if(root.left!=null){
                 forest.add(root.left); 
            }
            if(root.right!=null){
                 forest.add(root.right); 
            }
            return null;
        }
        return root;
    }
	
	
	static class TreeNode {
		      int val;
		      TreeNode left;
		      TreeNode right;
		      TreeNode(int x) { val = x; }
		  }
}
