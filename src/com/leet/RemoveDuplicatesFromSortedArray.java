package com.leet;

public class RemoveDuplicatesFromSortedArray {
	
	 public int removeDuplicates(int[] nums) {
		 if (nums.length == 0) return 0;
		 
			int cnt = 1, cur = 0;
			int count = 1;
		 
			for (int i = 1; i < nums.length; i++) {
				if (nums[cur] == nums[i]) {
					cnt++;
				} else {
					cur = i;
					cnt = 1;
				}
				if (cnt <= 2) 
					count++;
				
				//nums[i - (i + 1 - count)] = nums[i];
				nums[count-1] = nums[i];
			}
			return count;
	 }	
	 
	
	 public static void main (String args[]) {
		 int[] inputArr =  {0,0,1,1,1,1,2,3,3};
		 int len = new RemoveDuplicatesFromSortedArray().removeDuplicates(inputArr);
		 System.out.println(len);
		 for (int i = 0; i < len; i++) {
			 System.out.print(inputArr[i]+" ");
		}
	 }
}
