package com.leet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class QueueReconstructionByHeight {

	 public int[][] reconstructQueue(int[][] peopleArr) {
		 
		 Arrays.sort(peopleArr, new Comparator<int []>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				if(o1[0] == o2[0]) {
					return o1[1] - o2[1];
				}else{
					return o2[0] - o1[0];
				}
			}
			 
		});
		 
		 List<int[]> peopleList = new ArrayList<>();
	        for(int [] people : peopleArr) {
	        	peopleList.add( people[1], people);
	        }
	        return peopleList.toArray(new int[peopleArr.length][2]);
	      
	 }
	
	 public static void main(String args[]) {
		 int[][] people = new int[][]{{7,0}, {4,4},{7,1}, {5,0}, {6,1}, {5,2}};
		 int[][] resultArr   = new QueueReconstructionByHeight().reconstructQueue(people);
		// System.out.println(Arrays.toString(resultArr));
		for (int[] is : resultArr) {
			System.out.print("["+is[0]+","+is[1]+"]");
		}
	 }
	 
}
