package com.leet;

public class AddLinkedList {

	static class ListNode{
		int val;
		ListNode next;
		public ListNode(int val) {
			this.val = val;
		}
	}
	public static void main(String[] args) {
		 ListNode l1 =new ListNode(2);
		 l1.next = new ListNode(4);
		 l1.next.next = new ListNode(3);
      
         ListNode l2 =new ListNode(5);
		 l2.next = new ListNode(6);
		 l2.next.next = new ListNode(4);
         ListNode ret = addTwoNumbers(l1, l2);
         
         String out = listNodeToString(ret);
         
         System.out.print(out);
	}

	
	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		int carry = 0;
		ListNode head = new ListNode(0);
		ListNode result = head;
		while (l1 != null && l2 != null) {
			int val1 = l1.val;
			int val2 = l2.val;
			int total = val1 + val2 + carry;
			int val = total % 10;
			carry = total / 10;
			if (result == null) {
				result = new ListNode(val);
			} else {

				ListNode newNode = new ListNode(val);
				result.next = newNode;
				result = result.next ;
			}

			l1 = l1.next;
			l2 = l2.next;

		}
		if (carry > 0) {
			ListNode newNode = new ListNode(carry);
			result.next = newNode;
		}

		return head.next;
	}
	
	public static String listNodeToString(ListNode node) {
        if (node == null) {
            return "[]";
        }
    
        String result = "";
        while (node != null) {
            
            result += Integer.toString(node.val) + ", ";
            node = node.next;
            
        }
        return "[" + result.substring(0, result.length() - 2) + "]";
    }

}
