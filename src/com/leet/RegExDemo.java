package com.leet;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExDemo {
//https://regexr.com/
	public static void main(String[] args) {
		String emailString = "Hello aneeshd60@gmail.com , how are you. how is selvashobana@gmail.com and sona@gmail.com 1-(408)-594-5210 (408)-594-5210 408-594-5210 408 594 5210";
		String emailRegEx = "[A-Za-z0-9._%-]+@[A-Za-z0-9._-]+\\.[A-Za-z]{2,4}";
		find(emailRegEx, emailString);
		
		String mobileRegEx = "([0-9]( |-)?)?(\\(?[0-9]{3}\\)?|[0-9]{3})( |-)?([0-9]{3})( |-)?[0-9]{4}";
		find(mobileRegEx, emailString);
	}
	
	public static void find(String regEx, String str) {
		Pattern checkregEx= Pattern.compile(regEx);
		Matcher mat = checkregEx.matcher(str);
		while(mat.find()) {
			if(mat.group().length()>0) {
			System.out.println(mat.group());
			}
		}
	}

}
