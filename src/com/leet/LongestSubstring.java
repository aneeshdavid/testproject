package com.leet;

import java.util.HashMap;
import java.util.Map;

public class LongestSubstring {
/**
 * Input: "abcabcbb"
	Output: 3 
	
	Input: "bbbbb"
	Output: 1
	
	Input: "pwwkew"
	Output: 3
 * @param args
 */
	
	public static void main(String args[]) {
		int longest = findLongest("abcabcbb");
		System.out.println(longest);
	}
	
	private static int findLongest(String str){
		int ans = 0;
		int len = str.length();
		Map<Character, Integer> map = new HashMap<>();
		for(int i=0, j=0; j<len; j++) {
			
			if(map.containsKey(str.charAt(j))) {
				i = Math.max(map.get(str.charAt(j)), i );
			}
			
			ans = Math.max(ans, j-i+1);
			map.put(str.charAt(j), j+1);
		}
		
		
		return ans;
	}
	
}
