package com.leet;

public class ConvertToHexaDecimal {
	
	public String toHex(int num) {
		if(num == 0) return "0";
		char hex[] = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
		StringBuilder s = new StringBuilder();
		while(num!=0) {
			System.out.println(num&15);
			s.append(hex[num&15]);
			num = num >>> 4;
		}
		return s.reverse().toString();
	}
	public static void main(String args[]) {
		String result = new ConvertToHexaDecimal().toHex(-3);
		System.out.println(result);
	}
}
