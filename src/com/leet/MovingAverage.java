package com.leet;

import java.util.ArrayList;
import java.util.List;

/**
 Given a stream of integers and a window size, calculate the moving average of all integers in the sliding window.
Example:
MovingAverage m = new MovingAverage(3);
m.next(1) = 1
m.next(10) = (1 + 10) / 2
m.next(3) = (1 + 10 + 3) / 3
m.next(5) = (10 + 3 + 5) / 3
 * @author adavid
 *
 */
public class MovingAverage {
	
	static class MyQueue{
		int arr[] ;
		int count =0;
		int rear =0;
		int size ;
	
		public MyQueue(int size) {
			this.size = size;
			arr = new int[3];
		}
		
		private int  next(int value) {
			if(count==size)
				count--;
			
			rear = rear%size;
			arr[rear] = value;
			rear++;
			count++;
			int avg = getAverage();
			return avg;
		}
		
		private int getAverage() {
			int avg = 0;
			for(int i=0;i<count;i++) {
			
				avg=avg+arr[i];
			
			}
			return avg/count;
			
		}

	}

	public static void main(String[] args) {
		MyQueue qu = new MyQueue(3);	
		System.out.println(qu.next(1));
		System.out.println(qu.next(10));
		System.out.println(qu.next(3));
		System.out.println(qu.next(5));
		System.out.println(qu.next(100));
	}

}
