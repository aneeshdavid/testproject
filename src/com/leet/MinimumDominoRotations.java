package com.leet;

import java.util.Arrays;

public class MinimumDominoRotations {

	public static void main(String[] args) {
		int A[] = {2,1,2,4,2,2};
		int B[] = {5,2,6,2,3,2};
		
		int[] subarray = Arrays.copyOfRange(A, 5, A.length);
		System.out.println(Arrays.toString(subarray));
		
	//	int A[] = {3,5,1,2,3};
	//	int B[] = {3,6,3,3,4};
		
		int min = minDominoRotations(A, B);
		System.out.println(min);
		
		
		long mid = 50 << 1;
		System.out.println( mid);
	}


    public static int minDominoRotations(int[] A, int[] B) {
    	int length = A.length;
    	int rotation = findRotation(A[0], A, B , length);
    	if(rotation>=0) {
    		return rotation;
    	}
    	
    	return findRotation(B[0], A, B, length);
    	
    }


	private static int findRotation(int x, int[] A, int[] B, int length) {
		int rotation_a = 0, rotation_b = 0;
		for(int i = 0; i<length; i++) {
			if(x!=A[i] && x!=B[i]) {
				return -1;
			}
			if(x!=A[i]) {
				rotation_a++;
			}
			if(x!=B[i]) {
				rotation_b++;
			}
		}
		return Math.min(rotation_a, rotation_b);
	}
    
    
}
