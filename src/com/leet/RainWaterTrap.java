package com.leet;

import java.util.Arrays;

public class RainWaterTrap {

	public static void main(String[] args) {
		int height[] = {0,1,0,2,1,0,1,3,2,1,2,1};
		int totalWater = new RainWaterTrap().trap(height);
		System.out.println(totalWater);
	}

	
	public int trap(int[] height) {
        int totalWater = 0;
        if(height == null || height.length == 0)
            return totalWater;
        
        int[] leftHeight = new int[height.length];
        leftHeight[0] = 0;
        for(int i=0;i<height.length-1;i++){
            leftHeight[i+1] = Math.max(leftHeight[i],height[i]);
        }
        System.out.println("LeftHeight: "+ Arrays.toString(leftHeight));
        int[] rightHeight = new int[height.length];
        rightHeight[height.length - 1] = 0;
        for(int i = height.length -1 ;i>0 ;i--){
            rightHeight[i-1] = Math.max(rightHeight[i], height[i]);
        }
        System.out.println("RightHeight: "+ Arrays.toString(rightHeight));
        
        for(int i=0; i< height.length - 1; i++){
            int min = Math.min(leftHeight[i],rightHeight[i]);
            if(min > height[i])
                totalWater = totalWater + (min - height[i]);
        }
        
        return totalWater;
    }
}
