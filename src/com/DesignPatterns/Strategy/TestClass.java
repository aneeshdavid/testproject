package com.DesignPatterns.Strategy;

public class TestClass {

	public static void main(String[] args) {
		int num1=20;
		int num2=10;
		Context ctxt = new Context(new Addition());
		System.out.println("ADD:"+ctxt.executeOperation(num1, num2));
		ctxt = new Context(new Subtraction());
		System.out.println("SUB:"+ctxt.executeOperation(num1, num2));
		ctxt = new Context(new Multiplication());
		System.out.println("MUL:"+ctxt.executeOperation(num1, num2));

	}

}
