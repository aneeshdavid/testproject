package com.DesignPatterns.Strategy.RealWorldExample;

import java.util.Arrays;

public class TestClass {

	public static void main(String[] args) {
		RoleContext userContext = new RoleContext(new UserRole());
		System.out.println(" User:"+Arrays.toString(userContext.getCapabilities().toArray()));
		
		userContext = new RoleContext(new AdminRole());
		System.out.println(" Admin:"+Arrays.toString(userContext.getCapabilities().toArray()));
		
		userContext = new RoleContext(new AuditorRole());
		System.out.println(" Auditor:"+Arrays.toString(userContext.getCapabilities().toArray()));
	}

}
