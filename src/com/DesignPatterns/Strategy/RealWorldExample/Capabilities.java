package com.DesignPatterns.Strategy.RealWorldExample;

public enum Capabilities {
	VIEW,EDIT,DELETE,INSPECT
}
