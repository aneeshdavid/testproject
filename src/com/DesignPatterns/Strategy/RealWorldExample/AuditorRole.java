package com.DesignPatterns.Strategy.RealWorldExample;

import java.util.Arrays;
import java.util.List;

public class AuditorRole implements RoleStrategy{

	@Override
	public List<Capabilities> getCapabilities() {
		return Arrays.asList(new Capabilities[] {Capabilities.VIEW,Capabilities.INSPECT});
	}

}
