package com.DesignPatterns.Strategy.RealWorldExample;

import java.util.List;

public interface RoleStrategy {
	List<Capabilities>getCapabilities();
}
