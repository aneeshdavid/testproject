package com.DesignPatterns.Strategy.RealWorldExample;

import java.util.List;

public class RoleContext {
	RoleStrategy strartegy;
	public RoleContext(RoleStrategy strartegy) {
		this.strartegy =  strartegy;
	}
	
	public List<Capabilities> getCapabilities() {
		return this.strartegy.getCapabilities();
	}
}
