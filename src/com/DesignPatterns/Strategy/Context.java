package com.DesignPatterns.Strategy;

public class Context {
	Strategy strategy;
	public Context(Strategy strategy) {
		this.strategy = strategy;
	}	
	
	public int executeOperation(int num1, int num2) {
		return this.strategy.doOperation(num1, num2);
	}
}
