package com.DesignPatterns.Builder;

public class Pepsi implements IItem{

  @Override
  public String getName() {
    return "Pepsi";
  }

  @Override
  public float getPrice() {
    return 2;
  }

}
