package com.DesignPatterns.Builder;

public class NonVegBurger implements IItem {

  @Override
  public String getName() {
   return "Non Veg Burger";
  }

  @Override
  public float getPrice() {
    return 6.5f;
  }

  @Override
  public String getPackage() {
    return "Wrapper";
  }

}
