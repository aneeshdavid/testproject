package com.DesignPatterns.Builder;

public class BuilderPatternDemo {

  public static void main(String[] args) {
    
    MealBuilder mealBuilder = new MealBuilder();
    Meal vegMeal = mealBuilder.getVegMeal();
    System.out.println("Veg Meal");
    vegMeal.showItems();
    
    mealBuilder = new MealBuilder();
    Meal nonVegMeal = mealBuilder.getNonVegMeal();
    System.out.println("Non Veg Meal");
    nonVegMeal.showItems();
  }

}
