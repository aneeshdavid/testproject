package com.DesignPatterns.Builder;

public class VegBurger implements IItem {

  @Override
  public String getName() {
    return "Veg Burger";
  }

  @Override
  public float getPrice() {
    return 5;
  }

  @Override
  public String getPackage() {
   return "Wrapper";
    
  }

}
