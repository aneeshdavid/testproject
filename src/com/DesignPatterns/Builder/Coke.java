package com.DesignPatterns.Builder;

public class Coke implements IItem{

  @Override
  public String getName() {
   return"Coke";
    
  }

  @Override
  public float getPrice() {
    return 1.2f;
  }

  @Override
  public String getPackage() {
    return "Bottle";
  }

}
