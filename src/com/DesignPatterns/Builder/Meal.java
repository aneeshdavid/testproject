package com.DesignPatterns.Builder;

import java.util.ArrayList;
import java.util.List;

public class Meal {

  private List<IItem> items = new ArrayList<>();
  
  public void addItem(IItem item) {
   this.items.add(item);
  }

  public float getPrice() {
    float price =0;
    for (IItem iItem : items) {
      price +=iItem.getPrice();
    }
    return price;
  }

  public void showItems() {
    float price =0;
    for (IItem iItem : items) {
      System.out.println("Name: " + iItem.getName() +"\n");
      System.out.println("Price: " + iItem.getPrice() +"\n");
      price +=iItem.getPrice();
    }
    System.out.println("-------------------------------------\n");
    System.out.println("Total: " + price +"\n");
  }

}
