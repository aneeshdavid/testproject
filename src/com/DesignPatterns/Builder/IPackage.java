package com.DesignPatterns.Builder;

public interface IPackage {
  String getPackage();
}
