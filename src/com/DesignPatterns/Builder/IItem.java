package com.DesignPatterns.Builder;

public interface IItem {
  String getName();
  float getPrice();
  String getPackage();
  
}
