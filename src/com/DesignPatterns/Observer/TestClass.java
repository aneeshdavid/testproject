package com.DesignPatterns.Observer;

public class TestClass {

	public static void main(String args[]) {
		Observer observer = new Observer();
		observer.attach(new Add());
		observer.attach(new Mul());
		observer.attach(new Sub());
		
		observer.setNumbers(10, 20);
		System.out.println("***********");
		observer.setNumbers(30, 20);
		
	}
}
