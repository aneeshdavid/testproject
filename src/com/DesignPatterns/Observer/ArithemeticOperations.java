package com.DesignPatterns.Observer;

public abstract class ArithemeticOperations {
	
	abstract void doCalculation(int num1, int num2);

}
