package com.DesignPatterns.Observer;

public class Sub extends ArithemeticOperations{

	@Override
	void doCalculation(int num1, int num2) {
		int diff = num1-num2;
		System.out.println("DIFF:"+ diff);
	}

}
