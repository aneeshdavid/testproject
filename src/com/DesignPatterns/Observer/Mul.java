package com.DesignPatterns.Observer;

public class Mul extends ArithemeticOperations{

	@Override
	void doCalculation(int num1, int num2) {
		int mul = num1*num2;
		System.out.println("MUL:"+mul);
	}

}
