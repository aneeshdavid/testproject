package com.DesignPatterns.Observer;

public class Add extends ArithemeticOperations{

	@Override
	void doCalculation(int num1, int num2) {
		int sum =  num1+num2;
		System.out.println("SUM:"+sum);
	}

	
}
