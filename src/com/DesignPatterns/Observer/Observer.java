package com.DesignPatterns.Observer;

import java.util.ArrayList;
import java.util.List;

public class Observer {
	List<ArithemeticOperations>  operations = new ArrayList<>();
	int num1;
	int num2;
	
	public void attach(ArithemeticOperations operation) {
		operations.add(operation);
	}
	
	public void setNumbers(int num1, int num2) {
		this.num1=num1;
		this.num2=num2;
		notifyAllOper();
	}
	
	 private void notifyAllOper() {
		for (ArithemeticOperations operation : operations) {
			operation.doCalculation( num1,  num2);
		}
	}
	
	
	
}
