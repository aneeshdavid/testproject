package com.leet_hard;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

public class WordLadder_1 {
/**
 * Given two words (start and end), and a dictionary, find the length of shortest transformation sequence from start to end, such that only one letter can be changed at a time and each intermediate word must exist in the dictionary.
    For example, given:
    start = "hit"
	end = "cog"
	dict = ["hot","dot","dog","lot","log"]
	
	One shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog", the program should return its length 5.

 * */
	
	public int findWordLadder(String start, String end, Set<String> wordDict) {
		LinkedList<WordNode> queue = new LinkedList<>();
		queue.add(new WordNode(start,1));
		wordDict.add(end);
		while(!queue.isEmpty()) {
			WordNode topWord = queue.remove();
			
			if(topWord.word.equals(end)) {
				return topWord.numSteps;
			}
			
			char arr[] = topWord.word.toCharArray();
			for (int i = 0; i < arr.length; i++) {
				for (char c= 'a'; c <= 'z'; c++) {
					char temp = arr[i];
					if(arr[i]!=c) {
						arr[i]= c;
						String newWord = new String(arr);
						if(wordDict.contains(newWord)) {
							queue.add(new WordNode(newWord, topWord.numSteps+1) );
							wordDict.remove(newWord);
						}
					}
					arr[i] = temp;
				}
			}
		}
		
		return 0;
	}
	
	static class WordNode{
		String word;
		int numSteps;
		
		public WordNode(String word, int numSteps) {
			this.word = word;
			this.numSteps = numSteps;
		}
	}
	
	public static void main(String[] args) {
		Set<String> wordDict = new HashSet<>();
		wordDict.add("hot");
		wordDict.add("dot");
		wordDict.add("dog");
		wordDict.add("lot");
		wordDict.add("log");
		int len = new WordLadder_1().findWordLadder("hit", "cog",wordDict);
		System.out.println(len);
	}

}
