package com.leet_hard;

import java.util.HashMap;
import java.util.Map;

import javafx.util.Pair;
//https://leetcode.com/problems/max-points-on-a-line/solution/
public class MaxPointsOnLine {
	int[][] points;
	Map<Double,Integer> slopes = new HashMap<>();
	int horizontalLines;
	public int maxPoints(int[][] points) {
		this.points = points;
		if(points.length>3){
	           return points.length; 
	        }
			int maxCount = 1;
		
		for (int i = 0; i < points.length; i++) {
			maxCount = Math.max(findMaxPoints(i), maxCount);
		}
		
		
		return maxCount;
	}
	private int findMaxPoints(int i) {
		slopes.clear();
		horizontalLines = 1;
		int duplicates = 0;
		int count = 0;
		for (int j = i+1; j < points.length; j++) {
			MyPair<Integer, Integer>line = addLines(i, j, count, horizontalLines, duplicates);
			count =line.getKey();
			duplicates = line.getValue();
		}
		return count+duplicates;
	}
	private MyPair<Integer, Integer> addLines(int i, int j, int count, int horizontalLines, int duplicates) {
		int x1 = points[i][0];
		int y1 = points[i][1];
		int x2 = points[j][0];
		int y2 = points[j][1];
		if(x1==x2 && y1==y2) {
			duplicates++;
		}else if(y1==y2) {
			horizontalLines++;
			count  = Math.max(horizontalLines, count);
		}else {
			double slope = Math.abs((x1-x2)/(y1-y2));
			slopes.put(slope, slopes.getOrDefault(slope, 1)+1);
			count = Math.max(slopes.get(slope), count);
		}
		return new MyPair(count, duplicates);
	}
	
	static class MyPair<k,v>{
		k key;
		v value;
		public MyPair(k key, v value) {
			this.key =key;
			this.value = value;
		}
		public k getKey() {
			return this.key;
		}
		public v getValue() {
			return this.value;
		}
	}
	
}



/*
 *  public int maxPoints(int[][] points) {
		 int max = 0;
		 int sum = 0;
		 if(points.length <= 2) {
			 return(points.length);
		 }
		 for (int i = 0; i < points.length; i ++) {
			 for (int j = i + 1; j <points.length; j++) {
				 sum = 0;
				 for (int k = 0; k < points.length; k++) {
					 if( ((long)(points[k][1] - points[i][1]) * (long) (points[j][0] - points[i][0])) == ((long)(points[k][0] - points[i][0])*(long)(points[j][1] - points[i][1]))) {
						 sum += 1;
					 }
				 }
				 if((max <= sum) && (!((max != 0) && ((points[j][0] - points[i][0] == 0) && (points[j][1] - points[i][1] == 0))))) {
					 max = sum;
				 }
			 }
		 	}
		 	return(max);
		 }
 */
 
