package com.leet_hard;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

public class StrongPasswordChecker {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	
	public int strongPasswordChecker_pattern(String s) {
		int changeCount = 0;
        if(s.length()==0 || s.length()<6 || s.length()>20) {
        	return 1;
        }

        String password_pattern = "((?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@#$%]).{6,20})";
        Pattern pat = Pattern.compile(password_pattern);
        Matcher mat = pat.matcher(s);
        if(!mat.matches()) {
        	changeCount++;
        }
		return changeCount;
    }
	
	public int strongPasswordChecker(String s) {
		int requiredChar = getrRequiredCharCount(s);
		if(s.length()<6) {
			return Math.max(requiredChar, 6 - s.length());
		}
		char prevChar = 0;
		int sameCharCount = 0;
		int changeCount = 0;
		for(int i = 0; i<s.length(); i++) {
			if(prevChar==0) {
				prevChar = s.charAt(i);
			}
			if(s.charAt(i) == prevChar) {
				sameCharCount++;
			}else {
				if(sameCharCount>=3) {
					if(sameCharCount % 3==0) {
						changeCount+= 1;
					}
					if(sameCharCount % 3==1) {
						changeCount+= 2;
					}
				}
				prevChar = s.charAt(i);
				sameCharCount =1;
			}
		}
		if(sameCharCount>=3) {
			if(sameCharCount % 3==0) {
				changeCount+= 1;
			}
			if(sameCharCount % 3==1) {
				changeCount+= 2;
			}
		}
		
		if(s.length()<=20) {
			return Math.max(requiredChar, changeCount);
		}
		int deleteCount = (s.length()-20) - changeCount -  requiredChar;
		return deleteCount;
	}
	
	private int getrRequiredCharCount(String s) {
		int upperCase =1;
		int lowerCase =1;
		int digit =1;
		
		for (int i = 0; i < s.length(); i++) {
			if(Character.isUpperCase(s.charAt(i))) {
				upperCase = 0;
			}else if(Character.isLowerCase(s.charAt(i))) {
				lowerCase = 0;
			}else if(Character.isDigit(s.charAt(i))) {
				digit =0;
			}
		}
		return upperCase + lowerCase + digit;
	
	}
	
	@Test
	public void  testStrongPasswordChecker() {
		/*assertEquals(1, strongPasswordChecker("abc"));
		assertEquals(1, strongPasswordChecker("JAN2019$"));
		assertEquals(1, strongPasswordChecker("jan2019$"));
		assertEquals(1, strongPasswordChecker("jan2019a"));
		assertEquals(1, strongPasswordChecker("janabcda"));
		assertEquals(0, strongPasswordChecker("Jan2019$"));*/
		
		assertEquals(3, strongPasswordChecker("1111111111"));
		
	}

}
