package com.leet_hard;
/**
 *Given an input string (s) and a pattern (p), implement wildcard pattern matching with support for '?' and '*'.
 '?' Matches any single character.
'*' Matches any sequence of characters (including the empty sequence).
 * */
public class WildcardMatching {

	public boolean isMatch(String s, String p) {
		/*if(!p.contains("*") && s.length()!=p.length())
			return false;
		int pt = 0;	
		boolean astricFlag = false;
		for(int st =0; st<p.length();st++) {
			if(p.charAt(pt)=='?')
				pt++;
			else if(p.charAt(pt)=='*') {
				astricFlag = true;
				pt++;
			}else if(p.charAt(pt)==s.charAt(st)) {
				pt++;
			}else {
				 int lastIndex = getLastIndex(p.charAt(pt),st, s);
				if(astricFlag && lastIndex!= -1) {
					astricFlag = false;
					pt++;
					st+=lastIndex;
				}else if(!astricFlag && p.charAt(pt)!= s.charAt(st)) {
					return false;
				}
			}
				
		}
		return true;*/
		
		Boolean[][] memo = new Boolean[s.length()][p.length()];
		return matchHelper(s, p, 0, 0, memo);
	}
	
public Boolean matchHelper(String s, String p, int i, int j, Boolean[][] memo){
        
        if(memo[i][j]!=null){
            return memo[i][j];
        }
        memo[i][j] = false;
        
        if(i >=s.length() && j >=p.length()){
             memo[i][j] = true;
        }
        else if(i>=s.length() && j<p.length() && p.charAt(j)=='*'){
            memo[i][j] = matchHelper(s,p,i,j+1,memo);
        }
        else if(i >=s.length() || j >=p.length()){
            memo[i][j] = false;
        }
        else if(p.charAt(j)=='?' || s.charAt(i)== p.charAt(j)){
            memo[i][j] = matchHelper(s,p,i+1,j+1,memo);
        }
        else if(p.charAt(j)=='*'){
          memo[i][j] = matchHelper(s,p,i+1,j,memo) || matchHelper(s,p,i,j+1,memo);
        }

        return memo[i][j];
    }

	public static void main(String[] arg) {
		String s = "abefcdgiescdfimde";
		String p="ab*cd?i*de";		
		boolean result = new WildcardMatching().isMatch(s, p);
		System.out.println(result);
	}
}
