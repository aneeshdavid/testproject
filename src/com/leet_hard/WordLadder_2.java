package com.leet_hard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *Given two words (beginWord and endWord), and a dictionary's word list, find all shortest transformation sequence(s) from beginWord to endWord, such that:

Only one letter can be changed at a time
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
Note:

Return an empty list if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
Example 1:

Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output:
[
  ["hit","hot","dot","dog","cog"],
  ["hit","hot","lot","log","cog"]
]

Example 2:

Input:
beginWord = "hit"
endWord = "cog"
wordList = ["hot","dot","dog","lot","log"]

Output: []

Explanation: The endWord "cog" is not in wordList, therefore no possible transformation.
 * */
public class WordLadder_2 {
	
	public List<List<String>> findWordLader(String start, String end, List<String> wordDict){
		List<List<String>> result = new ArrayList<>();
		LinkedList<WordNode> queue = new LinkedList<>();
		queue.add(new WordNode(start, 1, null));
		
		Set<String>wordDictSet = new HashSet<>();
		wordDictSet.addAll(wordDict);
		wordDictSet.add(end);
		
		while(!queue.isEmpty()) {
			WordNode top = queue.remove();
			if(top.word.equals(end)) {
				return result;
			}
			char[] arr = top.word.toCharArray();
			for (int i = 0; i < arr.length; i++) {
				for (char c = 'a'; c <='z'; c++) {
					char temp = arr[i];
					if( arr[i]!=c) {
						 arr[i]=c;
						 String newWord = new String(arr);
						 
						 if(newWord.equals(end)) {
							 List<String> resultList = new ArrayList<>();
							 resultList.add(end);
							 WordNode node = top;
							 while(node!=null) {
								 resultList.add(node.word);
								 node = node.prev;
							 }
							 Collections.reverse(resultList);
							 result.add(resultList);
						 }
						 
						 if(wordDictSet.contains(newWord)) {
							 queue.add(new WordNode(newWord, top.depth+1, top));
							 wordDictSet.remove(newWord);
						 }
					}
					arr[i]=temp;
				}
			}
		}
		
		return result;
	}
	
	static class WordNode{
		String word;
		int depth;
		WordNode prev;
		
		public WordNode(String word, int depth, WordNode prev) {
			this.word = word;
			this.depth = depth;
			this.prev = prev;
		}
	}

	public static void main(String[] args) {
		List<String> wordDict = new ArrayList<>();
		wordDict.add("hot");
		wordDict.add("dot");
		wordDict.add("dog");
		wordDict.add("lot");
		wordDict.add("log");
		List result = new WordLadder_2().findWordLader("hit", "cog",wordDict);
		System.out.println("Success");
	}

}
