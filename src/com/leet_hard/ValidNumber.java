/**
 * 
 */
package com.leet_hard;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;

/**
 * @author adavid
 *
 *Validate if a given string can be interpreted as a decimal number.
 *
 Some examples:
"0" => true
" 0.1 " => true
"abc" => false
"1 a" => false
"2e10" => true
" -90e3   " => true
" 1e" => false
"e3" => false
" 6e-1" => true
" 99e2.5 " => false
"53.5e93" => true
" --6 " => false
"-+3" => false
"95a54e53" => false
 */
public class ValidNumber {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	

	}
	 @Test
	 public void testIsNumber() {
		 assertEquals(true, isNumber("0"));
		 assertEquals(true, isNumber(" 0.1"));
		 assertEquals(false, isNumber("abc"));
		 assertEquals(false, isNumber("1 a"));
		 assertEquals(true, isNumber("2e10"));
		 assertEquals(true, isNumber("-90e3"));
		 assertEquals(false, isNumber(" 1e"));
		 assertEquals(false, isNumber(" e3"));
		 assertEquals(true, isNumber(" 6e-1"));
		 assertEquals(false, isNumber("99e2.5"));
		 assertEquals(true, isNumber("53.5e93"));
		 assertEquals(false, isNumber("--6 "));
		 assertEquals(false, isNumber("-+3"));
		 assertEquals(false, isNumber("95a54e53"));
		 assertEquals(false, isNumber("959440.94f"));
		
	 }
	
	 
	 public boolean isNumber(String s) {
		 char[] charArr = s.trim().toCharArray();
		 if(charArr.length==0)
			 return false;
		 
		 boolean dotorEflag = false;
		 boolean posOrnagativeFlag = false;
		 if(charArr.length==1 && !Character.isDigit(charArr[0])) {
			return false; 
		 }
		 
		 for(int i=0; i<charArr.length; i++) {
			 if(!Character.isDigit(charArr[i]) && charArr[i]!='.' && charArr[i]!='e' && charArr[i]!='+' && charArr[i]!='-') {
				 return false;
			 }
			 if(charArr[i]=='.') {
				  if(dotorEflag == true) {
					 return false;
				 }
				
				 if(i+1 >= charArr.length) {
					 return false;
				 }
				 if(!Character.isDigit(charArr[i+1])) {
					 return false;
				 }
			 }
			 
			 if(charArr[i]=='e') {
				 if(i==0)
					 return false;
				 dotorEflag = true;
				  if(i+1 >= charArr.length) {
						 return false;
				  }
				  if(!Character.isDigit(charArr[i+1]) && charArr[i+1]!='+' &&  charArr[i+1]!='-') {
					  return false;
				  }
			 }
			 
			 if(charArr[i]=='+' || charArr[i]=='-') {
				 if(posOrnagativeFlag)
					 return false;
				 
				 posOrnagativeFlag = true;
				 
				 
				 
			 }
		 }
		 
		 
		 return true;
	 }
	 
	 
	 
	 
	/* public boolean isNumber(String s) {
	        try{
	        	if(s.contains("f") || s.contains("F") || s.contains("d") || s.contains("D"))
	                return false;
	           Double val =  Double.parseDouble(s.trim());
	           Float.parseFloat(s.trim());
	          
	           return true;
	           
	        }catch(Exception ex){
	            
	        }
	        return false;
	    
	 }*/
	 
	
}
