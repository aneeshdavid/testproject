package com.leet_hard;

import java.util.Arrays;

public class FindClosestPalindrome {

	public static void main(String args[]) {
		//new FindClosestPalindrome().findClosestPalindrome("11987");
		
		String result = new FindClosestPalindrome().nearestPalindromic("1213");
		System.out.println(result);
	}
	
	public void findClosestPalindrome(String n) {
		
		int mid = n.length()/2;
		int firstHalf = Integer.parseInt(n.substring(0,mid+1));
		if(n.charAt(mid)=='9') {
			firstHalf += 1 ;
		}else {
			firstHalf -= 1 ;
		}
		String mirror = getMirror(firstHalf+"");
		String result = firstHalf+mirror;
		System.out.println("input: "+ n);
		System.out.println("result: "+ result);
	}

	private String getMirror(String firstHalf) {
		int len = firstHalf.length();
		String mirror = "";
		for (int i = len-2; i >= 0; i--) {
			mirror+=firstHalf.charAt(len-2-i)+"";
		}
		return mirror;
	}
	
	
	
	
	
	
	public String nearestPalindromic(String n) {
        long maxPalindrome =0;
        long minPalindrome =0;
        long num = Long.parseLong(n);
        long newNum = num;
        int unit =1;
        for(int i =0; i<n.length()/2 - 1; i++) {
            unit*=10;
        }
        
        do{
            maxPalindrome = makePalindrom(newNum+=unit);
        }while(maxPalindrome <= num);
        
        newNum = num;
        
        do{
           //if(num<unit)
           //    break;
            minPalindrome = makePalindrom(newNum-=unit);
        }while(minPalindrome >= num);
        
        long nearestpalindrome = maxPalindrome-num >= num-minPalindrome ? minPalindrome : maxPalindrome;
        return nearestpalindrome+"";
    }
    
    private long makePalindrom(long num){
        char[] arr = String.valueOf(num).toCharArray();
        int head = 0;
        int tail = arr.length-1;
        while(head<tail){
           arr[tail--]= arr[head++];
        }
        return Long.parseLong(new String(arr));
    }
	
	
	
	
	
	
	
	
	
}
