package com.leet_hard;

public class IntegerToEnglishWords {
static String[] oneToNineteenArray = {"","One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen",
		"Fourteen","Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
static String[] tenToNienty = {"","","Twenty","Thirty","Forty","Fifty","Sixty","Seventy","Eighty","Ninety"};
	public String numberToWords(int num) {
        if(num==0) 
        	return "Zero";
   
        StringBuilder sb= new StringBuilder();
        int billions = num/1000000000;
        if(billions!=0)
        	sb.append(helper(billions) + " Billion ");
        num = num%1000000000;
        int millions = num/1000000;
        if(millions!=0)
        	sb.append(helper(millions) + " Million ");
        num = num%1000000;
        int thousands = num/1000;
        if(thousands!=0)
        	sb.append(helper(thousands) + " Thousand ");
        num = num%1000;
        
        sb.append(helper(num));
        return sb.toString().trim();
		
    }
	
	
	private static String helper(int num) {
		StringBuilder sb = new StringBuilder();
		int hundred = num/100;
		if(hundred!=0) {
			sb.append(oneToNineteenArray[hundred]+" Hundred ");
		}
		num=num%100;
		
		int ten = num/10;
		if(ten>=2) {
			sb.append(tenToNienty[ten]+" ");
		}
		if(num>19) {
			num = num%10;
		}
		sb.append(oneToNineteenArray[num]);
		
		return sb.toString();
	}


	public static void main(String[] args) {
		int num=20;
		System.out.println(new IntegerToEnglishWords().numberToWords(num));
//1234567891
	}

}
