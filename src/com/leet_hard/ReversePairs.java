package com.leet_hard;

public class ReversePairs {
	Node head = null;
	public static void main(String[] args) {
		int[] nums = {3,5,2,1,3,1,2};
		new ReversePairs().reversePairs(nums);
		System.out.println("Completed");
	}
	
	static class Node{
		int val;
		int count;
		Node left;
		Node right;
		
		public Node(int val) {
			this.val = val;
			this.count =1;
		}
	}
	
	public int reversePairs(int[] nums) {
		int count = 0;
		for (int i = 0; i < nums.length; i++) {
			count+=search(nums[i]*2, head);
			head =  addToBST(head, nums[i]);
		}
		return count;
	}

	private int search(int target, Node head) {
		if(head==null) {
			return 0;
		}
		else if(target==head.val) {
			return head.count;
		}
		else if(head.val>target) {
			return head.count+search(target,head.left);
		}else {
			return search(target,head.right);
		}
	}

	private Node  addToBST(Node node, int i) {
		if(node==null) {
			return new Node(i);
		}else if(i==node.val) {
			node.count++;
		}else if(i<node.val) {
			node.left = addToBST(node.left, i);
		}else{
			node.count++;
			node.right=addToBST(node.right,i);

		}
		return node;
	}

}
