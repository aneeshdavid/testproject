package com.leet_hard;

import java.util.Arrays;

/**
 Given a char array representing tasks CPU need to do. It contains capital letters A to Z where different letters represent different tasks. Tasks could be done without original order.
  Each task could be done in one interval. For each interval, CPU could finish one task or just be idle.

However, there is a non-negative cooling interval n that means between two same tasks, there must be at least n intervals that CPU are doing different tasks or just be idle.

You need to return the least number of intervals the CPU will take to finish all the given tasks.

 Example:

Input: tasks = ["A","A","A","B","B","B"], n = 2
Output: 8
Explanation: A -> B -> idle -> A -> B -> idle -> A -> B.
 

Note:

The number of tasks is in the range [1, 10000].
The integer n is in the range [0, 100].



You have following tasks to perform:
A, B, A, C, A, B, C, A, C, B, C
Now assume you can only do 1 task every second and after performing any task, you can pick up similar task only after n seconds, say 5 seconds.

So, you have to perform task A 4 times, task C 4 times and task B 3 times
Lets say you start with task A, this is what your task flow (just made up) will look like:

A __ __ __ __ __ A __ __ __ __ __ A __ __ __ __ __ A

Since you have to take a break of 5 seconds (n) before you can do similar task again, you see 5 __ between each task A

But wait, you have Bs and Cs as well. You can perform those kind of task between two As rather than waiting ideally. But remember you still have to wait 5 seconds (n) between two Bs or Cs. So this is one way you can do that:

A B C __ __ __ A B C __ __ __ A B C __ __ __ A B

 * **/
public class LeastInterval {
	
	public int findProcessTime(String tasks, int n) {
		
		
		char[] charArr = tasks.replace(",","").toCharArray();
		
		int[]  taskArr = new int[26];
		
		for(char c: charArr) {
			taskArr[c-'A']++;
		}
		Arrays.sort(taskArr);
		int time = 0;
		while(taskArr[25]>0) {
			int i = 0; int j = 25;
			while(i<=n && taskArr[j]>0) {
				i++;
				time++;
				taskArr[j]--;
				j = j==0 ?25:j-1;
			}
			Arrays.sort(taskArr);
			if(taskArr[25]==0) {
				break;
			}
			time +=n-i+1;
		}
		
		return time;	
	}

	public static void main(String[] args) {
		String tasks = "A,A,A,B,B,B";
		int idle = 2;
		int processTime = new LeastInterval().findProcessTime(tasks, idle);
		System.out.println(processTime);
	}

}
