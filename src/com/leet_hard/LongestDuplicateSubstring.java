package com.leet_hard;

import java.util.HashMap;
import java.util.Map;

public class LongestDuplicateSubstring {

	public String longestDupSubstring(String S) {
		String ans = null;
		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < S.length(); i++) {
			for (int j = i+1; j < S.length(); j++) {
				if(ans!=null && ans.length()>S.length()-i) {
					return ans;
				}
				String subString = S.substring(i,j+1);
				if(ans==null ) {
					if(map.containsKey(subString)) {
						map.put(subString, map.get(subString)+1);
						ans = subString;
					}else {
						map.put(subString, 1);
					}
				}else if(subString.length()>ans.length()) {
					if(map.containsKey(subString)) {
						map.put(subString, map.get(subString)+1);
					}else {
						map.put(subString, 1);
					}
					
					if(map.get(subString)>=2) {
						if(ans==null || subString.length()>ans.length()) {
							ans = subString;
						}
					}
				}
				
				
			}
		}
		return ans==null?"":ans;
    }
	
	
	public static void main(String[] args) {
		String S = "banana";
		String result = new LongestDuplicateSubstring().longestDupSubstring(S);
		System.out.println(result);
	}

}
