package com.leet_hard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SubstringConcatenationAllWords {
	
	public List<Integer> findSubstring(String s, String[] words) {
		List<Integer> indexList = new ArrayList<>();
		if(words.length==0)
            return indexList;
		int concatStringLength = words[0].length()* words.length;
		int wordLen = words[0].length();
		int strLength = s.length();
		int i = 0;
		int j = concatStringLength;
		Map<String,Integer> wordMap  = getCountMap(words);
		while(j<=strLength) {
			String subStr = s.substring(i,j);
			boolean matchFlag = true;
			Map<String,Integer> subStringMap = getSubStringMap(subStr, words, wordLen);
			for (Map.Entry<String,Integer> entry : wordMap.entrySet()) {
				if(!(wordMap.containsKey(entry.getKey()) && (entry.getValue() == subStringMap.get(entry.getKey())))) {
					matchFlag = false;
					break;
				}
			}
			if(matchFlag) {
				indexList.add(i);
			}
			i++;
			j++;
		}
	
		return indexList;
	}
	private Map<String, Integer> getSubStringMap(String str, String[] words, int wordLen) {
		int i =0;
		Map<String,Integer> wordMap = new HashMap<>();
		for (String string : words) {
			if(str.contains(string)){
				if(wordMap.containsKey(string)) {
					wordMap.put(string, wordMap.get(string)+1);
				}else {
					wordMap.put(string,1);
				}
				str = str.replaceFirst(string, "");
			}
		}
		return wordMap;
	}

private Map getCountMap(String[] words) {
	HashMap<String,Integer> wordMap = new HashMap<>();
	for (String str : words) {
		if(wordMap.containsKey(str)) {
			wordMap.put(str, wordMap.get(str)+1);
		}else {
			wordMap.put(str,1);
		}
	}
	return wordMap;
}
	
	public static void main(String[] args) {
		String s = "ababaab";
		String[] words = {"ab","ba","ba"};
		List<Integer> result = new SubstringConcatenationAllWords().findSubstring(s, words);
		System.out.println(Arrays.toString(result.toArray()));
	}
}

