package com.leet_hard;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class ShortestSubarraySumAtLeast_K {

	 public int shortestSubarray(int[] A, int K) {
	  int minLength = Integer.MAX_VALUE ;
	  int len = A.length;
	  if(len==1 & A[0]>=K) {
		  minLength = 1;
	  }
	  
		 for (int i = 0; i < len-1; i++) {
			 int sum = A[i];
			 if(sum >= K) {
				 minLength =1;
			 }
			 int j;
			for (j = i+1; j < len; j++) {
				sum+= A[j];
				if(sum >= K) {
					int currentLength = j-i+1;
					minLength = Math.min(currentLength, minLength);
				}
			}
			if(sum<K && minLength == Integer.MAX_VALUE) {
				i=j;
			}
		}
		 if(minLength == Integer.MAX_VALUE) {
			 return -1;
		 }
		 return minLength;
	 }
	
	 
	 public int shortestSubarray_1(int[] A, int K) {
		 int N = A.length;
	        long[] P = new long[N+1];
	        for (int i = 0; i < N; ++i)
	            P[i+1] = P[i] + (long) A[i];

	        // Want smallest y-x with P[y] - P[x] >= K
	        int ans = N+1; // N+1 is impossible
	        Deque<Integer> monoq = new LinkedList(); //opt(y) candidates, as indices of P
	        
	        for (int y = 0; y < P.length; ++y) {
	            // Want opt(y) = largest x with P[x] <= P[y] - K;
	        	//int last = monoq.getLast();
	        	//System.out.println("Y = "+y+" P[y]:"+P[y]+"  P[monoq.getLast()]:"+ P[last] );
	            while (!monoq.isEmpty() && P[y] <= P[monoq.getLast()]) // 0, 2, 1, 3
	                monoq.removeLast();
	            while (!monoq.isEmpty() && P[y] >= P[monoq.getFirst()] + K)
	                ans = Math.min(ans, y - monoq.removeFirst());

	            monoq.addLast(y);
	        }

	        return ans < N+1 ? ans : -1;
	 }
	
	public static void main(String[] args) {
		int K = 3;
		int[] A = {2,-1,2};
		int result = new ShortestSubarraySumAtLeast_K().shortestSubarray_1(A, K);
		System.out.println(result);
	}

	
	
}
