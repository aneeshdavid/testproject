package com.leet_hard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TextJustification {

	 public List<String> fullJustify(String[] words, int maxWidth) {
		 List<String> resultList = new ArrayList<>();
		 int strLength=0;
		 int currentWidth=0;
		 List<String> tempList=new ArrayList<>();
		 for (String word : words) {
			if(currentWidth+word.length() > maxWidth) {
				if(tempList.size()==1) {
					resultList.add(justify(tempList, maxWidth));
				}else {
					resultList.add(justify(tempList, strLength, maxWidth));
				}
				tempList.clear();
				tempList.add(word);
				strLength = word.length();
				currentWidth=word.length()+1;
			}else {
				tempList.add(word);
				strLength +=word.length();
				currentWidth+=word.length()+1;
			}
			 
		 }
		 if(tempList.size()>0) {
			 resultList.add(justify(tempList, maxWidth));
		 }
		 return resultList;
	 }
	
	private static String justify(List<String> tempList, int maxWidth) {
		StringBuilder sb =new StringBuilder();
		for (String word: tempList) {
            if (sb.length() > 0) {
            	sb.append(" ");
            }
            sb.append(word);
        }
		
		while (sb.length() < maxWidth) {
			sb.append(" ");
        }
		return sb.toString();
	}

	private static String justify(List<String> tempList , int currentLength, int maxWidth) {		
		int freeSpace = maxWidth - currentLength; 
        int size = tempList.size() - 1;
        int min = freeSpace / size; 
		StringBuilder sb =new StringBuilder();
		int count = 0;
		for ( int i=0; i<tempList.size(); i++) {
			sb.append(tempList.get(i));
			if(i==tempList.size()-1) {
				continue;
			}
			for (int j = 1; j <= min; j++) {
				sb.append(" ");
			}
			if (count < (freeSpace % tempList.size())) {
				sb.append(" ");
                count++;
            }
		}
		return sb.toString();
		
	}



	public static void main(String[] args) {
		String[] words = {"Science","is","what","we","understand","well","enough","to","explain",
		         "to","a","computer.","Art","is","everything","else","we","do"};
		int maxWidth = 20;
		
	List<String> res = 	new TextJustification().fullJustify(words, maxWidth);
	System.out.println(Arrays.toString(res.toArray()));

	}

	
	
}
