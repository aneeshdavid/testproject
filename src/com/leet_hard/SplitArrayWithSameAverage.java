package com.leet_hard;

import java.util.Arrays;

public class SplitArrayWithSameAverage {

	public boolean splitArraySameAverage(int[] A) {
       int sum =0;
		for (int i : A) {
			sum+=i;
		}
		double target = (double)sum/A.length;
		int mid = A.length/2;
		return splitArrayHelper(A, target, mid, 1);
    }
	
	
	
	private boolean splitArrayHelper(int[] A, double target, int mid, int k) {
		if(mid<k)
			return false;
		for (int i = 0; i < mid; i++) {
			int[] tempArr = Arrays.copyOfRange(A, 0, mid);
			for (int j = 0; j < k; j++) {
				tempArr[j] = A[mid+j];
			}
			double average= average(tempArr);
			if(average==target) {
				return true;
			}
		}
		
		if(k<A.length-mid) {
			return splitArrayHelper(A, target,mid, ++k);
		}
		return false;
	}



	private double average(int[] tempArr) {
		double sum = 0 ;
		for (int i : tempArr) {
			sum+=i;
		}
		return (double)sum/tempArr.length;
	}



	public static void main(String[] args) {
		int[] A = {2,0,5,6,16,12,15,12,4};
		boolean flag =  new SplitArrayWithSameAverage().splitArraySameAverage(A);
		System.out.println(flag);
	}

}
