package com.datastructures.linkedlist;

/**
 * @author adavid
 *
 * Jan 29, 2019
 */

public class MyLinkedList<T> {
public Node head;
  public static class Node<T>{
    public Node next;
    public T data;
    Node(T data){
      this.data = data;
      this.next =null;
    }
  }
  
  public void insert(T data) {
    Node<T> newNode = new Node<T>(data);
    if(head==null) {
      head = newNode;
    }else {
    	Node last = head;
    	while(last.next!=null) {
    		last = last.next;
    	}
      	last.next = newNode;
    }
  }
  
  public void print(Node node) {
    
    if(node==null) {
      System.out.println("Linked list is null");
    }else {
      while(node!=null) {
        System.out.println(node.data);
        node = node.next;
      }
    }
    
  }
  
 
  
  Node reverse(Node head)
  {
       if(head.next == null) 
         return head;
       
       Node newHead = reverse(head.next);
       head.next.next = head;
       head.next = null;
       return newHead;
  }
  
  public static void main(String[] args) {
    MyLinkedList<Integer> linkedList = new MyLinkedList<Integer>();
    linkedList.insert(1);
    linkedList.insert(2);
    linkedList.insert(3);
    System.out.println("Done");
    
    linkedList.print(linkedList.head);
    System.out.println("Reverse");
    linkedList.reverse(linkedList.head);
    System.out.println("");
  }

}

