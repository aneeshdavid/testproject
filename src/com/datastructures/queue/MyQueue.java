package com.datastructures.queue;

import java.util.ArrayList;

/**
 * @author adavid
 *
 * Nov 28, 2018
 */

public class MyQueue<T> {
  
  private ArrayList<T> list = new ArrayList<T>();
  int front = 0;
  int back = 0;
  private void enQueue(T t) {
    list.add(t);
    back++;
  }
  private void deQueue() {
    if(!isEmpty()) {
   // list.remove(front);
    front++;
    }
  }
  private boolean isEmpty() {
    if(list.isEmpty())
      return true;
    else
    return false;
  }
  private void printQueue() {
    String str="";
    for (int i =front;i<back;i++) {
      str+="---\n";
      str+="|"+list.get(i)+"|";
      str+="\n---";
    }
    System.out.println(str);
    System.out.println("________________");
  }
  public static void main(String[] args) {
    MyQueue<Integer> myQueue = new MyQueue<Integer>();
    myQueue.enQueue(1);
    myQueue.enQueue(2);
    myQueue.enQueue(3);
    myQueue.printQueue();
    
    myQueue.deQueue();
    myQueue.deQueue();
    myQueue.deQueue();
    myQueue.printQueue();
  }

}

