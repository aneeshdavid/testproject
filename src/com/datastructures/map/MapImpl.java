package com.datastructures.map;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * @author adavid
 *
 * Jan 31, 2019
 */

public class MapImpl<k,v> implements IMap<k,v> {
  
  List<Node<k,v>> nodeList = new ArrayList<Node<k,v>>();
  int bucketSize = 10;
  int currentSize = 0;

  
  @Override
  public void put(k key, v value) {
   int index = key.hashCode()%bucketSize;
   Node<k,v> existingNode = nodeList.get(index);
   Node<k,v> newNode = new Node<k,v>(key, value);
   if(existingNode != null) {
     while(existingNode!=null) {
       existingNode = existingNode.next;
     }
     existingNode = newNode;
   }else {
    nodeList.set(index,newNode);
    currentSize++;
   }
   if((0.1*currentSize)/ bucketSize >=  0.7) {
     increaseSize();
   }
  }
  
  @Override
  public void remove(k key) {
    int index = key.hashCode()%bucketSize;
    Node<k,v> existingNode = nodeList.get(index);
    Node<k,v> prev = null;
    while(existingNode!=null) {
      if(existingNode.key.equals(key)) {
        break;
      }
      prev = existingNode;
      existingNode = existingNode.next;
    }
    currentSize --;
    if (prev != null) {
      prev.next = existingNode.next; 
    }
    else {
      nodeList.set(currentSize, existingNode.next); 
    }
    
  }
  private void increaseSize() {
    List<Node<k,v>> tempNodeList = nodeList;
    bucketSize = bucketSize*2;
    currentSize = 0;
    
    nodeList.clear();
    
    for(Node<k,v>  node: tempNodeList) {
      while(node!=null) {
        put(node.key, node.value);
        node = node.next;
      }
    }
    
  }

  @Override
  public v get(k key){
    int index = key.hashCode()%bucketSize;
    Node<k,v> node = nodeList.get(index); 
    while(node!=null) {
      if(node.key.equals(key)) {
        return node.value;    
      }
      node = node.next;
    }
    return null;
  }

  @Override
  public void clear() {
    // TODO Auto-generated method stub
    
  }

  @Override
  public Set<Node<k, v>> entrySet() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Set<k> keySet() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean containsKey() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public boolean containsValue() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public int getSize() {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public boolean isEmpty() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public Collection<v> getValues() {
    List<v> result = new ArrayList<>();
    return result;
  }

  
}

