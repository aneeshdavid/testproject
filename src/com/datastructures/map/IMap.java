package com.datastructures.map;

import java.util.Collection;
import java.util.Set;

public interface IMap<k,v> {
  void put(k key,v value);
  void remove(k key);
  v get(k key);
  void clear();
  Set<Node<k,v>> entrySet();
  Set<k> keySet();
  boolean containsKey();
  boolean containsValue();
  int getSize();
  boolean isEmpty();
  Collection<v> getValues();
}
