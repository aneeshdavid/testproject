package com.datastructures.stack;

import java.util.ArrayList;

/**
 * @author adavid
 *
 * Nov 28, 2018
 */

public class MyStack  <T> {
  private ArrayList<T> list = new ArrayList<T>();
  int top = -1;
  
  public void push(T t) {
    list.add(t);
    top++;
  }
  public T pop() {
   T val = list.get(top);
    list.remove(top);
    top--;
    return val;
  }
  public boolean isEmpty() {
    if(list.isEmpty())
      return true;
    else
    return false;
  }
  public void printStack() {
  for (int i =list.size()-1;i>-1;i--) {
    System.out.println("|"+list.get(i)+"|");
    System.out.println("___");
  }
  System.out.println("________________");
}
  public static void main(String[] args) {
    MyStack<Integer> myStack = new MyStack<Integer>();
    myStack.push(1);
    myStack.push(2);
    myStack.push(3);
    myStack.printStack();
    
    myStack.pop();
    myStack.printStack();
    
    //TEST
   boolean result = isBalanced("((()))");
   System.out.println(result);
  }

  
  private static boolean isBalanced(String inputStr) {
    char[] chrArray = inputStr.toCharArray();
    MyStack<String> balStack = new MyStack<String>();
    for (char ch : chrArray) {
      if(ch=='(') {
        balStack.push(ch+"");
      }
      else if(ch==')') {
        if(balStack.isEmpty()) {
          balStack.push(ch+"");
        }else {
        balStack.pop();
        }
      }
    }
    if(balStack.isEmpty()) {
      return true;
    }
    return false;
  }
}

