package com.datastructures.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author adavid
 *
 * Sep 19, 2019
 */

public class SortByComparator {

  static class Epmployee{
    String name;
    int age;
    String sex;
    public String getName() {
      return name;
    }
    public void setName(String name) {
      this.name = name;
    }
    public int getAge() {
      return age;
    }
    public void setAge(int age) {
      this.age = age;
    }
    public String getSex() {
      return sex;
    }
    public void setSex(String sex) {
      this.sex = sex;
    }
    public Epmployee(String name,  int age,  String sex) {
      this.name = name;
      this.age=age;
      this.sex=sex;
    }
    
  }
  
  public static void main(String[] q) {
    
    List<Epmployee> empList = new ArrayList<>();
    empList.add(new Epmployee("D",12,"m"));
    empList.add(new Epmployee("A",10,"m"));
    empList.add(new Epmployee("C",16,"f"));
    System.out.println("Before Sort");
    printList(empList);
    /*Collections.sort(empList, new Comparator<Epmployee>() {

      @Override
      public int compare(Epmployee o1, Epmployee o2) {
       return o1.name.compareToIgnoreCase(o2.name);
      }
    });
    System.out.println("After Sort Name");
    printList(empList);
    
    Collections.sort(empList, new Comparator<Epmployee>() {

      @Override
      public int compare(Epmployee o1, Epmployee o2) {
        if(o1.age==o2.age) {
          return 0;
        }
        if(o1.age>o2.age) {
          return 1;
        }
        
        return -1;
       
      }
    });*/
    empList.sort(Comparator.comparing(Epmployee::getName).thenComparing(Epmployee::getAge));
    System.out.println("After Age Name");
    printList(empList);
  }
  
  private static void printList(List<Epmployee> empList) {
    empList.forEach(e->{
      System.out.println( e.name+","+e.age+","+e.sex);
    });
  }
  
}

