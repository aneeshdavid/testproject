package com.datastructures.circular_queue;

/**
 * @author adavid
 *
 * Nov 29, 2018
 */

public class MyCircularQueue {
  int size;
  int [] circularQueue = null;
  
  int front = 0;
  int rear = 0;
  public MyCircularQueue(int size) {
   this.size = size;
   circularQueue = new int[size];
  }
  private void enQueue(int value) {
    int pos = rear % size; 
    circularQueue[pos] = value;
    rear++;
  }
  private void deQueue() {
    front++;
  }
  private void printQueue() {
    for(int i=front; i<size; i++) {
      System.out.println(circularQueue[i]);
    }
    System.out.println("---------------");
  }
  public static void main(String[] args) {
    MyCircularQueue myCircularQueue = new MyCircularQueue(3);
    myCircularQueue.enQueue(1);
    myCircularQueue.enQueue(2);
    myCircularQueue.enQueue(3);
    myCircularQueue.printQueue();
    myCircularQueue.enQueue(4);
    myCircularQueue.printQueue();
    myCircularQueue.enQueue(5);
    myCircularQueue.printQueue();
  //  myCircularQueue.deQueue();
    //myCircularQueue.printQueue();
    
    
  }

}

