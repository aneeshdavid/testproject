package com.datastructures.binary_search_tree;

import java.util.Arrays;

/**
 * @author adavid
 *
 * Dec 2, 2018
 */

public class MyBinarySearchTree {

  int root;

  public boolean find(int[] array, int node) {
    int mid = Math.round(array.length/2);
    int[] newArray = null;
    if(mid<1) {
      if(array[0] ==node) {
        return true;
      }else {
        return false;
      }
    }else {
    if(array[mid] < node) {
      newArray = Arrays.copyOfRange(array, mid+1, array.length);
    }else {
      newArray = Arrays.copyOfRange(array, 0, mid-1);
    }
    
    return find(newArray,node);
    }
   
  }
  
  public static void main(String[] args) {
    
    int[] inputArr = {1,2,3,4,5,6,7,8,9}; 
    MyBinarySearchTree bst = new MyBinarySearchTree();
    boolean result = bst.find(inputArr, 36);
    System.out.println("Result : "+result);
  }

}

