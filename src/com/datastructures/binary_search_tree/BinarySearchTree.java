package com.datastructures.binary_search_tree;

/**
 * @author adavid
 *
 * Dec 3, 2018
 */

public class BinarySearchTree {
    Node root;
  //Node temp;
  class Node{
    int item;
    Node left;
    Node right;
    
    
    public Node(int item) {
      this.item = item;
    }

  }
  void insert(int item){
    root = insertItem(root, item);
  }

  public Node insertItem(Node root, int item) {
    if (root == null) {
      root = new Node(item);
      return root;
    }

    if (root.item <= item) {
      root.right = insertItem(root.right, item);
    } else {
      root.left = insertItem(root.left, item);
    }

    return root;
  }
  private void print_inorder(Node root) {
    if(root!=null) {
       print_inorder(root.left);
       System.out.println(root.item);
       print_inorder(root.right);
    }
   
  }
  private void print_preorder(Node root) {
    if(root!=null) {
      System.out.println(root.item);
      print_preorder(root.left);
      print_preorder(root.right);
   }
  
  }
  private void print_postorder(Node root) {
    if(root!=null) {
      print_postorder(root.left);
      print_postorder(root.right);
      System.out.println(root.item);
   }
  
  }
  public static void main(String[] args) {
    BinarySearchTree tree = new BinarySearchTree();
    tree.insert(50); 
    tree.insert(30); 
    tree.insert(20); 
    tree.insert(40); 
    tree.insert(70); 
    tree.insert(60); 
    tree.insert(80); 

   System.out.println("Inorder");
  tree.print_inorder(tree.root);
  System.out.println("Preorder");
  tree.print_preorder(tree.root);
  System.out.println("Postorder");
  tree.print_postorder(tree.root);
  }

 

}

