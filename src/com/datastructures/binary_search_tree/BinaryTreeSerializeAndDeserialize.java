package com.datastructures.binary_search_tree;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 Serialization is the process of converting a data structure or object into a sequence of bits so that it can be stored in a file or memory buffer, 
 or transmitted across a network connection link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary tree. There is no restriction on how your serialization/deserialization algorithm should work. 
You just need to ensure that a binary tree can be serialized to a string and this string can be deserialized to the original tree structure.
 * **/
public class BinaryTreeSerializeAndDeserialize {

	
	public String serialize(TreeNode root) {
	
		if(root==null)
			return null;
	
			String left = serialize(root.left);
			String right = serialize(root.right);
		  return root.val+","+left+","+ right;
	}
	

	
	
	 public TreeNode deserialize(String data) {
		
		 Queue<String> queue = new LinkedList<>();
		 queue.addAll(Arrays.asList(data.split(",")));
	      return deserialize_helper(queue);
		
	    }
	 
	 public TreeNode deserialize_helper(Queue<String> queue) {
		 String  val = queue.poll();
		 if(val.equals("null"))
			 return null;
		 
		 TreeNode root = new TreeNode(Integer.parseInt(val));
		 root.left = deserialize_helper(queue);
		 root.right = deserialize_helper(queue);
		 
		 return root;
	 }
	
	static class TreeNode {
	     int val;
	     TreeNode left;
	     TreeNode right;
	     TreeNode(int x) { val = x; }
	 }
	
	public static void main(String[] args) {
		TreeNode root = new  TreeNode(1);
		root.left = new  TreeNode(2);
		root.right = new  TreeNode(3);
		root.right.left = new  TreeNode(4);
		root.right.right = new  TreeNode(5);
		
		BinaryTreeSerializeAndDeserialize codec = new BinaryTreeSerializeAndDeserialize();
		String ser = codec.serialize(root);
		System.out.println(ser);
		TreeNode   deserializeRoot = codec.deserialize(ser);
		System.out.println("Success");
	}
	
	

}
