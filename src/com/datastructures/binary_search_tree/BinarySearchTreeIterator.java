package com.datastructures.binary_search_tree;

import java.util.Stack;

public class BinarySearchTreeIterator {

	Stack<TreeNode> myStack;
	    public BinarySearchTreeIterator(TreeNode root) {
	        myStack = new Stack<>();
	        findLeftMostNode(root);
	    }
	    
	    private void findLeftMostNode(TreeNode root){
	        while(root!=null){
	            myStack.push(root);
	            root = root.left;
	        }
	    }
	    
	    /** @return the next smallest number */
	    public int next() {
	        TreeNode topMostNode = myStack.pop();
	        if(topMostNode.right!=null){
	            findLeftMostNode(topMostNode.right);
	        }
	        System.out.println(topMostNode.val);
	        return topMostNode.val;
	    }
	    
	    /** @return whether we have a next smallest number */
	    public boolean hasNext() {
	       return myStack.size()>0;
	    }
	    
	    static class TreeNode {
		      int val;
		      TreeNode left;
		      TreeNode right;
		      TreeNode(int x) { val = x; }
		  }
	    
	    
	    public static void main(String args[]) {
	    	TreeNode root = new TreeNode(7);
	    	root.left = new TreeNode(3);
	    	root.right = new TreeNode(15);
	    	root.right.left = new TreeNode(9);
	    	root.right.right = new TreeNode(20);
	    	
	    BinarySearchTreeIterator iterator = new BinarySearchTreeIterator(root);
	    iterator.next();    // return 3
	    iterator.next();    // return 7
	    iterator.hasNext(); // return true
	    iterator.next();    // return 9
	    iterator.hasNext(); // return true
	    iterator.next();    // return 15
	    iterator.hasNext(); // return true
	    iterator.next();    // return 20
	    iterator.hasNext(); // return false
	    }
	}



