package com.inter.snyposys;

import java.util.Arrays;
import java.util.Random;

public class Algorithm2 {

  public static void main(String[] args) {
    System.out.println("Algorithm2");
    
    int[] arr = new Algorithm2().solution2(58);
    System.out.println(Arrays.toString(arr));
    
  }
  
  public int solution(int[] A) {
    
    Integer result=null;
     for(int ar : A) {
         if(result==null)
         result=ar;
         result*= ar;
     }
     if(result>=0){
         return 1;
     }else{
         return -1;
     }
 }
  
  
  public int[] solution2(int N) {
    int count =0;
    int[] arr = new int[N];
    while(true) {
      int random = getRandomNumberUsingNextInt( 0-N, N);
      if(notContains(arr, random)){
        arr[count] = random;
        count++;
      }
      if(count==N) {
        count = 0;
        int res = countArray(arr);
        if(res==0) {
          return arr;
        }
        arr = new int[N];
      }
    }
  }
  
private boolean notContains(int[] arr, int random) {
  for(int ar : arr) {
    if(ar==random) {
      return false;
    }
  }
    return true;
  }

public static int countArray(int[] A) {
    
    Integer result=null;
     for(int ar : A) {
         if(result==null)
         result=ar;
         result+= ar;
     }
    return result;
 }
  
  
  public static int getRandomNumberUsingNextInt(int min, int max) {
    Random random = new Random();
    return random.nextInt(max - min) + min;
}
}
