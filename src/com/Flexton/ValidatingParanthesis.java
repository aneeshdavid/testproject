package com.Flexton;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class ValidatingParanthesis {

  
  private static final Map<String, String> parMap = new HashMap<>();
  
  public ValidatingParanthesis() {
    parMap.put("}", "{");
    parMap.put(")", "(");
    parMap.put("]", "[");
  }
  
  public static void main(String[] args) {
   String paranthesis = "()[]{}()[]{}()[]{}()[]{}()[]{}()[]{()[]{}})";
   new ValidatingParanthesis().solution(paranthesis);
  }

  void solution(String paranthesis) {
    Stack<String> stack = new Stack<>();
    
    for(int i =0; i<paranthesis.length();i++) {
      String par = paranthesis.charAt(i)+"";
      String parVal = parMap.get(par);
      if(parVal==null) {
        stack.push(par);
      }else {
        if(!stack.isEmpty() && stack.peek().equals(parVal)) {
          stack.pop();
        }else {
          stack.push(par);
        }
      }
      
    }
    System.out.println(stack.isEmpty());
  }

}
