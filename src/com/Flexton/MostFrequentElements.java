package com.Flexton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
Question: Given a non-empty array of integers, return the k most frequent elements.
Example 1:

Input: nums = [1,1,1,2,2,3], k = 2
Output: [1,2]
Example 2:

Input: nums = [1], k = 1
Output: [1]
 * **/
public class MostFrequentElements {
  
  public static void main(String[] g) {
    List<Integer> result  = new MostFrequentElements().solution(new int[] {1,1,1,2,2,3}, 4);
    System.out.println(result);
  }
  
  private List<Integer> solution(int[] inputArr, int k){
    List<Integer> list = new ArrayList<>();
    int i=1;
    Arrays.sort(inputArr);
    int kCount=1;
    while(i<inputArr.length) {
      if(inputArr[i] == inputArr[i-1]) {
        kCount++;
      }else {
        if(kCount >= k) {
          list.add(inputArr[i-1]);
        }
        kCount = 1;
      }
      i++;
    }
    
    if(k==kCount) {
      list.add(inputArr[i-1]);
    }
    
    return list;
  }

}
