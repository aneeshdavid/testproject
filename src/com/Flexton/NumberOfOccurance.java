package com.Flexton;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
Question: Given a list of int, returns the all unique ints with no. of occurances in the list

a = {1,2,3,2,4,1,1} 
O/p:
1 - 3
2 - 2
3 - 1
4 - 1
**/
public class NumberOfOccurance {

  public static void main(String[] args) {
    Map<Integer, Integer> map =  new NumberOfOccurance().solution(new int[] {1,2,3,2,4,1,1} );
    for(Entry<Integer, Integer> ent : map.entrySet()) {
      System.out.println(ent.getKey()+"-"+ent.getValue());
    }
    
    new NumberOfOccurance().solution(null);
  }
  
 Map<Integer, Integer> solution(int[] arr) {
   if(arr==null)
     return null;
   
  Map<Integer, Integer> map = new HashMap<>();
  for(int i =0;i<arr.length; i++) {
    int val = map.getOrDefault(arr[i], 0);
    map.put(arr[i], ++val);
    
  }
  return map;
 }

}
