package com.Flexton;

import java.util.Stack;

public class QueueUsingStack<T> {
  int size=0;
  Stack<T> stack = new Stack<>();
  
   public QueueUsingStack() {
     this.stack = new Stack<>();
   }
  
  public void enqueue(T item) {
    this.stack.add(item);
    this.size++;
  }
  
  public T dequeue() {
    T result = null;
    if(this.size==0) {
      return result;
    }
    result = this.stack.firstElement();
    this.stack.removeElement(result);
    return result;
  }
  
  private int getSize() {
    return  this.size;
  }
  
  public static void main(String[] args) {
    QueueUsingStack<Integer> queue = new QueueUsingStack<>();
    queue.enqueue(1);
    queue.enqueue(2);
    queue.enqueue(3);
    
    System.out.println("Size:"+ queue.getSize());
    
    System.out.println("Dequeue:"+ queue.dequeue());
    System.out.println("Size:"+ queue.getSize());
    System.out.println("Dequeue:"+ queue.dequeue());
    System.out.println("Size:"+ queue.getSize());
    System.out.println("Dequeue:"+ queue.dequeue());
    System.out.println("Size:"+ queue.getSize());
    
  }

 
  

}
