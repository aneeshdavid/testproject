package com.Flexton;
//Input: "abcabcbb"
public class LongestSubstring {

  public static void main(String[] args) {
   
   int ans =  new LongestSubstring().solution("pwwkew");
   System.out.println(ans);
  }
  
  private int solution(String str) {
    int soFarLong =0;
    
    int i =0;
    int j =1;
    while(i<str.length() && j<str.length()) {
      String sofarLongString = str.substring(i,j);
      
      if(!sofarLongString.contains(str.charAt(j)+"")) {
        soFarLong = Math.max(soFarLong, j -i);
      }else {
        soFarLong = Math.max(soFarLong, j -i);
        i=j;
      }
      j++;
    }
    
    
    
    return soFarLong;
  }

}
