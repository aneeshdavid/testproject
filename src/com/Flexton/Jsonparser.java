package com.Flexton;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Consumer;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Jsonparser {
  
  private static int pos = 0;

  public static void main(String[] args) {
  
    String json =  "{\n" +
        "  \"name\":\"Example\",\n" +
        "  \"description\":\"An example\",\n" +
        "  \"process-1\": {\n" +
        "    \"name\":\"My process-1\",\n" +
        "    \"image\":\"Docker Image\",\n" +
        "    \"command\":[\"command\", \"file_name\"],\n" +
        "    \"arguments\":[\"--arg\"]\n" +
        "  },\n" +
        "  \"process-2\": {\n" +
        "    \"name\":\"My process-1\",\n" +
        "    \"image\":\"Docker Image\",\n" +
        "    \"command\":[\"command\", \"file_name\"],\n" +
        "    \"arguments\":[\"--arg\"]\n" +
        "  }\n" +
        "}\n";
    JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
    List<String> list = new LinkedList<>();
    new Jsonparser().getAllKeys(jsonObject, list);
    System.out.println(list);
    
    System.out.println("Get Value");
   new Jsonparser().getValue(jsonObject, "arguments");

   
  }
  private void getAllKeys(JsonObject jsonObject,  List<String> keySet){
    
    Set<Entry<String,JsonElement>> entrySet= jsonObject.entrySet();
    Set<String> keys = new HashSet<>();
    entrySet.forEach(new Consumer<Entry<String,JsonElement>>() {
      @Override
      public void accept(Entry<String, JsonElement> t) {
        keys.add(t.getKey());
      }
    });
    keySet.addAll(keys);
    
    keys.forEach(key->{
      if(jsonObject.get(key).getClass()==JsonObject.class){
        getAllKeys(new JsonParser().parse(jsonObject.get(key).toString()).getAsJsonObject(), keySet);
      }
    });
    
  
  }
  
  private void getValue(JsonObject jsonObject, String key) {
    Set<Entry<String,JsonElement>> entrySet= jsonObject.entrySet();
    for(Entry<String,JsonElement> entry : entrySet) {
      if(entry.getKey().equals(key)) {
        System.out.println(entry.getValue().toString());
        return;
      }else if(entry.getValue().getClass()==JsonObject.class){
        getValue(new JsonParser().parse(entry.getValue().toString()).getAsJsonObject(), key);
      }else if(entry.getValue().getClass()==JsonArray.class){
        JsonArray jarray = entry.getValue().getAsJsonArray();
         for (JsonElement  element : jarray) {
           if(element.getClass() == JsonObject.class) {
           getValue(new JsonParser().parse(element.toString()).getAsJsonObject(), key);
           }
         }
       
      }
    }
  }
 
}
