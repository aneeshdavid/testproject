package com.Flexton;

public class CountOccuranceOffGivenCharacter {

  public static void main(String[] args) {
    new CountOccuranceOffGivenCharacter().solution("abccdefgaa", 'a');
  }
  
  void solution(String str, char c){
    long count = str.chars().filter(ch-> ch==c).count();
    System.out.println(count);
  }

}
