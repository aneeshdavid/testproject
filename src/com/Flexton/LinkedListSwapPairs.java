package com.Flexton;

public class LinkedListSwapPairs {
  
  public static void main(String[] args) {
    Node head = new Node(1); 
    head.next = new Node(2);
    head.next.next = new Node(3);
    head.next.next.next = new Node(4);
    head.next.next.next.next = new Node(5);
    head.next.next.next.next.next = new Node(6);
    new LinkedListSwapPairs().printNode(head);
    new LinkedListSwapPairs().solution(head);
    System.out.println("After pair swap");
    new LinkedListSwapPairs().printNode(head);
    
  }
  //1->2->3
  void solution(Node head){
    Node tmp =head;
    while(tmp!=null && tmp.next!=null) {
      int tempVal = tmp.val;
      tmp.val=tmp.next.val;
      tmp.next.val = tempVal;
      tmp = tmp.next.next;
    }
   
  }
  
  private void printNode(Node node) {
    while(node!=null) {
      System.out.println(node.val);
      node= node.next;
    }
  }
  
  static class Node {
    int val;
    Node next;
    public Node(int val) {
      this.val = val;
    }
  }
  
}
