package com.Flexton;

//https://www.youtube.com/watch?v=3yRMrccLiTI
public class CountUniValueSubTree {

  static class TreeNode{
    public TreeNode(int i) {
      this.val = i;
    }
    int val;
    TreeNode left;
    TreeNode right;
  }
    static class Res{
    public  int val=0;
  }
  public static void main(String[] args) {
    TreeNode root = new TreeNode(5);
    root.left = new TreeNode(1);
    root.right = new TreeNode(5);
    root.left.left = new TreeNode(5);
    root.left.right = new TreeNode(5);
    
    root.right.right = new TreeNode(5);
    Res res= new Res();
    new CountUniValueSubTree().isUnivalue(root, res);
    System.out.println(res.val);
  
  }
  boolean isUnivalue(TreeNode root, Res res){
    if(root==null)
      return true;
    
    boolean left = isUnivalue(root.left, res);
    boolean right = isUnivalue(root.right, res);
    
    if(left && right && (root.left==null || root.val==root.left.val) && (root.right==null || root.val==root.right.val)) {
      res.val++;
      return true;
    }else {
      return false;
    }
    
  }
}
