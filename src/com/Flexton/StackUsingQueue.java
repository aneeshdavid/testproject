package com.Flexton;

import java.util.ArrayDeque;
import java.util.Deque;

public class StackUsingQueue<T> {

  int size;
  Deque<T> queue;
  public StackUsingQueue() {
    this.queue = new ArrayDeque<>();
  }
  
  public void push(T item) {
    queue.add(item);
    size++;
  }
  
  public T pop() {
   
    T result = null;   
    if(size==0) {
      return result;
    }
    result = this.queue.removeLast();
    
    if(result!=null) {
      this.size--;
    }
    return result;
  }
  
  public int getSize() {
  
   return this.size;
  }

  
  public static void main(String[] args) {
   
    StackUsingQueue<Integer> stack = new StackUsingQueue<>();
    stack.push(1);
    stack.push(2);
    stack.push(3);
    System.out.println("Size:"+ stack.getSize());
    
    System.out.println("Pop:"+ stack.pop());
    
    System.out.println("Size:"+ stack.getSize());
    
    System.out.println("Pop:"+ stack.pop());
    
    System.out.println("Pop:"+ stack.pop());
    
    System.out.println("Pop:"+ stack.pop());
    
  }
}
