package com.Flexton;


/**
 * op:
 * Output: mat[M][N] ={{'X', 'O', 'X', 'X', 'X', 'X'},
                      {'X', 'O', 'X', 'X', 'X', 'X'},
                      {'X', 'X', 'X', 'X', 'X', 'X'},
                      {'O', 'X', 'X', 'X', 'X', 'X'},
                      {'X', 'X', 'X', 'O', 'X', 'O'},
                      {'O', 'O', 'X', 'O', 'O', 'O'},
                    };
 * 
 * @author aneeshd
 *
 */
public class MatrixReplaceOwithX {

  public static void main(String[] args) {
    char[][] matrix = new char[][]{{'X', 'O', 'X', 'X', 'X', 'X'},
                                   {'X', 'O', 'X', 'X', 'O', 'X'},
                                   {'X', 'X', 'X', 'O', 'O', 'X'},
                                   {'O', 'X', 'X', 'X', 'X', 'X'},
                                   {'X', 'X', 'X', 'O', 'X', 'O'},
                                   {'O', 'O', 'X', 'O', 'O', 'O'},
                                 };
                                 
                                 
                                 
   new MatrixReplaceOwithX().solution(matrix); 
   
   new MatrixReplaceOwithX().printMatrix(matrix);
   
  }

  void solution(char[][] matrix) {
    //Step1
    new MatrixReplaceOwithX().fillWith(matrix, 'O','_'); 
    
    new MatrixReplaceOwithX().printMatrix(matrix);
    
    //Step2: if there is no chance of flood the fill with X else O
    for(int i =0; i<matrix.length; i++) {
      for(int j =0; j<matrix[0].length; j++) {
        if(matrix[i][j] =='_' && hasGap(i,j,matrix)) {
          matrix[i][j] = 'O';
        }
      }
    }
    
  //Step3
   new MatrixReplaceOwithX().fillWith(matrix, '_','X');    
    
  }

  private boolean hasGap(int i, int j, char[][] matrix) {
    boolean hasGap = true;
    //left
    for(int l = j-1; l>=0 ; l--) {
      if(matrix[i][l]=='X') {
        hasGap = false;
        break;
      }
    }
    
    if(hasGap)
      return true;
    
    //Right
    hasGap = true;
    for(int r =j+1; r<matrix[0].length; r++) {
      if(matrix[i][r]=='X') {
        hasGap = false;
        break;
      }
    }
    if(hasGap)
      return true;
   
    
    //Top
    hasGap = true;
    for(int t=i-1; t>=0; t--) {
      if(matrix[t][j]=='X') {
        hasGap = false;
        break;
      }
    }
    if(hasGap)
      return true;
   
    
    //Bottom
    hasGap = true;
    for(int b=i+1; b<matrix.length; b++) {
      if(matrix[b][j]=='X') {
        hasGap = false;
        break;
      }
    }
      
    if(hasGap)
      return true;
    
    return false;
  }

  private boolean surroundedByX(int i, int j, char[][] matrix) {
    if((i!=0 && matrix[i-1][j] == 'X') && (i!=matrix.length-1 && matrix[i+1][j] =='X') &&
       (j!=0 && matrix[i][j-1] == 'X') && (j!=matrix[0].length-1 && matrix[i][j+1] == 'X')) {
      return true;
    }
    return false;
  }

  //Replace m with n
  private void fillWith(char[][] matrix, char m, char n) {
    for(int i =0; i<matrix.length; i++) {
      for(int j =0; j<matrix[0].length; j++) {
        if(matrix[i][j]==m) {
          matrix[i][j]=n;
        }
      }
    }
  }
  
  private void printMatrix(char[][] matrix) {
    for (char[] cs : matrix) {
      System.out.println(cs);
    }
    System.out.println("=======================");
  }
  
}



