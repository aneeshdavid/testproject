package com.Flexton;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *op:The traversal sequence is 1, 8, 9, 10, 11, 12, 13, 14, 15, 3, 2, 4, 5, 6, 7
 * **/
public class BinaryTreeAntiClockWise {

  public static void main(String[] args) {
    
    TreeNode root = new TreeNode(1);
    root.left = new TreeNode(2);
    root.right = new TreeNode(3);
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(5);
    root.right.left = new TreeNode(6);
    root.right.right = new TreeNode(7);
    root.left.left.left = new TreeNode(8);
    root.left.left.right = new TreeNode(9);
    root.left.right.left = new TreeNode(10);
    root.left.right.right = new TreeNode(11);
    root.right.left.left = new TreeNode(12);
    root.right.left.right = new TreeNode(13);
    root.right.right.left = new TreeNode(14);
    root.right.right.right = new TreeNode(15);
    
    new BinaryTreeAntiClockWise().solution(root);
  }

  void solution(TreeNode tree) {
    
    List<LinkedList<Integer>> resultList = new ArrayList<>();
    traverse(tree, resultList, 0);
    
    int i = 0;
    int j = resultList.size()-1;
    while(i<j) {
     
      LinkedList<Integer> iList = resultList.get(i);
      for(int k=iList.size()-1 ; k>=0;k--) {
        System.out.print(iList.get(k) +" ");
      }
      
      System.out.println(resultList.get(j));
      
      i++;
      j--;
    }
    
    
  }
  
  

  
  
  private void traverse(TreeNode tree, List<LinkedList<Integer>> resultList, int level) {
    if(tree==null) {
      return;
    }
    
    if(resultList.size()>=level+1) {
      resultList.get(level).add(tree.val);
    }else {
      LinkedList<Integer> list = new LinkedList<>();
      list.add(tree.val);
      resultList.add(level, list);
    }
    
    traverse(tree.left, resultList, level+1);
    traverse(tree.right, resultList, level+1);

    
  }





  static class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;
    
    public TreeNode(int val) {
      this.val = val;
    }
  }
}
