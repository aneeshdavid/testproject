package com.Flexton;

public class FindPatternAndPrintNextMissingCharacter {

  public static void main(String[] args) {
    solution("aaaaa");
  }
  
  static void solution(String str) {
    int length = str.length();
    for(int i =str.length()-1; i>0;i--) {
      if((length+1) % i == 0) {
        if(findPatternMatched(i, str)) {
          return;
        }
      }
    }
    System.out.println("Exception");
  }

  private static boolean findPatternMatched(int patternLength, String str) {
   int i = 0;
   String currentPattern = str.substring(0,patternLength);
   
    while(i< str.length()-1 - patternLength) {
      if(!str.substring(i,patternLength).equals(currentPattern)) {
        return false;
      }
      i=i+patternLength;
    }
    
    System.out.println(currentPattern.charAt(currentPattern.length()-1));
    return true;
   
  }
  
  
}
