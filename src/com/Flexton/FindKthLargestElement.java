package com.Flexton;

import java.util.PriorityQueue;

public class FindKthLargestElement {

  public static void main(String[] args) {
    int[] arr = new int[] {3,2,1,5,6,4};
    int k=3;
    new FindKthLargestElement().solution(arr, k);
  }
  void solution(int[] arr, int k){
    PriorityQueue<Integer> queue = new PriorityQueue<Integer>();
    for (int el : arr) {
      queue.add(el);
      if(queue.size()>k)
        queue.poll();
    }
    System.out.println(k+"nd largest element:"+queue.poll());
  }

}
