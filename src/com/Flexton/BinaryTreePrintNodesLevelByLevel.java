package com.Flexton;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BinaryTreePrintNodesLevelByLevel {

  public static void main(String[] args) {
    
    TreeNode tree = new TreeNode(1);
    tree.left = new TreeNode(2);
    tree.left.left = new TreeNode(4);
    tree.left.right = new TreeNode(5);
    
    tree.right = new TreeNode(3);
    tree.right.left = new TreeNode(6);
    tree.right.right = new TreeNode(7);
    
    new BinaryTreePrintNodesLevelByLevel().solution(tree);

  }
  
  void solution(TreeNode tree) {
   
    List<LinkedList<Integer>> resultList = new ArrayList<>();
    traverse(tree, resultList, 0);
    
    resultList.forEach(list->{
      System.out.println(list);
    });
    
    
  }
  
  

  
  
  private void traverse(TreeNode tree, List<LinkedList<Integer>> resultList, int level) {
    if(tree==null) {
      return;
    }
    
   // LinkedList<Integer> list = resultList.get(level);
    if(resultList.size()>=level+1) {
      resultList.get(level).add(tree.val);
    }else {
      LinkedList<Integer> list = new LinkedList<>();
      list.add(tree.val);
      resultList.add(level, list);
    }
    
    traverse(tree.left, resultList, level+1);
    traverse(tree.right, resultList, level+1);

    
  }





  static class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;
    
    public TreeNode(int val) {
      this.val = val;
    }
  }
}
