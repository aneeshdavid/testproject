package com.Flexton;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;

public class NumberOfConsecutivelyRepeatedCharacter {

  public static void main(String[] args) {
    
    new NumberOfConsecutivelyRepeatedCharacter().solution("abbbcddddeffabbbbbb");
  }
  
  void solution(String str) {
    Map<String, Integer> counTmap = new HashMap<>();
    int count =0;
   for(int i=1; i<str.length();i++) {
     if((str.charAt(i)+"").equals(str.charAt(i-1)+"")) {
       count++;
     }else {
       if(count>0) {
         counTmap.put(str.charAt(i-1)+"", counTmap.getOrDefault(str.charAt(i-1)+"", 0) + 1);
         count =0;
       }
     }
   }
   
   if(count>0) {
     counTmap.put(str.charAt(str.length()-1)+"", counTmap.getOrDefault(str.charAt(str.length()-1)+"", 0) + 1);
   }
   
   counTmap.entrySet().forEach(new Consumer<Entry>() {

    @Override
    public void accept(Entry t) {
      System.out.println(t.getKey()+":"+ t.getValue());
       
    }
     
   
  });
}
  
}
