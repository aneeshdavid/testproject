package com.Flexton;

import java.util.Arrays;

public class MergeTwoSortedArray {

  
  public static void main(String[] args) {
   //Input: arr1[] = { 1, 3, 4, 5}, arr2[] = {2, 4, 6, 8}

    new MergeTwoSortedArray().Solution(new int[] {1, 3, 4, 5}, new int[]{2, 4, 6, 8});
  }
  
  
  void Solution(int[] arr1, int[] arr2) {
    int[] mergedArr = new int[arr1.length + arr2.length];
    doMerge(mergedArr, arr1, arr2);
    System.out.println(Arrays.toString(mergedArr));
  }


  private void doMerge(int[] mergedArr, int[] arr1, int[] arr2) {
   
    int i =0;
    int j=0;
    int k=0;
    while(i<arr1.length && j<arr2.length) {
      if(arr1[i] == arr2[j]) {
        mergedArr[k] = arr1[i];
        i++;
        j++;
      }
      else if(arr1[i]<arr2[j]) {
        mergedArr[k] = arr1[i];
        i++;
      }else {
        mergedArr[k] = arr2[j];
        j++;
      }
      k++;
    }
    
    while(i<arr1.length) {
      mergedArr[k] = arr1[i];
      i++;
      k++;
    }
    
    while(j<arr2.length) {
      mergedArr[k] = arr2[j];
      j++;
      k++;
    }
  }

}
