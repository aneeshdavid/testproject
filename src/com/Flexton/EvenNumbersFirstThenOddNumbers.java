package com.Flexton;

import java.util.Arrays;

public class EvenNumbersFirstThenOddNumbers {

  public static void main(String[] args) {
    //[3,2,1,4] ==> [2,4,3,1]
    int[] arr = new int[] {3,2,1,4,6,7,9,8};
    new EvenNumbersFirstThenOddNumbers().solution(arr);
    System.out.println(Arrays.toString(arr));
  }

  void solution(int[] arr) {
    
    int i =0;
    int j =1;
    
    while(j<arr.length) {
      if(arr[j]%2==0) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
         i++;
      }
      j++;
    }
    
  }
  
  
  
}
