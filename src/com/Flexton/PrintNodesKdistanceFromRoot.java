package com.Flexton;

import java.util.ArrayList;
import java.util.List;

public class PrintNodesKdistanceFromRoot {

  public static void main(String[] args) {
   
    TreeNode tree = new TreeNode(1);
    tree.left = new TreeNode(2);
    tree.left.left = new TreeNode(4);
    tree.left.right = new TreeNode(5);
    
    tree.right = new TreeNode(3);
    tree.right.left = new TreeNode(8);
    
    new PrintNodesKdistanceFromRoot().solution(tree, 2);

  }
  
  void solution(TreeNode root, int distance) {
    List<List<Integer>> nodes = new ArrayList<>();
    getNodesByLevel(root, distance, nodes, 0);
    
    if(distance == nodes.size()-1) {
      System.out.println(nodes.get( nodes.size()-1));
    }else {
      System.out.println("Tree does not have "+distance+" distance");
    }
    
  }
  
  private void getNodesByLevel(TreeNode root, int distance, List<List<Integer>> nodes, int level) {
   if(root==null || level > nodes.size()) {
     return;
   }
    
    List<Integer> levelNode ;
    if(nodes.size()> level) {
      levelNode = nodes.get(level);
    }else {
      levelNode = new ArrayList<>();
      nodes.add(levelNode);
    }
    levelNode.add(root.item);
    
    
    
    getNodesByLevel(root.left, distance, nodes, level+1);
    getNodesByLevel(root.right, distance, nodes, level+1);
    
    
  }

  static class TreeNode{
    int item;
    TreeNode left;
    TreeNode right;
     public TreeNode( int item) {
       this.item = item;
     }
  }

}
