package com.Flexton;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class NumberOfOccouranceInTheList {

  public static void main(String[] args) {
  
    new NumberOfOccouranceInTheList().solution(new int[]{1,2,3,2,4,1,1});
 
  }
  void solution(int[] arr){
    
    List<Integer> list =Arrays.stream(arr).boxed().collect(Collectors.toList());
    Map<String,Long> result = list.stream().collect(Collectors.groupingBy(e-> e.toString(), Collectors.counting()));
   
    result.entrySet().forEach(new Consumer<Entry<String, Long>>() {
      @Override
      public void accept(Entry<String,Long> t) {
       System.out.println(t.getKey()+":"+t.getValue());
      }
    });
    
  }
}
