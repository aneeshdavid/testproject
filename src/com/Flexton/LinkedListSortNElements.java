package com.Flexton;

public class LinkedListSortNElements {

  public static void main(String[] args) {
    Node head = new Node(1); 
    head.next = new Node(2);
    head.next.next = new Node(3);
    head.next.next.next = new Node(4);
    head.next.next.next.next = new Node(5);
    head.next.next.next.next.next = new Node(6);
    new LinkedListSortNElements().printNode(head);
    int k=3;
    new LinkedListSortNElements().solution(head, k);
    System.out.println("After pair swap");
    new LinkedListSortNElements().printNode(head);
    
  }
  void solution( Node head, int k){
    Node tmp = head;
    while(tmp!=null) {
      int count = k-1;
     int[] arr = new int[k];
     Node innerTemp =tmp;
      while(count>=0 && innerTemp!=null) {
        arr[count] = innerTemp.val;
        innerTemp = innerTemp.next;
        count--;
      }
      
     
       innerTemp =tmp;
       int i =0;
      while(i<k && innerTemp!=null) {
        innerTemp.val = arr[i];
        innerTemp = innerTemp.next;
        i++;
      }
      tmp = innerTemp;
    }
  }
  
  private void printNode(Node node) {
    while(node!=null) {
      System.out.println(node.val);
      node= node.next;
    }
  }
  
  static class Node {
    int val;
    Node next;
    public Node(int val) {
      this.val = val;
    }
  }
  
}
