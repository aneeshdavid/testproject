package com.Flexton;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

/*
 
 1
 /\
 2 3
 / /\
 4 5 6
   /\
   7 8
 */
public class TreeProblem {

  static class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;
    
    public TreeNode(int val) {
      this.val=val;
    }
  }
  
  Map<Integer,Queue<TreeNode>> map= new HashMap<>();
  
  
  void solution(TreeNode root, Queue<TreeNode> queue) {
    queue.add(root);
    
    solution(root.right, queue); 
    
    
    
    
   /* while(!queue.isEmpty()) {
      TreeNode currRoot = queue.poll();
      System.out.println(currRoot.val);
      queue.add(currRoot.right);
      queue.add(currRoot.left);
    }*/
    
  }
  
  
  
  
}
