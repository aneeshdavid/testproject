package com.Flexton;

public class RainWaterTrap {

  /***
 Question:

Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it is able to trap after raining.
Examples:

Input: arr[]   = {2, 0, 2}
Output: 2

We can trap 2 units of water in the middle gap.

Input: arr[]   = {3, 0, 2, 0, 4}
Output: 7

We can trap "3 units" of water between 3 and 2,
"1 unit" on top of bar 2 and "3 units" between 2 
and 4.

Input: arr[] = [0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1]
Output: 6

Trap "1 unit" between first 1 and 2, "4 units" between
first 2 and 3 and "1 unit" between second last 1 and last 2 


   */
  public static void main(String[] args) {
    int[] inputArr = new int[] {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
  Long time = System.currentTimeMillis();
  new RainWaterTrap().solution(inputArr);
  System.out.println(System.currentTimeMillis() - time);
  time = System.currentTimeMillis();
  new RainWaterTrap().solution_optimized(inputArr);
  System.out.println(System.currentTimeMillis() - time);
  }
  
  void solution(int[] inputArr) {
    int ans=0;
    for(int i=1;i<inputArr.length-1;i++) {
      
       int leftMax = inputArr[i];
       for(int l=0;l<i;l++) {
         leftMax = Math.max(leftMax, inputArr[l]);
       }
       int rightMax = inputArr[i];
       for(int r=i+1; r<inputArr.length; r++) {
         rightMax = Math.max(rightMax, inputArr[r]);
       }
       
       ans+=Math.min(leftMax, rightMax)-inputArr[i];
      
    }
    
    System.out.println(ans);
    
  }
  
  void solution_optimized(int[] inputArr) {
    int l = inputArr.length;
    int left[] = new int[l];
    int right[] = new int[l];
    
    left[0] = inputArr[0];
    for(int i=1; i<l;i++) {
      left[i] = Math.max(left[i-1], inputArr[i]);
    }
    
    right[l-1] = inputArr[l-1];
    for(int i=l-2; i>=0;i--) {
      right[i] = Math.max(right[i+1], inputArr[i]);
    }
    
    int ans=0;
    for(int i =0 ;i<l;i++) {
      ans+=Math.min(left[i] ,right[i] ) - inputArr[i];
    }
    
    System.out.println(ans);
    
  }

}
