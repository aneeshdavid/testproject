package com.Flexton;

public class BinaryTreeLeftAndRightSame {

  public static void main(String[] args) {
    TreeNode root  = new TreeNode(5);
    root.left =  new TreeNode(3);
    root.right =  new TreeNode(3);
    
    root.left.left = new TreeNode(4);
    root.left.right = new TreeNode(4);
    
    root.right.left = new TreeNode(4);
    root.right.right = new TreeNode(4);
  
    new BinaryTreeLeftAndRightSame().solution(root);
  }

 static class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;
    
    public TreeNode(int val) {
      this.val =val;
    }
  }
  
  boolean solution( TreeNode root){
    if(root==null) {
     return true;
    }
    
            
    
    return false;
  }
}
