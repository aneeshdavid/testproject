package com.Flexton;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CheckPairofNumbersIsSum {

  public static void main(String[] args) {
   
    int[] result =  new CheckPairofNumbersIsSum().solution(new int[] {0, -1, 2, -3, 1}, -2);
    System.out.println(Arrays.toString(result));
  }
  
  int[] solution(int[] arr, int sum) {
    int[] result = new int[2];
    
    Set<Integer> set = new HashSet<>();
    for(int i=0;i<arr.length;i++) {
      Integer remaining =  sum - arr[i];
      if(set.contains(remaining)) {
        result[0] = remaining;
        result[1] = arr[i];
        return result;
      }else {
        set.add(arr[i]);
      }
      
    }

    return result;
  }

}
