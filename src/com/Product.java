package com;

/**
 * @author adavid
 *
 * Sep 27, 2018
 */

public class Product {
private String name;
private float price;
private Long id;

public Long getId() {
  return id;
}
public void setId(Long id) {
  this.id = id;
}
public String getName() {
  return name;
}
public void setName(String name) {
  this.name = name;
}
public float getPrice() {
  return price;
}
public void setPrice(float price) {
  this.price = price;
}
}

