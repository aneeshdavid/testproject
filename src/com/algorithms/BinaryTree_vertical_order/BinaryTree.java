package com.algorithms.BinaryTree_vertical_order;

/**
 * @author adavid
 *
 * Dec 2, 2018
 */

public class BinaryTree {
  class Values  
  { 
      int max, min; 
  } 
  private void findMinMax(Node node, Values min, Values max, int hd) {
    if(node == null)
      return;
    
    if(hd<min.min) {
      min.min = hd;
    }
    if(hd>max.max) {
      max.max = hd;
    }
    
    findMinMax(node.left,min,max,hd-1);
    findMinMax(node.right,min,max,hd+1);
  }
  private void verticalOrder(Node node) {
    Values val = new Values(); 
    findMinMax(node,val,val,0);
    System.out.println("Min :"+val.min+" Max :"+val.max);
    for(int line= val.min;line<=val.max;line++) {
      printVerticalLine(node,line,0);
      System.out.println(""); 
    }
  }
  private void printVerticalLine(Node node, int line, int hd) {
   if(node==null)
     return;
   if(hd==line) {
     System.out.println(node.data+" ");
   }
   printVerticalLine(node.left, line, hd-1);
   printVerticalLine(node.right, line, hd+1);
  }
  public static void main(String args[]) {
    BinaryTree tree = new BinaryTree(); 
    
    Node node = new Node(1); 
    node.left = new Node(2); 
    node.right = new Node(3); 
    node.left.left = new Node(4); 
    node.left.right = new Node(5); 
    node.right.left = new Node(6); 
    node.right.right = new Node(7); 
    node.right.left.right = new Node(8); 
    node.right.right.right = new Node(9); 

    System.out.println("vertical order traversal is :"); 
    tree.verticalOrder(node); 
  }

  
}

