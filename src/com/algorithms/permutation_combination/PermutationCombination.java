package com.algorithms.permutation_combination;

/**
 * @author adavid
 *
 * Nov 29, 2018
 */

public class PermutationCombination {
int count;
  private void permAndComb(String inputStr, int p) {
    int length =  inputStr.length();
    
    count++;
  if(p==inputStr.length()-1) {
    p =0;
  }
    String begin = inputStr.substring(0, p);
    String swap = swap(inputStr.substring(p,p+2));
    String end = inputStr.substring(p+2);
    
    String newWord = begin + swap + end;
    System.out.println(newWord);
    p++;
    if(count < length*(length-1)) {//n*(n-1)
      permAndComb(newWord,p);
    }
   
  }
  
  private String swap(String str) {
    return str.substring(str.length()-1)+str.substring(0,1);
  }

  
  public static void main(String[] args) {
   
    new PermutationCombination().permAndComb("HAI",0);
    
  }

}

