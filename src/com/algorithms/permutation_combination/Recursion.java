package com.algorithms.permutation_combination;

/**
 * @author adavid
 *
 * Nov 29, 2018
 */

public class Recursion {
int count =0;
  private int callMe(int n) {
    
    if(count<10) {
      count++;
      int sum = n+1;
     // System.out.println(sum);
      return callMe(sum);
    }
    
    return n;
   
  }
  public static void main(String args[]) {
    System.out.println(new Recursion().callMe(10));
  }
}

