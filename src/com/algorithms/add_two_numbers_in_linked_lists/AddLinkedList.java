package com.algorithms.add_two_numbers_in_linked_lists;

/**
 * @author adavid
 *
 * Dec 2, 2018
 */

public class AddLinkedList {
  static class Node<T>{
    private T data;
    Node next;
    
    public Node(T data) {
     this.data = data;
    }
  }
  
  public static void main(String arr[]) {
    Node<Integer> node1 = new Node<Integer>(5);
    node1.next =  new Node<Integer>(6);
    node1.next.next =  new Node<Integer>(3);
    System.out.println("List1");
    printList(node1);
    System.out.println("List2");
    Node<Integer> node2 = new Node<Integer>(8);
    node2.next =  new Node<Integer>(4);
    node2.next.next =  new Node<Integer>(2);
    node2.next.next.next =  new Node<Integer>(6);
    printList(node2);
    
    Node<Integer> result = addList(node1,node2); 
    System.out.println("Result List");
    printList(result);
  }

  private static Node<Integer> addList(Node<Integer> node1, Node<Integer> node2) {
   Node resultNode = null;
   int sum;
   int bal = 0;
   Node temp;
   Node prev = null;
   while(node1!=null || node2!=null) {
     sum = (node1!=null?node1.data:0) + (node2!=null?node2.data:0) + bal;
     bal = (sum>=10)?1:0;
     sum = sum %10;
     temp =  new Node(sum);
     if(resultNode==null) {
       resultNode = temp;
     }else {
       prev.next = temp;
     }
     prev = temp;
     if(node1!=null) {
       node1 = node1.next;
     }
     if(node2!=null) {
       node2 = node2.next;
     }
   }
    return resultNode;
  }

  private static void printList(Node<Integer> node) {
  while(node!=null) {
    System.out.println(node.data);
    node = node.next;
  }
    
  }
}

