package com.algorithms.findMostOccuranceOfWord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * @author adavid
 *
 * Oct 18, 2019
 */


/**
 * available features = ["battery","dual","sound","screen","waterproof"]
 * top features count =2
 * 
 * 
 */
public class MostOccurance {
  
  private static ArrayList<String> findTopFeatures( ArrayList<String> availableFeatures, int topFeatures, ArrayList<String> requestedFeatures) {
    ArrayList<String> resultList = new ArrayList<>();
    try {
      Map<String, Integer> occuranceMap = new TreeMap<>();
      
      requestedFeatures.forEach(reqFeature ->{
        availableFeatures.forEach(availFeature ->{
          if(reqFeature.contains(availFeature)) {
            if(occuranceMap.get(availFeature)!=null) {
              occuranceMap.put(availFeature, occuranceMap.get(availFeature)+1);
            }else {
              occuranceMap.put(availFeature, 1);
            }
          }
        });
      });
      
     
      Map<String, Integer> topfeatureMap =
          occuranceMap.entrySet().stream()
             .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
             .limit(topFeatures)
             .collect(Collectors.toMap(
                Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
      
      resultList = (ArrayList<String>) topfeatureMap.keySet();
      
    }catch(Exception e) {
      e.printStackTrace();
    }
    
    
  
  
    return resultList;
  }
  
  public static void main(String args[]) {
    ArrayList<String> availableFeatures = (ArrayList<String>) Arrays.asList(new String[] {"battery","dual","sound", "screen", "waterproof"});
   int topFeaturesCount = 2;
    ArrayList<String> requestedFeatures = new ArrayList<>();
    requestedFeatures.add("inner join user_authority ua on sound ua.person_id");
    requestedFeatures.add("n.t.d.l.l.SLF4JQueryLoggingListener waterproof Query Logging Listener");
    requestedFeatures.add("battery , p.asl_person_id as asl_person_id ");
    requestedFeatures.add("select 1 from student_advisor a where a.advisor_person_id");
    requestedFeatures.add("student_advisor a sound a.advisor_person_id");
    
    MostOccurance.findTopFeatures(availableFeatures, topFeaturesCount, requestedFeatures).forEach(feature->{
      System.out.println(feature);
    });
    
  }
  
  
  
}

