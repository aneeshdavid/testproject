package com.algorithms.population;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * @author adavid
 *
 *         Nov 8, 2018
 *         
 *        1. Iterate all persons
 *        2.add  population to the map by calculating " oldpopulation + birthCount - DeathCount"
 *        
 */

public class Population {
  private Person[] persons ;
  
  public static void main(String args[]) {
    Person[] persons = new Person[7];
    persons[0] = new Person(1818,1870);
    persons[1] = new Person(1820,1870);
    persons[2] = new Person(1920,1940);
    persons[3] = new Person(1920,1960);
    persons[4] = new Person(1925,1970);
    persons[5] = new Person(1945,1975);
    persons[6] = new Person(1995,2050);
    
    int year  = new Population().findPopulationByYear(persons);
    System.out.println("H.P.Year : "+year);
  }

  private int findPopulationByYear(Person[] persons) {
   this.persons = persons;
    Map<Integer, Integer> populationByYearMap = new HashMap<Integer, Integer>();
    for (Person p : persons) {
      if (populationByYearMap.get(p.getBirthYear()) == null) {
        populationByYearMap.put(p.getBirthYear(), 1);
      }
        int deathCount = getDiedOnYear(p.getBirthYear() + 1);
        int birthCount = populationByYearMap.get(p.getBirthYear());
        int oldPopulation = getOldPopulation(p.getBirthYear()) ;
        int totalPopulation = oldPopulation + birthCount - deathCount;
        populationByYearMap.put(p.getBirthYear(), totalPopulation);
      
    }
    return findHighestPopulatedYear(populationByYearMap);
  }

  private int getOldPopulation(int year) {
    int oldPopulation = 0;
    for (Person p : persons) {
      if (!p.isVisited() && p.getBirthYear() < year) {
        oldPopulation++;
        p.setVisited(true);
      }
    }
    return oldPopulation;
  }

  private int getDiedOnYear(int year) {
    int diedCount = 0;
    for (Person p : persons) {
      if (!p.isVisited() && p.getDeathYear() < year) {
        diedCount++;
      }
    }
    return diedCount;
  }
  private int findHighestPopulatedYear( Map<Integer, Integer> populationByYearMap) {
    int maxCount = 0;
    int maxYear = 0;
   Set<Entry<Integer, Integer>> yearSet = populationByYearMap.entrySet();
   for (Entry<Integer, Integer> entry : yearSet) {
    if(entry.getValue() > maxCount) {
      maxCount = entry.getValue();
      maxYear = entry.getKey();
    }
  }
   System.out.println("maxYear : "+maxYear +" maxCount: "+ maxCount);
    return maxYear;
  }
}
