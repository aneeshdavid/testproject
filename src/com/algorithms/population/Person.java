package com.algorithms.population;

/**
 * @author adavid
 *
 * Nov 8, 2018
 */

public class Person {

  private Integer birthYear;
  private Integer deathYear;
  private boolean isVisited;
  
  public Person(Integer birthYear,Integer deathYear) {
   this.birthYear = birthYear;
   this.deathYear = deathYear;
  }

  public Integer getBirthYear() {
    return birthYear;
  }

  public void setBirthYear(Integer birthYear) {
    this.birthYear = birthYear;
  }

  public Integer getDeathYear() {
    return deathYear;
  }

  public void setDeathYear(Integer deathYear) {
    this.deathYear = deathYear;
  }

  public boolean isVisited() {
    return isVisited;
  }

  public void setVisited(boolean isVisited) {
    this.isVisited = isVisited;
  }
  
}

