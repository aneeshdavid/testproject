package com.algorithms.active_inactive_cells;

import java.util.Arrays;

/**
 * @author adavid
 *
 * Dec 20, 2018
 */

public class ActiveInactiveCells {

  private static void doCompete(int [] arr, int k) {
    int[] newArr = new int[arr.length];
    while(k>0) {
      for(int i=0;i<arr.length;i++) {
        int left = 0,right=0;
        if(i!=0) {
          left = arr[i-1];
        }
        if(i<arr.length-1) {
          right = arr[i+1];
        }
        
        if(left==right) {
          newArr[i] = 0;
        }else {
          newArr[i] = 1;
        }
        
      }
      k--;
    }
    System.out.println(Arrays.toString(newArr));
  }
  public static void main (String a[]) {
    int[] inputArr = {0, 1, 0, 1, 0, 1, 0, 1};
    int noOfDays = 2;
    doCompete(inputArr,noOfDays);
  }
}

