package com.algorithms.spiral_array;

/**
 * @author adavid
 *
 * Nov 26, 2018
 */
public class SpiralArray {

  static int arraySize = 3;
  public static void main(String args[]) {
    int[][] normalArray = insertIntoArray() ;
    printArray(normalArray);
    
    System.out.println("Spiral Array \n");
    int[][] spiralArray = insertIntoArray_spiral() ;
     printArray(spiralArray);
  }
  private static int[][]  insertIntoArray() {
    int[][] array = new int[arraySize][arraySize];
    int n = 1;
    for(int i =0; i<arraySize ;i++) {
      for(int j =0;j<arraySize; j++) {
        array[i][j] = n;
        n++;
      }
    }
    return array;
  }
  private static int[][]  insertIntoArray_spiral() {
    int[][] array = new int[arraySize][arraySize];
    int value =1;
    int minRow=0;
    int maxRow=arraySize-1;
    int minCol=0;
    int maxCol=arraySize-1;
    
    
  while(value <= arraySize*arraySize) {
    for(int j = minRow;j<=maxCol;j++) {
      array[minRow][j] = value;
      value++;
    }
    minRow++;
    
    for(int i = minRow;i<=maxRow;i++) {
      array[i][maxCol] = value;
      value++;
    }
    maxCol--;
    
    for(int j=maxCol;j >= minCol ;j--) {
      array[maxRow][j] = value;
      value++;
    }
    maxRow--;
    
    for(int i=maxRow;i >= minRow;i--) {
      array[i][minCol] = value;
      value++;
    }
    minCol++;
  }
      
      
    return array;
  }
  private static void printArray(int[][] array) {
    String arrayString = "";
    for(int i =0; i<arraySize;i++) {
      for(int j =0;j<arraySize; j++) {
        arrayString+=array[i][j]+"\t";
      }
      arrayString+="\n";
    }
    System.out.println(arrayString);
  }
  
}

