package com.algorithms.MergeIntervals;

import java.util.ArrayList;
import java.util.List;

/**
 * @author adavid
 *
 * Dec 3, 2018
 */

public class MergeIntervals {
  
  static class Interval{
    int start;
    int end;
    public Interval(int start, int end) {
     this.start = start;
     this.end = end;
    }
  }
  
public static List<Interval> merge(List<Interval> intervals){
  List<Interval> mergedIntervals = new ArrayList<Interval>();
  int start = intervals.get(0).start;
  int end = intervals.get(0).end;
  
  for(int i =1;i<intervals.size();i++) {
    Interval currentInterval = intervals.get(i);
    if(currentInterval.start <= end) {
      end = Math.max(end, currentInterval.end);
    }else {
      mergedIntervals.add(new Interval(start,end));
      start = currentInterval.start;
      end = currentInterval.end;
    }
  }
  mergedIntervals.add(new Interval(start,end));
  return mergedIntervals;
}
  
private static void printIntervals(List<Interval> intervals) {
  String intervalsStr ="";
  for (Interval interval : intervals) {
    intervalsStr+="{"+interval.start+","+interval.end+"}";
  }
  System.out.println(intervalsStr);
}
  
  public static void main(String[] args) {
    List<Interval> intervals = new ArrayList<Interval>();
    intervals.add(new Interval(1,3));
    intervals.add(new Interval(2,6));
    intervals.add(new Interval(8,10));
    intervals.add(new Interval(15,18));
    intervals.add(new Interval(17,20));
    printIntervals(intervals);
    
    intervals = merge(intervals);
    printIntervals(intervals);
  }

}

