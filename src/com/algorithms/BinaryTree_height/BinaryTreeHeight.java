package com.algorithms.BinaryTree_height;

/**
 * @author adavid
 *
 * Feb 16, 2019
 */

public class BinaryTreeHeight<T> {
  Node<T> root;
  static class Node<T> {
    T data;
    Node<T> left;
    Node<T> right;
    public Node(T data) {
     this.data  = data;
     
    }
    }

  int findHeight(Node<T> aNode) {
    if (aNode == null) {
        return -1;
    }

    int lefth = findHeight(aNode.left);
    int righth = findHeight(aNode.right);

    if (lefth > righth) {
        return lefth + 1;
    } else {
        return righth + 1;
    }
}
  
  public static void main(String[] args) {
    BinaryTreeHeight<Integer> tree = new BinaryTreeHeight<Integer>(); 
    
    // Let us create a binary tree shown in above diagram 
    tree.root = new Node<Integer>(1); 
    tree.root.left = new Node<Integer>(2); 
    tree.root.right = new Node<Integer>(3); 
    tree.root.left.left = new Node<Integer>(4); 
    tree.root.left.right = new Node<Integer>(5); 
    
    System.out.println("Height of tree is " + tree.findHeight(tree.root)); 


  }

}

