package com.algorithms.FindSumOfCube;

import java.util.Scanner;

/**
 * @author adavid
 *
 * Dec 4, 2018
 */

public class FindSumOfCube {

  private static Scanner scn;

  public static void main(String[] args) {
   scn = new Scanner(System.in);
   int sum = 0;
  
     //if(scn.nextLine().equals(""))break;
     int num = Integer.parseInt(scn.next());
     sum+=Math.pow(num, 3);
     
     num = Integer.parseInt(scn.next());
     sum+=Math.pow(num, 3);
     
     num = Integer.parseInt(scn.next());
     sum+=Math.pow(num, 3);
     scn.close();
   System.out.println("Result :" + sum);
  }

}

