package com.algorithms.shift_elements;

import java.util.Arrays;

/**
 * @author adavid
 *
 * Dec 4, 2018
 */

public class ShiftArrayElements {

  public static void main(String[] args) {
  int[] inputArr = {1,2,3,4,5,6};
  int pos = 2;
  int[] outputArr = shiftRight(inputArr, pos);
  System.out.println("ShiftRigth :"+ Arrays.toString(outputArr));
  outputArr = shiftRight(outputArr, pos);
  System.out.println("ShiftRigth :"+ Arrays.toString(outputArr));
  }

  
  
  private static int[] shiftRight(int[] inputArr, int pos) {
    int len = inputArr.length;
    int mainTemp = inputArr[len-1];
    int prev= inputArr[0];
    for(int i=0;i<len;i++) {
      int temp =   inputArr[i+1%len-1] ;
      inputArr[i+1%len-1] = prev;
      prev = temp;
      
    }
    inputArr[1%len-1] = mainTemp;
    return inputArr;
  }

}

