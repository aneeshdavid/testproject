package com.algorithms;

import java.util.ArrayList;
import java.util.List;

public class RedBlueTapes {
	
	private boolean redTapeIsVisible(int[][] blueTapes, int[] redTape) {
		if(blueTapes.length==0)
			return true;
		int prevStart=blueTapes[0][0];
		int prevEnd=blueTapes[0][1];
		List<Integer[]> tapeList = new ArrayList<>();
		for (int i=1; i<blueTapes.length;i++) {
			int currentStart = blueTapes[i][0];
			int currentEnd = blueTapes[i][1];
			if(currentStart<=prevEnd || currentStart<prevStart) {
				if(currentStart<=prevEnd ) {
					prevEnd = currentEnd;
				}
				if(currentStart<prevStart) {
					prevStart = currentStart;
				}
			}
			else {
				tapeList.add(new Integer[] {currentStart,currentEnd});
			}
		}
		
		tapeList.add(new Integer[] {prevStart,prevEnd});
		
		for (Integer[] integers : tapeList) {
			if(integers[0]<=redTape[0] && integers[1]>=redTape[1]) {
				return false;
			}
		}
		
		return true;
	}
	public static void main(String[] args) {
		
		int[][] blueTape = new int[][]{{3,4},{4,5},{5,6}};
		int[] redTape = new int[] {3,6};
		boolean isRedVisible = new RedBlueTapes().redTapeIsVisible(blueTape, redTape);
		System.out.println(isRedVisible);
		
		
	}

}
