package com.algorithms;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LRUcache<K,V> {
	
	int size;
	int count;
	
	LinkedList<Node> list = new LinkedList<>();
	Map<K, Node> cache = new HashMap<>();
	
	public LRUcache(int size) {
		this.size = size;
	}
	
	private void put(K k, V v) {
		if(count>=size) {
			cache.remove(list.getLast().key);
			list.removeLast();
			count--;
		}
		Node node  = cache.get(k);
		if(node==null) {
			node = new Node(k,v);
			list.addFirst(node);
			cache.put(k, node);
			count++;
		}else {
			node.value = v;
			list.remove(node);
			list.addFirst(node);
		}
	}
	
	private V get(K key) {
		Node node  = cache.get(key);
		if(node == null)
			return null;
		//remove the node from the list 
		list.remove(node);
		// add to front;
		list.addFirst(node);
		return node.value;
	}

	class Node{
		K key;
		V value;
		public Node(K k, V v) {
			this.key = k;
			this.value=v;
		}
	}
	
	
	public static void main(String[] args) {
	
		LRUcache<Integer, Integer> lruCache = new LRUcache<>(3);
		lruCache.put(1,10);
		lruCache.put(2,20);
		lruCache.put(3,30);
		lruCache.put(4,40);

	}

}
