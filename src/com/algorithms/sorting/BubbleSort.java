package com.algorithms.sorting;

import java.util.Arrays;

/**
 * @author adavid
 *
 * Dec 2, 2018
 */

public class BubbleSort {
  
 public  int[] sort(int[] array) {
    return doSort(array); 
  }

  private int[] doSort(int[] array) {
    boolean swapFlag = false;
    for(int i=0;i<array.length-1;i++) {
      for(int j=i+1;j<=array.length-1;j++) {
        if(array[i]>array[j]) {
          swapFlag = true;
          int temp = array[i];
          array[i] = array[j];
          array[j] = temp;
        }
      }
      if(!swapFlag) {
        break;
      }
    }
    
  return array;
  
}

  public static void main(String[] args) {
   int[] input_arr = {4,5,3,1,8,2,7,8,0,2,3,0,-1,-2,-3}; 
   BubbleSort bubbleSort = new BubbleSort();
   int[] resultArr =  bubbleSort.sort(input_arr);
   System.out.println("Result : "+Arrays.toString(resultArr));
  }

}

