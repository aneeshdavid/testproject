package com.algorithms.balanced_parentheses;

import java.util.Stack;

/**
 * @author adavid
 *
 * Nov 27, 2018
 */

public class BalancedParentheses {

  public static void main(String[] args) {
  String input = "((()";
   boolean result =   isBalanced(input);
   System.out.println("Result : "+ result);

  }

  private static boolean isBalanced(String inputStr) {
    char[] chrArray = inputStr.toCharArray();
    Stack<String> balStack = new Stack<String>();
    for (char ch : chrArray) {
      if(ch=='(') {
        balStack.push(ch+"");
      }
      else if(ch==')') {
        if(balStack.isEmpty()) {
          balStack.push(ch+"");
        }else {
        balStack.pop();
        }
      }
    }
    if(balStack.isEmpty()) {
      return true;
    }
    return false;
  }
}

