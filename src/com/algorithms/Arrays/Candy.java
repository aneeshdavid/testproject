package com.algorithms.Arrays;

import java.util.HashMap;
import java.util.Map;

public class Candy {

  public static void main(String[] args) {
    
    int[] ratings = new int[] {1,2,2};
    int totalCandy = new Candy().candy(ratings);
    System.out.println(totalCandy);
    System.out.println("Completed");
  }
  
  
  
  public int candy(int[] ratings) {
    int totalCandy = 0 ;
    Map<Integer, Children>  candyMap= new HashMap<>();
    for(int i =0;i<ratings.length;i++) {
      totalCandy++;
      candyMap.put(i, new Children(ratings[i], 1));
      Children children = candyMap.get(i);
        int pos = -1;
       if(i==0) {
         pos = i+1;
         if(candyMap.get(pos)!=null && candyMap.get(pos).rank<children.rank) {
           children.candy= candyMap.get(pos).candy+1;
           totalCandy++;
         }
       }else if(i == ratings.length-1) {
         pos = i-1;
         if(candyMap.get(pos)!=null && candyMap.get(pos).rank<children.rank) {
           children.candy= candyMap.get(pos).candy+1;
           totalCandy++;
         }
       }else {
         pos = ratings[i-1] < ratings[i+1] ? i-1 : i+1;
         if(candyMap.get(pos)!=null && candyMap.get(pos).rank<children.rank) {
           children.candy= candyMap.get(pos).candy+1;
           totalCandy++;
         }
       }
       
    }
   
    for(int i = ratings.length-1;i>=0; i--) {
      Children children = candyMap.get(i);
      int pos = -1;
     if(i==0) {
       pos = i+1;
       if(candyMap.get(pos)!=null && candyMap.get(pos).rank<children.rank) {
         children.candy= candyMap.get(pos).candy+1;
         totalCandy++;
       }
     }else if(i == ratings.length-1) {
       pos = i-1;
       if(candyMap.get(pos)!=null && candyMap.get(pos).rank<children.rank) {
         children.candy= candyMap.get(pos).candy+1;
         totalCandy++;
       }
     }else {
       pos = ratings[i-1] < ratings[i+1] ? i-1 : i+1;
       if(candyMap.get(pos)!=null && candyMap.get(pos).rank>children.rank) {
         children.candy= candyMap.get(pos).candy+1;
         totalCandy++;
       }
     }
    }
    
    return totalCandy;
    
  }
  
  class Children{
    int rank;
    int candy;
    public Children(int rank, int candy){
      this.rank = rank;
      this.candy =candy;
    }
  }

}
