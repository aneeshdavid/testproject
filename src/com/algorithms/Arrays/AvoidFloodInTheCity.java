package com.algorithms.Arrays;

import java.util.Arrays;

public class AvoidFloodInTheCity {

  public static void main(String[] args) {
    int[] rains = new int[] {1,2,3,4};
    int[] resultArr = new AvoidFloodInTheCity().avoidFlood(rains);
    System.out.println(Arrays.toString(resultArr));
    System.out.println("Completed");
  }
  
  
  
  public  int[] avoidFlood(int[] rains) {
    int[] resultArr = new int[rains.length];
    for(int i=0;i<resultArr.length;i++) {
      if(rains[i]>0) {
        if(isExistAlready(rains, i)) {
          if(isDried(resultArr, rains[i], i)) {
            resultArr[i] = -1;
          }else {
            return new int[] {};
          }
        }else {
          resultArr[i] = -1;
        }
        
      }else {
        int nextDry = findNextDry(resultArr,rains,i);
        resultArr[i] = nextDry;
        
      }
    }
    return resultArr;
  }



  private int findNextDry(int[] resultArr, int[] rains, int pos) { 
    for(int i = pos+1 ; i<rains.length; i++) {
      if(rains[i]!=0) {
        for(int j=0;j<pos-1;j++) {
          if(rains[j]!=0 && rains[j] == rains[i]) {
            if(!isFound(resultArr,rains[i])) {
              return rains[i];
            }
          }
        }
      }
    }
    
    
    
    for(int j =pos+1; j<rains.length;j++) {
      if(rains[j]!=0) {
        return rains[j];
      }
    }
    
    return 1;
  }



  private boolean isFound(int[] resultArr, int lake) {
    for(int i=1;i<=resultArr.length;i++) {
      if(resultArr[i-1]==lake) {
        return true;
      }
    }
     return false;
  }



  private boolean isDried(int[] resultArr, int lake, int currentPos) {
   for(int i=1;i<=currentPos;i++) {
     if(resultArr[i-1]==lake) {
       return true;
     }
   }
    return false;
  }



  private boolean isExistAlready(int[] rains, int currentPos) {
    for(int i=0 ;i<currentPos; i++) {
      if(rains[i]==rains[currentPos]) {
        return true;
      }
    }
    return false;
  }

}
