package com.algorithms.Arrays;

import java.util.ArrayList;
import java.util.List;


/*
Given an array of integers, return an list of integers which contains the
[1st integer, Sum of next 2 integers (2nd, 3rd), 
Sum of next 3 integers (4th, 5th, 6th)…] and so on

Input 
[1,6,8,5,9,4,7,2]
Output 
[1,14,18,9]
 */

public class SumOfElements {

  public static void main(String[] args) {
    System.out.println(summation(new int[]{1,6,8,5,9,4,7,2}));    
  }
  
  private static List<Integer> summation(int[] arr){
    List<Integer> result = new ArrayList<>();
    int count = 1;
    for(int i = 0; i<arr.length;){
      int sum = 0;
      
     // i--;
      int j =i;
      while(i<j+count && i< arr.length) {
        sum+= arr[i];
       i++;
      }
      count++;
      result.add(sum);
      
    }
    
    return result;
  }

}
