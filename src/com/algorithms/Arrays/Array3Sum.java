package com.algorithms.Arrays;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class Array3Sum {

  public static void main(String[] args) {
   int[] inputArr = new int[] {-1, 0, 1, 2, -1, -4};
   Set<int[]> outputArr = threeSum(inputArr);
   for (int[] is : outputArr) {
    System.out.println(Arrays.toString(is));
   }
   System.out.println("Done");
  }

  private static Set<int[]> threeSum(int[] inputArr) {
    Set<int[]> resultSet = new LinkedHashSet<>();
    
    for(int i=1;i<=inputArr.length-2;i++) {
      
      int j=i;
      while(j<=inputArr.length-2) {
        int sum = inputArr[i-1] + inputArr[j] + inputArr[j+1];
        if(sum==0) {
          resultSet.add(new int[] {inputArr[i-1] , inputArr[j] , inputArr[j+1]});
        }
        j++;
      }
    }
    
    return resultSet;
  }

}
