package com.algorithms.Arrays;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


class Employee1 {

  private int id;
  private String name;
  private List<String> skills;

  public int getId() {
      return id;
  }

  public void setId(int id) {
      this.id = id;
  }

  public String getName() {
      return name;
  }

  public void setName(String name) {
      this.name = name;
  }

  public List<String> getSkills() {
      return skills;
  }

  public void setSkills(List<String> skills) {
      this.skills = skills;
  }

  public Employee1(int id, String name, List<String> skills) {
      super();
      this.id = id;
      this.name = name;
      this.skills = skills;
  }

  public Employee1() {
      // TODO Auto-generated constructor stub
  }

  @Override
  public String toString() {
      // TODO Auto-generated method stub
      return name + " : {" + skills + "} ";
  }

}

public class FlexTest {

  public static void main(String[] args) {

      List<Employee1> employees = Arrays.asList(
              new Employee1(101, "Hemendra Singh Chouhan", Arrays.asList("Java", "Spring Boot", "Hibernate")),
              new Employee1(102, "Rebeca", Arrays.asList("Java", "Marketing")),
              new Employee1(103, "David",
                      Arrays.asList("Payroll Management", "Human Resource", "Leaves", "Time Sheet")),
              new Employee1(104, "Robin", Arrays.asList("Medicine", "Surgery")));
      
      
      
      employees.sort(new Comparator<Employee1>() {

        @Override
        public int compare(Employee1 o1, Employee1 o2) {
          int result = 0;
          if(o1.getSkills().size() > o2.getSkills().size()) {
            result = -1;
          }else {
            result = 1;
          }
          
          
          return result;
        }
      });  
      
      System.out.println(employees);
      
  }

}


  

