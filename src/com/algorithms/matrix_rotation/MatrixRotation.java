package com.algorithms.matrix_rotation;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author adavid
 *
 * Aug 10, 2019
 */

public class MatrixRotation {

  static int matrix_array[][] = { {1, 2, 3}, 
                      {4, 5, 6}, 
                       {7, 8,9,} 
                      }; 
  
  public static void main(String[] a) {
    printMatrix();
    rotateMatrix_clockwise();
   // printMatrix();
  }

  private static void rotateMatrix_clockwise() {
    Collections.reverse(Arrays.asList(matrix_array));
    System.out.println("Reversed");
    printMatrix();
    for (int i = 0; i < matrix_array.length; i++) {
     for (int j = 0; j < i; j++) {
       int temp = matrix_array[i][j] ;
       matrix_array[i][j]  =  matrix_array[j][i];
       matrix_array[j][i]=temp;
     }
   }
    System.out.println("Rotated");
    printMatrix();
  }

  private static void printMatrix() {
   for (int i = 0; i < matrix_array.length; i++) {
     String str="";
    for (int j = 0; j < matrix_array.length; j++) {
      str+=matrix_array[i][j];
    }
    System.out.println(str);
  }
    
  }
  
}

