package com.algorithms.additionAndmultiplication_stack;

import java.util.Stack;

/**
 * @author adavid
 *
 * Dec 4, 2018
 */

public class AdditionAndMultiplication {

  public static void main(String[] args) {
    String input = " [8, 3, 2, [5, 6, [9]], 6] ";
    int result = calculate(input);
    System.out.println(result);
       


  }

  private static int calculate(String input) {
   String strInput =  input.replace(",", ""). replaceAll("\\s","");
    Stack stack = new Stack();
    int result =1;
    for (int i=0; i<strInput.length(); i++) {
      char c =strInput.charAt(i);
      if(c!=']') {
        stack.push(c+"");
      }else {
      String popKey = "";
      int innerSum = 0;
        while(!popKey.equals("[")) {
          popKey =  stack.pop().toString();
          if(popKey.equals("["))continue;
          innerSum += Integer.parseInt(popKey);
        }
        result *=innerSum;
       
      }
    }
    return result;
  }

}

