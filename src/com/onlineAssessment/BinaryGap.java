package com.onlineAssessment;

public class BinaryGap {

	
	public int solution(int N) {
        String str = Integer.toBinaryString(N);
        char[] charArr = str.toCharArray();
        int i=0;
        int ans=0;
        for(int j=0;j<charArr.length;j++){
            if(charArr[j]=='1'){
                i = j;
               
            }
            if(ans!=0)
            ans = Math.max(ans,(j-i));
        }
        
        return ans;
        
    }
	
	public static void main(String[] args) {
		int n = new BinaryGap().solution(32);
		System.out.println(n);
	}
}
