package com.onlineAssessment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * 
 * @author adavid
 
 AWS wants to increase the reliability of their network by upgrading crucial data center routers. Each data center has a single router that is connected to every other data center through a direct link or some other data center.
To increase the fault tolerance of the network, AWS wants to identify routers which would result in a disconnected network if they went down and replace then with upgraded versions.
Write an algorithm to identify all such routers.

Input:
The input to the function/method consists of three arguments:
numRouters, an integer representing the number of routers in the data center.
numLinks, an integer representing the number of links between the pair of routers.
Links, the list of pair of integers - A, B representing a link between the routers A and B. The network will be connected.

Output:
Return a list of integers representing the routers with need to be connected to the network all the time.

Example :
Input:
numRouters = 7
numLinks = 7
Links = [[0,1], [0, 2], [1, 3], [2, 3], [2, 5], [5, 6], [3,4]]

Output:
[2, 3, 5]

Explanation:
On disconnecting router 2, a packet may be routed either to the routers- 0, 1, 3, 4 or the routers - 5, 6, but not to all.
On disconnecting router 3, a packet may be routed either to the routers - 0,1,2,5,6 or to the router - 4, but not to all.
On disconnecting router 5, a packet may be routed either to the routers - 0,1,2,3,4 or to the router - 6, but not to all.
 */
public class CriticalRouters {
	static Node rootNode;
	static class Node{
		int value;
		List<Node> next = new ArrayList<>();
		public Node(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
		public void setValue(int route) {
			this.value = route;
		}
		public List<Node> getNext() {
			return next;
		}
		public void setNext(List<Node> next) {
			this.next = next;
		}
		
		public Node getNode(int route, Node root) {
			Node absoluteNode = null;
			if(root.getValue() != route) {
				for(Node node : root.getNext()) {
					return getNode(route, node);
				}
			}else {
				 absoluteNode = root ;
			}
			
			return absoluteNode;
		}
		
	}
	 public static void main(String args[]) {
		 int links[][] = new int[][] {
			 {0, 1},{0, 2},{1, 3},{2, 3},{2, 5},{5, 6},{3, 4}
		 };
		 int numRouters = 7;
		 
		 findCriticalLinks(links, numRouters);
		 
	 }

	private static void findCriticalLinks(int[][] links, int numRouters) {
		Node headNode = null;
		for(int[] route : links) {
			
			if(headNode!=null) {
				headNode = findNode(rootNode,route[0]);
			}
			if(headNode == null) {
				headNode = new Node(route[0]);
			}
			headNode.next.add(new Node(route[1]));
			if(rootNode==null) {
				rootNode = headNode;
			}
		}
		
	System.out.println("Completed");	
		
	}
	
	
	public static Node findNode(Node root, int route) {
		if(root.getValue() == route) {
			return root;
		}else {
			for(Node node : root.getNext()) {
				return findNode(node, route);
			}
		}
		
		return root;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*
	static class Link{
		private int a;
		private int b;
		public Link() {
			// TODO Auto-generated constructor stub
		}
		public Link(int a , int b) {
			this.a = a;
			this.b = b;
		}
		public int getA() {
			return a;
		}
		public void setA(int a) {
			this.a = a;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
		
	}
 public static void main(String args[]) {
	 //int links[][] = new int[][] {
	//	 {0, 1},{0, 2},{1, 3},{2, 3},{2, 5},{5, 6},{3, 4}
	// };
	 ArrayList<Link> links= new ArrayList<>();
	 links.add(new Link(0,1));
	 links.add(new Link(0,2));
	 links.add(new Link(1,3));
	 links.add(new Link(2,3));
	 links.add(new Link(2,5));
	 links.add(new Link(5,6));
	 links.add(new Link(3,4));	 
	 
	 int numRouters = 7;
	 ArrayList<Integer> criticalRouters = findCriticalLinks(links, numRouters);
	 for (Integer router : criticalRouters) {
		System.out.println(router);
	}
 }

private static ArrayList<Integer> findCriticalLinks(List<Link> links, int numRouters) {	
	
		Map<Integer, ArrayList<Integer>> map= new LinkedHashMap<>();
		for (Link link : links) {
			if(map.get(link.getA())==null) {
				ArrayList<Integer> lis = new ArrayList<>();
				lis.add(link.getB());
				map.put(link.getA(),lis);
			}else {
				map.get(link.getA()).add(link.getB());
			}
		}
		
	ArrayList<Integer> criticalRouters= new ArrayList<>(); 
		
	for(int i=0;i<numRouters;i++) {
		Set<Integer> set = new HashSet<>();
		for(int j=0;j<numRouters;j++) {
			if(map.get(j)!= null && j!=i) {
				Integer currentRouter = null;
				for(Integer router :map.get(j)) {
					currentRouter = router;
					if(router!=i) {
					//check the current router has any connections 
					//other than the disconnected one
					 if(hasAnyConnectionsOtherThanDisconnected(map,router, i)) {
						 set.add(router);
					 }
					}
				}
				if(maphastheRouterAsValue(map,j, i)) {
					 set.add(j);
				}
			}
		}
		System.out.println("");
		if(isCritical(set, i, numRouters)) {
			criticalRouters.add(i);
		}
	}
	return criticalRouters;
}



private static boolean maphastheRouterAsValue(Map<Integer, ArrayList<Integer>> map, int router, int disconnectedRouter) {
	for(Entry<Integer,ArrayList<Integer>> entry : map.entrySet()) {
		if(entry.getKey()!=disconnectedRouter && entry.getValue().contains(router)) {
			return true;
		}
	}
	return false;
}

private static boolean isCritical(Set<Integer> set, int currentRouter, int numRouters) {
	if(set == null || set.size()==0) {
		return false;
	}
	for(int i=0;i<numRouters;i++) {
		if(i!=currentRouter && !set.contains(i)) {
			return true;
		}
	}
	return false;
}

private static boolean hasAnyConnectionsOtherThanDisconnected(Map<Integer, ArrayList<Integer>> map, Integer currentRouter,
		int disconnectedRouter) {
	for(Entry<Integer,ArrayList<Integer>> entry : map.entrySet()) {
		if(entry.getKey()!=disconnectedRouter && entry.getValue().contains(currentRouter)) {
			return true;
		}
	}
	return false;
}*/

}
