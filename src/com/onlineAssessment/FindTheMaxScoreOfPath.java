package com.onlineAssessment;

import java.util.ArrayList;
import java.util.List;

/**
 * @author adavid
 *
 * Nov 13, 2019
 */

/**
 Given a two 2D integer array, find the max score of a path from the upper left cell to bottom right cell that doesn't visit any of the cells twice. The score of a path is the minimum value in that path.
For example:
Input:
[7,5,3]
[2,0,9]
[4,5,9]
Here are some paths from [0,0] to [2,2] and the minimum value on each path:
path: 7->2->4->5->9, minimum value on this path: 2
//{0,0}{1,0}{2,0}{2,1}{2,2}
path: 7->2->0->9->9, minimum value on this path: 0
//{0,0}{1,0}{1,1}{1,2}{2,2}
path: 7->2->0->5->9, minimum value on this path: 0
//{0,0}{1,0}{1,1}{2,1}{2,2}
Notice: the path can move all four directions. 
Here 7->2->0->5->3->9->9 is also a path, and the minimum value on this path is 0.
//{0,0}{1,0}{1,1}{0,1}{0,2}{1,2}{2,2} 
The path doesn't visit the same cell twice. So 7->2->0->5->3->9->0->5->9 is not a path. 
In the end the max score(the min value) of all the paths is 3. 
Output: 3
*/
public class FindTheMaxScoreOfPath {

  public static void main(String[] args) {
   int matrix[][] = new int[][] {{7,5,3},{2,0,9},{4,5,9}};
	//int matrix[][] = new int[][] {{1,3,5,8},{4,2,1,7},{4,3,2,3}};
   printArray(matrix);
   //getPossibleMoves(3);
   int max = maximumMinimumPath(matrix);
   System.out.println(max);
  }
  
  
  
  private static List<String> getPossibleMoves(int length) {
	  System.out.println("Possible Paths:"); 
	  List<String> moves = new ArrayList<>();
	  for(int r=0; r<length;r++) {
	      for(int c=0; c<length;c++) {
	    	  String move = "{"+r+","+c+"}";
	    	  moves.add(move);
	    	  System.out.println(move);
	      }
	    }

	  return moves;
  }
  
  private static void printArray(int[][] matrix) {
	  System.out.println("Matrix:"); 
    for(int r=0; r<3;r++) {
      String row ="";
      for(int c=0; c<3;c++) {
        row += matrix[r][c]+" ";
      }
      System.out.println(row);
    }
  }
  
  
  /*public static int maximumMinimumPath(int[][] matrix) {

      if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {

          return 0;

      }

      int n = matrix.length;

      int m = matrix[0].length;

      int[][] dp = new int[n][m];

      // dp[i,j]The minimum value in the path (but the difference is different)

      dp[0][0] = matrix[0][0];// [0,0] Certainly in the point of consideration

      for (int i = 1; i < n; i++) {

          dp[i][0] = Math.min(dp[i - 1][0], matrix[i][0]); 

      }

      for (int i = 1; i < m; i++) {

          dp[0][i] = Math.min(dp[0][i - 1], matrix[0][i]);

      }

      for (int i = 1; i < n; i++) {

          for (int j = 1; j < m; j++) {

              dp[i][j] = Math.min(Math.max(dp[i - 1][j], dp[i][j - 1]), matrix[i][j]);

          }

      }

      return dp[n - 1][m - 1];

  }*/
  
  public static int maximumMinimumPath(int[][] matrix) {
	   if(matrix == null ||matrix.length == 0 || matrix[0].length==0) {
		   return 0;
	   }
	   int r_len = matrix.length;
	   int c_len = matrix[0].length;
	   
	   int newArr[][] = new int[r_len][c_len];
	   newArr[0][0] = matrix[0][0];//start with the left corner element that is 0,0
	   
	   for(int c=1;c<c_len;c++) {
		   newArr[0][c] = Math.min(newArr[0][c-1], matrix[0][c]);
	   }
	   
	   for(int r=1;r<r_len;r++) {
		   newArr[r][0]= Math.min(newArr[r-1][0], matrix[r][0]);
	   }
	   
	   
	   for(int r=1;r<r_len;r++) {
		   for(int c=1;c<c_len;c++) {
			   newArr[r][c]=Math.min(Math.max(newArr[r][c-1], newArr[r-1][c]),matrix[r][c]);
		   }
	   }
	   
	   
	   
	   return newArr[r_len-1][c_len-1];
  }
  
  
}

