package com.onlineAssessment;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author adavid

 Given a 2D grid, each cell is either a zombie or a human. Zombies can turn adjacent (up/down/left/right) human beings into zombies every day. Find out how many days does it take to infect all humans?
Input:
matrix, a 2D integer array where a[i][j] = 1 represents a zombie on the cell and a[i][j] = 0 represents a human on the cell.

Output:
Return an integer represent how many days does it take to infect all humans.
Return -1 if no zombie exists.

Example :
Input:
[[0, 1, 1, 0, 1],
[0, 1, 0, 1, 0],
[0, 0, 0, 0, 1],
[0, 1, 0, 0, 0]]

Output:
2

Explanation:
At the end of the day 1, the status of the grid:
[[1, 1, 1, 1, 1],
[1, 1, 1, 1, 1],
[0, 1, 0, 1, 1],
[1, 1, 1, 0, 1]]

At the end of the day 2, the status of the grid:
[[1, 1, 1, 1, 1],
[1, 1, 1, 1, 1],
[1, 1, 1, 1, 1],
[1, 1, 1, 1, 1]]
 */
public class ZombieInfection {

	public static void main(String[] args) {
		int [][] matrixArr = new int[][] {
			{0, 1, 1, 0, 1},
			{0, 1, 0, 1, 0},
			{0, 0, 0, 0, 1},
			{0, 1, 0, 0, 0}
		};

		int day = countDaysConvertToZombie(matrixArr);
		System.out.println(day);
	}
	
	private static int countDaysConvertToZombie(int[][] matrixArr) {
		int day = 0;
		while(hasHuman(matrixArr)) {
			day++;
			convertToZombie(matrixArr);
		}
		return day;
	}

	private static int[][] convertToZombie(int[][] matrixArr) {
		
		int row_len = matrixArr.length;
		int col_len = matrixArr[0].length;
		
		for(int r = 0; r < row_len; r++) {
			for(int c = 0; c < col_len; c++) {   //right    				//left
				if(matrixArr[r][c] == 0 && ((c < col_len-1 && matrixArr[r][c+1] == 1) || (c>0 && matrixArr[r][c-1] == 1) || 
											(r < row_len-1 &&  matrixArr[r+1][c] == 1)|| (c>0 && matrixArr[r-1][c] == 1))) {
												//bottom 					//top 
					matrixArr[r][c] = 1;
				}
			}
		}
		
		return matrixArr;
	}

	//return true if any one of the cell is human ie.. 0
	private static boolean hasHuman(int [][] matrixArr) {
		if(matrixArr == null || matrixArr.length == 0 || matrixArr[0].length == 0) {
			return false;
		}
		
		int row_len = matrixArr.length;
		int col_len = matrixArr[0].length;
		
		for(int r = 0; r < row_len; r++) {
			for(int c = 0; c < col_len; c++) {
				if(matrixArr[r][c] == 0) {
					return true;
				}
			}
		}
		List<List<Integer>> list = new ArrayList<>();
		
		return false;
	}

}
