package com.onlineAssessment;

public class SapInterview {

  public static void main(String[] args) {
    

  }
  
  //
  
  boolean hasSubArray(int[] imputArr, int sum) {
    if(imputArr.length==0) {
      return false;
    }
    
    int currentSum=0;
    //7 4 2 6 9 2 3 1
    int i =0;
    
    while(i<imputArr.length) {
     int currentElement = imputArr[i];
     if(currentElement==sum) {
       return true;
     }
     for(int j=i+1; j<imputArr.length;j++) {
       currentElement+=imputArr[j];
       
       if(currentElement==sum) {
        return true;
       }
       if(currentElement>sum) {
        break;
        }
       
     }
     
     i++;
    
    }
    
    return false;
  }
  

}




/**
 * 
 * 
 * Write a SQL query to find all duplicate emails in a table named Person.

        | Id | Email   |
       +---+---------+
        | 1  | a@b.com |
        | 2  | c@d.com |
        | 3  | a@b.com |
        +----+---------+
 * 
 * */

//select * from Person p inner join temp_prs on p.id = temp_prs.id  ( select id, email from Person p Having count(email)>2  group by id) as temp_prs;






